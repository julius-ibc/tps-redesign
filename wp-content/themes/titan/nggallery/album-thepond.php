<?php 
/**
Template Page for the album overview (extended)

Follow variables are useable :

	$album     	 : Contain information about the album
	$galleries   : Contain all galleries inside this album
	$pagination  : Contain the pagination content

 You can check the content when you insert the tag <?php var_dump($variable) ?>
 If you would like to show the timestamp of the image ,you can use <?php echo $exif['created_timestamp'] ?>
**/
?>
<?php if (!defined ('ABSPATH')) die ('No direct access allowed'); ?><?php if (!empty ($galleries)) : ?>

<div class="ngg-albumoverview">	
	<!-- List of galleries -->
	<?php foreach ($galleries as $gallery) : ?>
<a class="clearfix" href="<?php echo $gallery->pagelink ?>">
	<div class="ngg-album-wrap">
		<div class="ngg-albumtitle "><?php echo $gallery->title ?></div>
			
				
					<img class="ngg-thumb " alt="<?php echo $gallery->title ?>" src="<?php echo $gallery->previewurl ?>"/>
				
				
				<div class="ngg-view" >View</div>
		
	</div>
</a>
 	<?php endforeach; ?>
 	
	<!-- Pagination -->
 	<?php echo $pagination ?>
 	
</div>

<?php endif; ?>