<div id="sidebar" class="sidebar-wrapper section-wrapper left">
	<div class="sidebar-pad section-pad-wrapper">

		<div class="sidebar-wrap section-block-wrapper left">
			<div class="sidebar <?php if (is_front_page()) { echo "front-page"; } else { echo "sub-page"; } ?>">
				<?php //dynamic_sidebar('first-widget-area'); ?>
                <?php //dynamic_sidebar('second-widget-area'); ?>
                <?php //dynamic_sidebar('third-widget-area'); ?>
                <div class="add-cart-wrapper clearfix">
					 <?php 
					 if (function_exists('wpsc_cart_item_count')) {
					  if (wpsc_cart_item_count() < 1 || isset($_GET['sessionid'])) {
					   $item = "<span class='highlight-items'>0</span> item"; 
					   $total = wpsc_cart_total_widget();
					  } else { 
					   if(wpsc_cart_item_count() > 1)
						$item = '<span class="highlight-items">'.wpsc_cart_item_count()."</span> items"; 
					   else
						$item = '<span class="highlight-items">'.wpsc_cart_item_count()."</span> item"; 
					   
					   $total = wpsc_cart_total_widget();
					  }    
					 }
					 ?>
					 <h4>Shopping Cart</h4>
					 <div class="cart_info left">
					 <div id="cart-item-count" class="item">You have <?php echo $item; ?></div>
					 <div id="cart-total" class="item">Total: <span class="highlight-total"><?php echo $total; ?></span></div>
					 
					 </div>
					 

					
						<div class="checkout right clearfix"><a href="/products/checkout/"></a></div>

				</div>				
				<div class="sidebar-product-categories">
					<h4>Product Categories</h4>
					<?php
        				remove_filter( 'get_terms', 'wpsc_get_terms_category_sort_filter' );
      					$a_args = array(
      						'taxonomy'   			=> 'wpsc_product_category',
      						'order' 				=> 'ASC',
      						'orderby' 				=> 'name',
      						'use_desc_for_title' 	=> '',
      						'title_li'				=> '',
      						'style'               => 'list',
      						'hide_empty'          	=> 0
      						
      					);
      					echo '<ul>';
      					echo wp_list_categories( $a_args );
      					echo '</ul>';
      				?>
				</div>
                <div class="sidebar-showroom">
                <a href="<?php bloginfo('url')?>/our-showroom"><h4>Visit Our Showroom</h4></a>
                <div class="margin_top_10 show_room_content padding_right_20 padding_left_20 clearfix">
                <div class="tel"><span>P:</span> <?php echo do_shortcode("[titan-business-details value=phone  name=the-pond]") ?></div>
                <div class="address margin_top_10 padding_bottom_15"><?php echo do_shortcode("[titan-business-details value=address-1 name=the-pond]") ?><br/>
                 <?php echo do_shortcode("[titan-business-details value=suburb name=the-pond]") ?><br/> <?php echo do_shortcode("[titan-business-details value=state name=the-pond]") ?><br/> <?php echo do_shortcode("[titan-business-details value=postcode name=the-pond]") ?></div>
                 <div class="office_hours margin_top_10 padding_bottom_10">
                 <div class="office_title">Opening Hours</div>
                 <div class="day_sched clearfix margin_top_10"><span class="label">Monday - Friday :</span><span class="value"><?php echo do_shortcode("[titan-business-details value=opening-mon-fri name=the-pond]") ?></span></div>
                 <div class="day_sched clearfix margin_top_5"><span class="label">Saturdays :</span><span class="value"><?php echo do_shortcode("[titan-business-details value=opening-sat name=the-pond]") ?></span></div>
                 <div class="day_sched clearfix margin_top_5"><span class="label">Sunday :</span><span class="value"><?php echo do_shortcode("[titan-business-details value=opening-sun name=the-pond]") ?></span></div>
                 <div class="day_sched clearfix margin_top_5"><span class="label">Public Holidays :</span><span class="value"><?php echo do_shortcode("[titan-business-details value=opening-sun name=the-pond]") ?></span></div>
                 </div>
                </div>
                </div>
			</div>
		</div>

	</div>
</div>