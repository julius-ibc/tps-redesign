<?php
/**
* The template for displaying Search Results pages.
*/
get_header(); 
?>

<div id="sub-page" class="main-content main-sub-page search padding_left_18 clearfix"><?php
	$search_text = esc_sql($_REQUEST['s']);
	//print $search_text."###";
	$page=(int)$_REQUEST['page'];

	$search_text= str_replace('?','\"', $search_text);
 

	if (preg_match('#page/([0-9]+)#',$_SERVER['REQUEST_URI'],$match)) {
		$page = (int)$match[1];
	}
	$num_results = 20;
	$results_start = $page * $num_results;


	// DB Query to obtain post ID, title and name
	$sql = "SELECT SQL_CALC_FOUND_ROWS ID, post_title, post_name, post_content FROM wp_posts AS post LEFT JOIN wp_postmeta AS meta ON post.ID = meta.post_id WHERE post_type = 'wpsc-product' AND post_status = 'publish' AND post_title LIKE '%$search_text%' GROUP BY ID";

	global $wpdb;
	$query = $wpdb->get_results($sql);
	/*if (!$query) {
		wp_die("Unable to get search results",'Search Error');
	}*/
	$results = count($query);

	if( $results > 0 ): ?>

		<h1>Search Results</h1>
		<p class="margin_top_15">Found <?php echo $results; ?> results for &ldquo;<strong><?php echo htmlspecialchars_decode($_GET['s']); ?></strong>&rdquo;:</p>

		<div class="wpsc_default_product_list-search clearfix">
		<?php 
			foreach ($query as $resultsArray) {
				$productLink = get_permalink( $resultsArray->ID );
				
				$specialPrice = get_post_meta($resultsArray->ID, '_wpsc_special_price');
				$specialPrice = $specialPrice[0];
				
				$productPrice = get_post_meta($resultsArray->ID, '_wpsc_price');
				$productPrice = $productPrice[0];
			
				$variationPrice = get_post_meta($resultsArray->ID, '_wpsc_price');
				$variationPrice = $variationPrice[0];

				
				$img = wp_get_attachment_image_src(get_post_thumbnail_id($resultsArray->ID),"product-listing"); 

				?>
				<div class="default_product_display product_view_80 water-features group"><?php 
					if(!empty($specialPrice)) : ?>
						<div class="wpsc_sale_item"></div><?php 
					endif; ?>
					<h2 class="prodtitle entry-title"><?php echo $resultsArray->post_title; ?></h2>			
					<div class="imagecol" style="width:154px;">
						<div class="wpsc_product_thumb"><a href="<?php echo $productLink?>"><img class="product_image" id="product_image_80" alt="<?php echo $resultsArray->post_title; ?>" title="<?php echo $resultsArray->post_title; ?>" src="<?php echo $img[0]?>" width="<?php echo $img[1]?>" height="<?php echo $img[2]?>"></a></div>
					</div>											
					<div class="productcol">
						<div class="wpsc_product_price">				                     
							<div class="pricedisplay old product_80">            
								<span class="oldprice" id="old_product_price_80">
									<?php 
									if(!empty($specialPrice)) {
										echo '$'.$specialPrice;
									}
									elseif($productPrice>0){
										echo '$'.$productPrice;
									}
									else{
										echo '<em>No Price Available</em>';
									}
									?>
								</span>
							</div>
							<div class="pricedisplay price product_80">
								<span id="product_price_80" class="currentprice pricedisplay">
								<?php if(!empty($specialPrice)):?>
									was $<?php echo $productPrice?>
								<?php endif;?>
								</span>
							</div>
						</div>
						<a class="wpsc_product_title" href="<?php echo $productLink?>" title="Product 1"><div class="view_details"> View Details</div></a>
					</div>
					<div class="clear"></div>
				</div> 
		<?php 
			
			} //end of foreach..
		print "</div>";
	else : ?>
	<div class="wpsc_default_product_list-search clearfix">
		<div id="post-0" class="post no-results not-found">
			<h1>No results found</h1>
		<p class="margin_top_15">Found <?php echo $results; ?> results for &ldquo;<strong><?php echo htmlspecialchars_decode($_GET['s']); ?></strong>&rdquo;:</p>
		</div>
	</div>
	<?php 
	endif; 
	?>
	<?php wp_reset_query(); ?>
</div>


<?php get_footer(); ?>