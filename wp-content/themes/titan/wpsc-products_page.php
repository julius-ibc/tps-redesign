<?php
global $wp_query,$wpsc_query,$wpdb;
$currentcat = wpsc_category_id();
$image_width = get_option('product_image_width');
/*
 * Most functions called in this page can be found in the wpsc_query.php file
 */
$lastcat_sql = "SELECT term_id FROM wp_term_taxonomy WHERE term_id NOT IN (SELECT DISTINCT parent FROM wp_term_taxonomy) AND taxonomy = 'wpsc_product_category'";
$lastcat = $wpdb->get_results($lastcat_sql,ARRAY_N);
$lastlvl = in_array_r($currentcat, $lastcat);
?>

<div id="default_products_page_container" class="wrap wdith_100 wpsc_container<?php if(!$lastlvl) : ?> catlevel<?php else : ?> prodlevel<?php endif; ?>">
	
	<div class="breadcrumbs">
	<?php
		//outputs the wp e-commerce breadcrumb
		wpsc_output_breadcrumbs();
		//Plugin hook for adding things to the top of the products page, like the live search
		do_action('wpsc_top_of_products_page'); 
	?>
	</div>
	
	<?php
		//if display categories
		//START CATEGORY LOOP HERE
	?>
	<?php if(wpsc_display_categories()): ?>

		<?php if($s_cat_desc = wpsc_category_description()): ?>
			<div class='wpsc_category_decription'>
				<?php  // links not showing because fo this wpautop($s_cat_desc);
					   echo $s_cat_desc;
				 ?>
			</div>
		<?php endif; ?>
		

		<div class="wpsc_categories wpsc_category_grid group<?php if($lastlvl) : ?> lastlvl<?php endif; ?>">
			<?php wpsc_start_category_query(array('parent_category_id'=> $currentcat , 'show_thumbnails'=> 1)); ?>
				<div class="wpsc_main_category_wrap wpsc_category_grid_item <?php wpsc_print_category_classes_section(); ?>">
					<a href="<?php wpsc_print_category_url();?>" class="wpsc_category_name" ><em><?php wpsc_print_category_name(); ?></em></a>
					<a href="<?php wpsc_print_category_url();?>" class="category-noimg" style="width:<?php echo get_option('category_image_width'); ?>px;height:<?php echo get_option('category_image_height'); ?>px;" ><img src="<?php bloginfo('template_url'); ?>/wpsc-noimage.gif" alt="&nbsp;" style="width:<?php echo get_option('category_image_width'); ?>px;height:<?php echo get_option('category_image_height'); ?>px;" /></a>
					<a href="<?php wpsc_print_category_url();?>" class="category_thumb_img" style="width:<?php echo get_option('category_image_width'); ?>px;height:<?php echo get_option('category_image_height'); ?>px;" ><?php wpsc_print_category_image(get_option('category_image_width'),get_option('category_image_height')); ?></a>
					<?php if(wpsc_show_category_description()) :?>
						<?php wpsc_print_category_description("<div class='wpsc_category_decription'>", "</div>"); ?>
					<?php endif;?>
					<?php
						//Un-Comment this to print out the subcategories
						//wpsc_print_subcategory("<div class='wpsc_sub-categories_wrap'>", "</div>");
					?>
				</div><!--/wpsc_category_wrap-->
			<?php if( !function_exists( 'wpsc_end_category_query_titan' ) ) { wpsc_end_category_query(); }else{ wpsc_end_category_query_titan(); } ?>
		</div><!--close wpsc_categories-->
			
	<?php endif; //end if display categories // END CATEGORY LOOP HERE ?>
	
	
	<?php //if display products ?>
	<?php if(wpsc_display_products()): ?>
		<?php if(wpsc_is_in_category()) : ?><h2 class="hide">Products List</h2><?php endif; ?>
	
		<?php //!!START PAGINATION TOP HERE ?>
		<?php if(wpsc_has_pages_top()) : ?>
			<div class="wpsc_page_numbers_top">
				<?php wpsc_pagination(); ?>
			</div><!--close wpsc_page_numbers_top-->
		<?php endif; ?>
		<?php //!!END PAGINATION TOP HERE ?>

<!-- START -------- -->
		<?php //!!START PRODUCT LOOP HERE ?>
		<div class="margin_top_20 wpsc_default_product_list<?php if(wpsc_is_in_category()) : ?> inside-category width_100 clearfix<?php endif; ?>">
			<div class="el-product-wrapper">
			<?php 
			//start of while loop
			while (wpsc_have_products()) {
				wpsc_the_product(); 
			?>
				<div class="el-prod-div">
					<?php echo getProductImage(); //display the Product Image with HTML ?>
					<?php echo getProductTitle(); //display the Product Title with HTML ?>
					<?php echo getProductPrice(); //display the Product Price with HTML ?>
					<?php echo getProductMainDescription(); //display the Product Main Description with HTML ?>
					<?php echo getViewDetailsButton(); //display the View Details button with HTML ?>
				</div>
			<?php
			} //end of while loop in getting the products
			?>
			</div>
		</div>
		<?php //!!END PRODUCT LOOP HERE ?>

<!-- END -------- -->

		<?php //!!if there is no products ?>
		<?php if(wpsc_product_count() == 0):?>
		<div class="wpsc_default_product_list wpsc_no-product">
			<h3><?php  _e('There are no products in this group.', 'wpsc'); ?></h3>
		</div>
		<?php endif ; ?>

		<?php do_action( 'wpsc_theme_footer' ); ?>

		<?php //!!START PAGINATION BOTTOM HERE ?>
		<?php if(wpsc_has_pages_bottom()&&  $lastlvl && wpsc_product_count() > 0) : ?>
			<div class="page-nav-container ">
		
			<div class="page-nav clearfix wpsc_paging_bottom">
				<?php wpsc_pagination(); ?>
			</div><!--close wpsc_page_numbers_bottom-->
			</div><!--close wpsc_page_numbers_bottom-->
		<?php endif; ?>
		<?php //!!END PAGINATION BOTTOM HERE ?>
	
	<?php endif; //end if display products ?>

</div><!--/default_products_page_container-->

<?php

function getProductImage() {
	
	$html = '<div class="el-product-image-container">'; // start div container for product image
	
	//if free shipping
	$product_meta = get_post_meta( wpsc_the_product_id(), '_wpsc_product_metadata' );

	if( $product_meta[0]['no_shipping'] == 1)
   	{
   		$html .= noShipping(); //if no shipping is clicked;
   	}

   	if( wpsc_product_on_special() ) {
		$html .= getSpecialWave($product_meta[0]['no_shipping']); //get the special image attachec in the product iamge.
	}
	
	//check if there is thumb nail
	if( wpsc_the_product_thumbnail() ) {
		$html .= '<a href="'. wpsc_the_product_permalink() .'">'; //start of the link
		$html .= '<img class="el-product-img" id="el-product-img-'. wpsc_the_product_id() .'" alt="'. wpsc_the_product_title() .'" title="'. wpsc_the_product_title() .'" src="'. wpsc_the_product_thumbnail() .'"/>'; //the image
		$html .= '</a>'; //end of the link
	}
	else { //if no image
		$html .= '<a href="'. wpsc_the_product_permalink() .'">'; //start of the link
		$html .= '<img class="el-no-img" id="el-product-img-'. wpsc_the_product_id() .'" alt="No Image" title="'. wpsc_the_product_title() .'" src="'. get_bloginfo('template_url') .'/wpsc-noimage.gif" height="'. get_option('product_image_width') .'" width="'. get_option('product_image_height') .'"/>'; //the image
		$html .= '</a>'; //end of the link		
	}
	$html .= '</div>'; //end of div container for product image
	
	//$html = '<div class="el-product0image-unshow"><em>Product Image Thumbnails Unshow</em></div>';
	
	return $html;
}

function getProductTitle() {
	$the_tile = wpsc_the_product_title();
	//check if all string are CAPS.
	if( strtoupper($the_tile) == $the_tile ) {
		$prod_title = ucfirst( $the_tile ); //convert to First Letter capitalize..
		$prod_title = ucfirst( strtolower($prod_title) ); //convert to First Letter capitalize..
	}
	else {
		$prod_title = ucfirst( $the_tile ); //convert to First Letter capitalize..
	}
	$prod_title = $the_tile;
	
	$html = '<div class="el-product-title-container"><h2>'; // start div container for product title
	//check if hide name link
	if( get_option('hide_name_link') == 1 ) {
		$html .= '<a class="wpsc_product_title" href="'. wpsc_the_product_permalink() .'" title="'. $prod_title .'">'; //start of link
		$html .= $prod_title;
		$html .= '</a>'; //end of link
	}
	else {
		$html .= '<a class="wpsc_product_title" href="'. wpsc_the_product_permalink() .'" title="'. $prod_title .'">'; //start of link
		$html .= $prod_title;
		$html .= '</a>'; //end of link
	}
	//get the product sku..
	//$html .= '<span class="wpsc_product_sku">'. wpsc_product_sku( wpsc_the_product_id() ) .'</span>';
	
	$html .= '</h2></div>'; // end div container for product title
	
	return $html;
}

function getProductPrice() {
	
	$html = '<div class="el-price-container">'; // start div container for price
	
	//do stock container here.. 
	
	// if price is not zero
	//get_post_meta(wpsc_the_product_id(), '_wpsc_price', true)  >  0
	
	if( wpsc_product_has_stock() ) {
		// if price is donation
		if( wpsc_product_is_donation() ) {
			$html .= '<label class="el-donation-label"> Donation </label>';
			$html .= ' <input type="text" id="" name="donation_price" value="'. wpsc_calculate_price(wpsc_the_product_id()) .'" size="6" />';
		}
		else { //else if not donation
			//wpsc_the_product_price_display()
			//wpsc_the_product_price()
			if( get_post_meta(wpsc_the_product_id(), '_wpsc_price', true)  >  0 ) {
				$html .= '<span class="el-sale-price">'. wpsc_the_product_price() .'</span>';
				//This is not yet appeared.. not include for a moment
				//$html .= '<span class="el-old-price"><sup>RRP '. wpsc_product_normal_price() .'</sup></span>';
			}
			elseif (wpsc_have_variation_groups()) { 
				$html .= '<span class="el-sale-price">'. wpsc_the_product_price() .'</span>';
			}
			else {
				$html .= '<div class="el-no-price-div">';
				$html .= '<em>Call for pricing</em>';
				$html .= '</div>';
			}
			
			//check if product is special
			if( wpsc_product_on_special() ) {
				//$html .= wpsc_product_normal_price();
			}
			//check if multi currency code
			if( wpsc_product_has_multicurrency() ) {
				$html .= wpsc_display_product_multicurrency();
			}
			//check if show product postage and packaging
			if( wpsc_show_pnp() ) {
				$html .= '<label class="el-shipping-label">Shipping</label>';
				$html .= wpsc_product_postage_and_packaging();
			}
		}
	}
	else { //if price is zero
		$html .= '<div class="el-no-price-div">';
		$html .= '<em>Currently Out of Stock</em>';
		$html .= '</div>';
	}
	
	$html .= '</div>'; // end of div container for price
	
	return $html;				
}

function getProductMainDescription() {

	$html = '<div class="el-product-main-desc-container">'; // start of div container for product main description
	
	//$description = ucfirst( substr( plaintext(wpsc_the_product_description()), 0, 60 ) ); 
	//$description = ucfirst( strtolower($description) );
	$description = get_field('enter_short_text');
	
	$html .= '<span>' .$description. '</span>';
	
	$html .= '</div>'; // end of div container for product main description
	
	return $html;
}

function getViewDetailsButton() {

	$html = '<div class="el-view-details-container">'; // start of div container for view details button

	$html .= '<a href="' .wpsc_the_product_permalink(). '" title="' .wpsc_the_product_title(). '">'; // start of link view details
	$html .= 'View Details'; // the title of the button
	$html .= '</a>'; //end of link view details
	
	$html .= '</div>'; // end of div container for view details button
	
	return $html;
}

function getSpecialWave($has_shipping = '') {	
	$style = '';

	if($has_shipping)
	{
		$style="style='top: 34px'";
	}

	$html = "<div class='el-special-product-wave' $style></div>";
	return $html;
}

function noShipping() {
	$html = '<div class="el-no-shipping-product"></div>';

	return $html;
}


function plaintext($html) {
    // remove comments and any content found in the the comment area (strip_tags only removes the actual tags).
    $plaintext = preg_replace('#<!--.*?-->#s', '', $html);

    // put a space between list items (strip_tags just removes the tags).
    $plaintext = preg_replace('#</li>#', ' </li>', $plaintext);

    // remove all script and style tags
    $plaintext = preg_replace('#<(script|style)\b[^>]*>(.*?)</(script|style)>#is', "", $plaintext);

    // remove br tags (missed by strip_tags)
    $plaintext = preg_replace("#<br[^>]*?>#", " ", $plaintext);

    // remove all remaining html
    $plaintext = strip_tags($plaintext);

    return $plaintext;
}


?>