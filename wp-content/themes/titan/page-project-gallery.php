<?php get_header(); ?>
<div class="main-content section-block-wrapper padding_left_18 left">
	<div id="sub-page" class="main-sub-page section-pad-wrapper">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<h1 class="page-title"><?php the_title(); ?></h1>
		<div class="clearfix width_100">
		<?php $gallery_id= get_query_var('gallery');
		
				if(empty($gallery_id)):
				?>
				<div class="breadcrumbs margin_top_10"><a href="<?php bloginfo('url');?>">Home</a> &nbsp;<span class="arrow">»</span>&nbsp; <span class="current">Project Gallery</span> </div>
				<?php else:?>
				<?php 
				$query_gallery="SELECT title FROM  wp_ngg_gallery   WHERE gid=".$gallery_id;
				
				
				$gallery_result=$wpdb->get_row($query_gallery);
				
				?>
				<div class="breadcrumbs margin_top_10"><a href="<?php bloginfo('url');?>">Home</a> &nbsp;<span class="arrow">»</span>&nbsp; <a href="<?php bloginfo('url');?>/project-gallery">Project Gallery</a> &nbsp;<span class="arrow">»</span>&nbsp; <span class="current"><?php echo $gallery_result->title?></span> </div>
				<?php endif;?>
		
		<div class="margin_top_15 clearfix">
		<?php the_content(); ?>
		</div>
		
		</div>
	<?php endwhile; else: ?>
		
	<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>