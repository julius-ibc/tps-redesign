<?php
/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 960;

/** Tell WordPress to run titan_setup() when the 'after_setup_theme' hook is run. */
add_action( 'after_setup_theme', 'titan_setup' );

if ( ! function_exists( 'titan_setup' ) ):
function titan_setup() {
	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();
	// This theme uses post thumbnails
	// IMP: ADD/REMOVE more image size as required BEFORE starting implementation.
	add_theme_support( 'post-thumbnails' );
	add_image_size('banner-slide',708,316,true);
	add_image_size('product-listing',154,154,true);
	add_image_size('product',237,288,false);
	add_image_size('product-gallery-thumb', 51, 51, true);
	add_image_size('gallery-listing-category',158, 91, true);
	add_image_size('gallery-listing',160, 104, true);
	add_image_size('page',222, 160, true);

	// This theme uses wp_nav_menu() in two location.
	// IMP: ADD/REMOVE more nav if needed.
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'titan' ),
		'footer-nav' => __( 'Footer Top Navigation', 'titan' ),
		'footer-nav-b' => __( 'Footer Bottom Navigation', 'titan' )
	) );
}
endif;

function titan_excerpt_length( $length ) {
	return 120;
}
add_filter( 'excerpt_length', 'titan_excerpt_length' );

/**
 * Returns a "Continue Reading" link for excerpts
 */
function titan_continue_reading_link() {
	return ' <a href="'. get_permalink() . '">' . __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'titan' ) . '</a>';
}

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and titan_continue_reading_link().
 */
function titan_auto_excerpt_more( $more ) {
	return ' &hellip;' . titan_continue_reading_link();
}
add_filter( 'excerpt_more', 'titan_auto_excerpt_more' );

/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 */
function titan_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= titan_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'titan_custom_excerpt_more' );

/**
 * Remove inline styles printed when the gallery shortcode is used.
 */
function titan_remove_gallery_css( $css ) {
	return preg_replace( "#<style type='text/css'>(.*?)</style>#s", '', $css );
}
add_filter( 'gallery_style', 'titan_remove_gallery_css' );

/**
 * Register widgetized areas, including two sidebars and two widget-ready columns in the footer.
 *
 * To override titan_widgets_init() in a child theme, remove the action hook and add your own
 * function tied to the init hook.
 */
function titan_widgets_init() {
	// Area 1
	register_sidebar( array(
		'name' => __( 'First Widget Area', 'titan' ),
		'id' => 'first-widget-area',
		'description' => __( 'The first widget area', 'titan' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 2
	register_sidebar( array(
		'name' => __( 'Second Widget Area', 'titan' ),
		'id' => 'second-widget-area',
		'description' => __( 'The second widget area', 'titan' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 3
	register_sidebar( array(
		'name' => __( 'Third Widget Area', 'titan' ),
		'id' => 'third-widget-area',
		'description' => __( 'The third widget area', 'titan' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 4
	register_sidebar( array(
		'name' => __( 'Fourth Widget Area', 'titan' ),
		'id' => 'fourth-widget-area',
		'description' => __( 'The fourth widget area', 'titan' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
/** Register sidebars by running titan_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'titan_widgets_init' );

if ( ! function_exists( 'titan_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post—date/time and author.
 */
function titan_posted_on() {
	printf( __( '<span class="%1$s">Posted on</span> %2$s <span class="meta-sep">by</span> %3$s', 'titan' ),
		'meta-prep meta-prep-author',
		sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="entry-date">%3$s</span></a>',
			get_permalink(),
			esc_attr( get_the_time() ),
			get_the_date()
		),
		sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s">%3$s</a></span>',
			get_author_posts_url( get_the_author_meta( 'ID' ) ),
			sprintf( esc_attr__( 'View all posts by %s', 'titan' ), get_the_author() ),
			get_the_author()
		)
	);
}
endif;

if ( ! function_exists( 'titan_posted_in' ) ) :
/**
 * Prints HTML with meta information for the current post (category, tags and permalink).
 */
function titan_posted_in() {
	// Retrieves tag list of current post, separated by commas.
	$tag_list = get_the_tag_list( '', ', ' );
	if ( $tag_list ) {
		$posted_in = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'titan' );
	} elseif ( is_object_in_taxonomy( get_post_type(), 'category' ) ) {
		$posted_in = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'titan' );
	} else {
		$posted_in = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'titan' );
	}
	// Prints the string, replacing the placeholders.
	printf(
		$posted_in,
		get_the_category_list( ', ' ),
		$tag_list,
		get_permalink(),
		the_title_attribute( 'echo=0' )
	);
}
endif;

/*CUSTOM POST TYPES*/
// Set priority to avoid plugin conflicts
add_action('init', 'register_rc', 1);
function register_rc() { // A unique name for our function
	$labels = array(
		'name' => _x('Banner Slides', 'post type general name'),
		'singular_name' => _x('Banner List', 'post type singular name'),
		'add_new' => _x('Add New', 'Banner'),
		'add_new_item' => __('Add New Banner'),
		'edit_item' => __('Edit Banner'),
		'new_item' => __('New Banner'),
		'view_item' => __('View Banner'),
		'search_items' => __('Search Banner'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash')
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => 'true', // Activate the archive
		'supports' => array('title','editor','thumbnail','page-attributes','custom-fields'),
		'taxonomies' => array('category') // this is IMPORTANT
	);
	register_post_type( 'banner-slide', $args );

}
add_action('init', 'register_design_services', 1);
function register_design_services() { // A unique name for our function
	$labels = array(
			'name' => _x('Design Services', 'post type general name'),
			'singular_name' => _x('Design Service List', 'post type singular name'),
			'add_new' => _x('Add New', 'Design Service'),
			'add_new_item' => __('Add New Design Service'),
			'edit_item' => __('Edit Design Service'),
			'new_item' => __('New Design Service'),
			'view_item' => __('View Design Service'),
			'search_items' => __('Search Design Service'),
			'not_found' =>  __('Nothing found'),
			'not_found_in_trash' => __('Nothing found in Trash')
	);
	$args = array(
			'labels' => $labels,
			'public' => true,
			'has_archive' => 'true', // Activate the archive
			'supports' => array('title','editor','thumbnail','page-attributes','custom-fields'),
			'taxonomies' => array('category') // this is IMPORTANT
	);
	register_post_type( 'design-service-list', $args );

}
add_action('init', 'register_help_centres', 1);
function register_help_centres() { // A unique name for our function
	$labels = array(
			'name' => _x('Help Centres', 'post type general name'),
			'singular_name' => _x('Help Centre List', 'post type singular name'),
			'add_new' => _x('Add New', 'Help Centre'),
			'add_new_item' => __('Add New Help Centre'),
			'edit_item' => __('Edit Help Centre'),
			'new_item' => __('New Help Centre'),
			'view_item' => __('View Help Centre'),
			'search_items' => __('Search Help Centre'),
			'not_found' =>  __('Nothing found'),
			'not_found_in_trash' => __('Nothing found in Trash')
	);
	$args = array(
			'labels' => $labels,
			'public' => true,
			'has_archive' => 'true', // Activate the archive
			'supports' => array('title','editor','thumbnail','page-attributes','custom-fields'),
			'taxonomies' => array('category') // this is IMPORTANT
	);
	register_post_type( 'help-centre-list', $args );

}

add_action('init', 'register_news', 1);
function register_news() { // A unique name for our function
	$labels = array(
			'name' => _x('News', 'post type general name'),
			'singular_name' => _x('News List', 'post type singular name'),
			'add_new' => _x('Add New', 'News'),
			'add_new_item' => __('Add News'),
			'edit_item' => __('Edit News'),
			'new_item' => __('New News'),
			'view_item' => __('View News'),
			'search_items' => __('Search News'),
			'not_found' =>  __('Nothing found'),
			'not_found_in_trash' => __('Nothing found in Trash')
	);
	$args = array(
			'labels' => $labels,
			'rewrite' => array('slug' => 'news'),
			'public' => true,
			'has_archive' => 'true', // Activate the archive
			'supports' => array('title','editor','thumbnail','page-attributes','custom-fields'),
			'taxonomies' => array('category') // this is IMPORTANT
	);
	register_post_type( 'news-list', $args );
}

// TinyMCE won't strip out iframe anymore
function mytheme_tinymce_config( $init ) {
	$valid_iframe = 'iframe[id|class|title|style|align|frameborder|height|longdesc|marginheight|marginwidth|name|scrolling|src|width]';
	if ( isset( $init['extended_valid_elements'] ) ) {
		$init['extended_valid_elements'] .= ',' . $valid_iframe;
	} else {
		$init['extended_valid_elements'] = $valid_iframe;
	}
	return $init;
}
add_filter('tiny_mce_before_init', 'mytheme_tinymce_config');

// remove junk from head
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
// Remove Product List RSS Wordpress
remove_action('wp_head', 'wpsc_product_list_rss_feed');

// Remove Styles WP E-Commerce
function childtheme_deregister_styles() {
//  wp_deregister_style( 'wpsc-theme-css' );
//This is for the "Updating cart..." popup, you can remove also if you want
//wp_deregister_style( 'wpsc-theme-css-compatibility' );
  wp_deregister_style( 'wpsc-product-rater' );
  wp_deregister_style( 'wp-e-commerce-dynamic');
  wp_deregister_style( 'wpsc-thickbox' );
  wp_deregister_style( 'wpsc-gold-cart' );
}
add_action( 'wp_print_styles', 'childtheme_deregister_styles', 100 );

// Remove Scripts WP E-Commerce
function childtheme_deregister_scripts() {
	wp_deregister_script( 'wpsc-gold-cart' );
	wp_deregister_script( 'infieldlabel' );
}
add_action( 'wp_print_scripts', 'childtheme_deregister_scripts', 100 );

//Remove Adminbar
add_filter('show_admin_bar', '__return_false');

//Adding jQuery from Google and load any additional script the correct way
function jquery_init() {
	if (!is_admin()) {//load scripts for non admin pages
		//deregister current jquery
		wp_deregister_script('jquery');
		//load jquery from google api, and place in footer
		wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js', false, '1.8',false);
		wp_enqueue_script('jquery');
		// load a additional js files
	}elseif (is_admin()){
		//load scripts for admin page
	}
}
add_action('init', 'jquery_init');

/* Titan Truncate */
function ShortenText($text, $chars_limit = 10 , $after = "..." ) {
	$chars_text = strlen($text);
	$text = $text." ";
	$text = substr($text,0,$chars_limit);
	$text = substr($text,0,strrpos($text,' '));
	/* $text = strip_tags($text); */
	if ($chars_text > $chars_limit) { $text = $text.$after; }
	return $text;
}
//display wpsc products category
function my_wpsc_products_shortcode($atts) {
        $number_per_page = get_option('use_pagination') ? get_option('wpsc_products_per_page') : 0;
        $query = shortcode_atts(array(
               'category_id' => isset($atts['cat']) ? $atts['cat'] : 0,
               'limit_of_items' => 0,
               'sort_order' => null,
               'number_per_page' => $number_per_page,
               'page' => 0,
        ), $atts);

        return wpsc_display_products_page($query);
}
add_shortcode('my_wpsc_category', 'my_wpsc_products_shortcode');

//get post attachments
function get_attachments($id,$type,$numberposts=false){
	if(!$numberposts){
		$numberposts = -1;
	}
	$args = array(
		'numberposts' => 0,
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'post_type' => 'attachment',
		'numberposts' => $numberposts,
		'post_status' => null,
		'post_mime_type' => $type,
		'post_parent' => $id
	); //'post_mime_type' : image, application (pdf,doc);
	$att = array();
	$attachment_list = get_posts($args);
	if (count($attachment_list)>0) {
		foreach ( $attachment_list as $attachment ) {
				$alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
				//put them in an array
				$temp = array(
				  'name' => $attachment->post_name,
				  'guid' => $attachment->guid,
				  'alt' => $alt,
				  'post_title' => $attachment->post_title,
				  'post_content'=> $attachment->post_content
				);
				array_push($att, $temp);
		}
	}
	return $att;
}

// plugin less breadcrumbs
function dimox_breadcrumbs() {

  $delimiter = '&raquo;';
  $home = 'Home'; // text for the 'Home' link
  $before = '<span class="current">'; // tag before the current crumb
  $after = '</span>'; // tag after the current crumb

  if ( !is_home() && !is_front_page() || is_paged() ) {

    echo '<div id="crumbs">';

    global $post;
    $homeLink = get_bloginfo('url');
    echo '<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';

    if ( is_category() ) {
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
      echo $before . 'Archive by category "' . single_cat_title('', false) . '"' . $after;

    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;

    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;

    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;

    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . ' ';
        echo $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        echo $before . get_the_title() . $after;
      }

    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;

    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';
      echo $before . get_the_title() . $after;

    } elseif ( is_page() && !$post->post_parent ) {
      echo $before . get_the_title() . $after;

    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
      echo $before . get_the_title() . $after;

    } elseif ( is_search() ) {
      echo $before . 'Search results for "' . get_search_query() . '"' . $after;

    } elseif ( is_tag() ) {
      echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;

    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . 'Articles posted by ' . $userdata->display_name . $after;

    } elseif ( is_404() ) {
      echo $before . 'Error 404' . $after;
    }

    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }

    echo '</div>';

  }
} // end dimox_breadcrumbs()

function in_array_r($needle, $haystack) {
    $found = false;
    foreach ($haystack as $item) {
    if ($item === $needle) {
            $found = true;
            break;
        } elseif (is_array($item)) {
            $found = in_array_r($needle, $item);
			if($found) {
				break;
            }
        }
    }
    return $found;
}

//give editor access to gravity form entries.
$role = get_role( 'editor' );
$role->add_cap( 'gravityforms_view_entries' );
$role->add_cap( 'gravityforms_export_entries' );
$role->add_cap( 'gravityforms_view_entry_notes' );

// Add class to body
function add_body_class( $classes ) {
  global $post;

  if( isset( $post ) ) {
    $classes[] = $post->post_type.'-'.$post->post_name;
  }
  return $classes;
}
add_filter('body_class', 'add_body_class');

// pie
function css_pie ( $vars ) {
	$vars[] = 'pie';

	return $vars;
}
add_filter( 'query_vars' , 'css_pie'); //WordPress will now interpret the PIE variable in the url
function load_pie() {
	if ( get_query_var( 'pie' ) == "true" ) {
		header( 'Content-type: text/x-component' );
		wp_redirect( get_bloginfo('template_url').'/css/PIE/PIE.htc' ); // adjust the url to where PIE.htc is located, in this example we are fetching in the themes includes directory
		// Stop WordPress entirely since we just want PIE.htc
		exit;
	}
}
add_action( 'template_redirect', 'load_pie' );



function custom_product_breadcrumbs(){
	global $wp_query,$wpdb;


	?>
	<?php if(isset($wp_query->query_vars['wpsc_product_category'])&& !isset($wp_query->query_vars['wpsc-product'])):?>
<div class="breadcrumbs margin_top_10"><a href="<?php bloginfo('url');?>">Home</a> &nbsp;<span class="arrow">»</span>&nbsp; <a href="<?php bloginfo('url');?>/products">Our Products</a> &nbsp;<span class="arrow">»</span>&nbsp; <span class="current"><?php echo $wp_query->queried_object->name?></span> </div>
	<?php elseif(isset($wp_query->query_vars['wpsc-product'])):?>
	<?php

	$query_term="SELECT name FROM $wpdb->terms  WHERE slug='".$wp_query->query_vars['wpsc_product_category']."'";


	$term_r=$wpdb->get_row($query_term);

	?>
	<div class="breadcrumbs margin_top_10"><a href="<?php bloginfo('url');?>">Home</a> &nbsp;<span class="arrow">»</span>&nbsp; <a href="<?php bloginfo('url');?>/products">Our Products</a> &nbsp;<span class="arrow">»</span> <a href="<?php bloginfo('url');?>/products/<?php echo $wp_query->query_vars['wpsc_product_category']?>"><?php echo $term_r->name?></a> &nbsp;<span class="arrow">»</span>&nbsp; <span class="current"><?php echo $wp_query->queried_object->post_title?></span> </div>
	<?php endif;?>
	<?php


}


/**
 * Function for first-time login redirect
 *
 * @param $redirect_to
 * @return $redirect_to
*/
function custom_login_redirect( $redirect_to )
{

	/**
	 * Normally, the redirect process does not require login info as that
	 * occurs in the login function that the redirect filter is hooking into.
	 * However, in this case, we have a db flag that is used to indicate if
	 * this is a first-time login or not. In order to secure that process,
	 * we need to check login info.
	 */

	/**
	 * Get the $user object using the username
	 */


	$user = sanitize_user( $_POST['log'] );
	$user = get_user_by( 'login', $user );

	/**
	 * Validate the user/pass combination
	*/
	$pass = wp_check_password( $_POST['pwd'], $user->user_pass, $user->ID );
	if( ! $pass ) {
		/**
		 * If it doesn't validate, we will just return the unfiltered
		 * $redirect_to.  The login will fail in the login function.
		 */
		return $redirect_to;
	}



	$url = get_bloginfo('url');


	return $url;

}
/**
 * Call the filter, make it priority 1
 */
add_filter( 'wpmem_login_redirect', 'custom_login_redirect', 1 );

/* fix for Titan Admin CSS */
function acf_fix() {
	echo '<link rel="stylesheet" href="'. get_bloginfo('template_url') .'/css/titan_admin.css" type="text/css" media="all" />';
}
add_action('admin_head', 'acf_fix');
/* fix for Titan Admin CSS */

/* For Short Description/Text in Add/Edit Product */
/* for local used: $("#acf-field_13").attr("maxlength","60"); */
function ti_limit_text() {

	echo '
		<style type="text/css">

		</style>
	';

	echo '
	<script type="text/javascript">
	jQuery(document).ready(function( $ ) {
		$("#acf-field_11").attr("maxlength","60");
	});
	</script>';
}
add_action('admin_head', 'ti_limit_text');

/* Showing Related Products */
function selectlistofRelatedProducts() {
	echo '
	<script type="text/javascript">
	jQuery(document).ready(function( $ ) {
		$("#wpsc_rp_product_1").attr("style","width: 100%; height: 200px;");
	});
	</script>';
}
add_action('admin_head', 'selectlistofRelatedProducts');

/** begin -- For New Admin Login Implementaiton 10-24-2013 **/

// Style the login page with Titan Logo. Change to put client logo instead.
function my_custom_login_logo() {
	echo '<style type="text/css">
        #login {width: 320px;padding: 114px 0 0;margin: auto;}
        .login h1 a {display:none;}
		.login h1 { margin-bottom: 20px; background-image:url(/wp-content/themes/titan/images/logo.png); background-size: contain;background-repeat: no-repeat;padding-left: 35px;width: 100%;height: 109px; }
        .login input[type="text"], .login input[type="password"] {width:100%;}
	</style>';
}
add_action('login_head', 'my_custom_login_logo');

// Add Titan logo under the dashboard
if (! function_exists('dashboard_footer') ) {
	function dashboard_footer() {
		echo '<div class="titan-footer alignleft"><a href="http://www.ibc.com.au" target="_blank" title="IBC Digital"><img src="https://www.ibc.com.au/IBC%20Digital/Templates/img/ibc-digital-logo.png" alt="IBC Digital"></a></div>';
	}
}
add_filter('admin_footer_text', 'dashboard_footer');

// changing the link to titan logo in the admin login form
function titan_login_url() {  return 'http://www.ibc.com.au'; }
add_filter('login_headerurl', 'titan_login_url');

// changing the alt text on the logo to show titan web in the admin login form
function titan_login_title() { return 'IBC Digital'; }
add_filter('login_headertitle', 'titan_login_title');

/** end -- For New Admin Login Implementaiton 10-24-2013 **/



function my_wpsc_default_weight_unit( $unit ) {
    return 'kilogram';
}
add_filter( 'wpsc_default_weight_unit', 'my_wpsc_default_weight_unit' );

function my_wpsc_default_dimension_unit( $unit ) {
    return 'cm';
}
add_filter( 'wpsc_default_dimension_unit', 'my_wpsc_default_dimension_unit' );





function return_weight() {

    $product_id = $_GET['post_id'];
    $variations = array();

	$response = array();
	$response['error'] = 'false';
    $response['over_weight'] = false;
	$response['stock'] = '0';
	$response['no_shipping'] = '0';

  	parse_str($_GET['variation'], $var_ids);

    foreach ( $var_ids['variation'] as $variation ) {
		if ( is_numeric( $variation ) ) {
			$variations[] = (int)$variation;
		}
	}

	if(empty($variations)) {
  		$response['error'] = 'true';
  	}

    global $wpdb;

    

	$selected_post = get_posts( array(
				'post_parent'      => $product_id,
				'post_type'        => "wpsc-product",
				'post_status'      => 'any',
				'suppress_filters' => true,
				'numberposts'      => -1,
			) );

	$selected_variation = false;

	foreach ( $selected_post as $variation ) {
		$matches = 0;
		$terms = wpsc_get_product_terms( $variation->ID, 'wpsc-variation' );
		foreach ( $terms as $term ) {
			if ( in_array( $term->term_id, $variations ) )
				$matches++;
		}

		if ( $matches == count( $variations ) ) {
			$selected_variation = $variation->ID;
		}
	}

	if ( ! $selected_variation )
		return false;

	if ( wpsc_product_has_stock( $selected_variation ) ) {
		$response['stock'] = get_product_meta( $selected_variation, 'stock', true );	
		$meta = get_post_meta( $selected_variation, '_wpsc_product_metadata', true );
	 	
	 	if($response['stock'] === '') {
	 		$response['stock'] = '1';
	 	}	 		

	 	if ( $meta['weight'] > 48 ) {
            $response['over_weight'] = 'true';
        }
       	
       	if( get_field('store_pick-up_only',$selected_variation) == 'Yes' ){
       		$response['store_pick_up'] = 'true';
       	}

        $response['no_shipping'] = $meta['no_shipping'];
	}


	echo json_encode($response);
	exit();
    /*









    foreach ( $variations as $item ) {
        
        $term_list = wp_get_post_terms($item->ID, 'wpsc-variation');
        $found = false;
        
        foreach ($term_list as $term_item) {

            if ( $term_item->term_id == $term_id ) {
                $found = true;
                break;
            }
            
        }
        
        if ( $found ) {
            
            //weight
            $meta   = get_post_meta( $item->ID, '_wpsc_product_metadata', true );
            
            if ( $meta['weight'] > 48 ) {
                $result['over_weight'] = 'true';
            }
            
            break;
            
        }
        
        
        
    }
    
    echo json_encode($result);
    exit; */
}

add_action('wp_ajax_return_weight', 'return_weight'); //fire get_more_posts on AJAX call for logged-in users;
add_action('wp_ajax_nopriv_return_weight', 'return_weight'); //fire get_more_posts on AJAX call for all other users;






function return_weight_single() {
    
    $post_id = $_GET['post_id'];
    
    $result['over_weight'] = 'false';
    
    //weight
    $meta   = get_post_meta( $post_id, '_wpsc_product_metadata', true );

    if ( $meta['weight'] > 48 ) {
        $result['over_weight'] = 'true';
    }
    
    echo json_encode($result);
    exit;
}

add_action('wp_ajax_return_weight_single', 'return_weight_single'); //fire get_more_posts on AJAX call for logged-in users;
add_action('wp_ajax_nopriv_return_weight_single', 'return_weight_single'); //fire get_more_posts on AJAX call for all other users;


//update the cap required to view sales report
add_filter( 'wpsc_purchase_logs_cap', 'titan_purchase_logs_cap' );
function titan_purchase_logs_cap ( $cap ) 
{
    return 'edit_others_posts';
    
}


function purchaseLogStatusEvent( $id, $status, $old_status, $purchase_log ) {
    /************************
    status codes:
      1: Incomplete Sale
      2: Order Received
      3: Accepted Payment
      4: Job Dispatched
      5: Closed Order
      6: Payment Declined
    ************************/
    if ( $status==4 ) {
        
        global $wpdb;
        
        $query = 'SELECT * FROM wp_wpsc_submited_form_data WHERE log_id=' . $id;
        
        $user_details = $wpdb->get_results($query, ARRAY_A);
        
        $first_name = '';
        $last_name = '';
        $email_address = '';
        
        foreach ( $user_details as $user ) {
            
            switch ($user['form_id']) {
                case 2:
                    $first_name = $user['value'];
                    break;
                case 3:
                    $last_name = $user['value'];
                    break;
                case 9:
                    $email_address = $user['value'];
                    break;
            }
            
        }
        
        $subject  = "The Pond Shop - Your order has now been despatched";
        
        $message = '<p><img alt="The Pond Shop" src="http://thepondshop.com.au/wp-content/themes/titan/images/logo.png"></p>
            <p>&nbsp;</p>
            <p>Many thanks for shopping with The Pond Shop,<br />
            Your order has now been despatched and should be with you shortly.<br />
            If you\'ve any queries, please don\'t hesitate to <a href="http://www.thepondshop.com.au/contact-us/">contact us</a>.
            </p>';
         
        add_filter('wp_mail_content_type', create_function('', 'return "text/html"; '));
        wp_mail( $email_address, $subject, $message );
        add_filter('wp_mail_content_type', create_function('', 'return "text/plain"; '));
        
        
    }

}

add_action('wpsc_update_purchase_log_status', 'purchaseLogStatusEvent', 9, 4 );



function theme_cart_update_custom() {

    $result['cart_count'] = wpsc_cart_item_count();
    $result['total'] = wpsc_cart_total_widget();
    
    echo json_encode($result);
    exit;
}

add_action('wp_ajax_theme_cart_update_custom', 'theme_cart_update_custom'); //fire get_more_posts on AJAX call for logged-in users;
add_action('wp_ajax_nopriv_theme_cart_update_custom', 'theme_cart_update_custom'); //fire get_more_posts on AJAX call for all other users;


 /**
function theme_cart_update() {
	$cart_count = wpsc_cart_item_count();
	$total = "Total: <span class='highlight-total'>".wpsc_cart_total_widget()."</span>";
	if( $cart_count > 1)
		$cart_count = "You have <span class='highlight-items'>".$cart_count."</span> items";
	else
		$cart_count = "You have <span class='highlight-items'>".$cart_count."</span> item";

	echo '
  jQuery("#cart-item-count").html("'.$cart_count.'");
  jQuery("#cart-total").html("'.$total.'");
  ';
}
add_action('wpsc_alternate_cart_html', 'theme_cart_update');
 */

/**
 * We need to check through each item in the cart
 * and make sure that there is no item that is over aust post weight/dimensions.
 * Reason for this function is due to the problem in aust post. 
 * If you have multiple items in the cart, even when some of the items are too big, it will still 
 * give shipping method as long as there is any product that can be fitted into a box
 * 
 * @global type $wpsc_cart
 * @return boolean
 */
function titan_allow_shipping() {
     global $wpsc_cart;
     foreach( $wpsc_cart->cart_items as $cart_item ) {
            if( !titan_is_austpost_shippable( $cart_item->meta[0] ) ) {
                    return false;
            }
     }
     return true;
}


/**
 * Check if the item is too big or too heavy for aust post
 * @param type $meta
 * @return boolean
 */
function titan_is_austpost_shippable( $meta ) {
        if( !$meta ) //no data
            return true;
        
        $dimensions     = $meta['dimensions'];
        $weight         = $dimensions['weight'];
        $height         = $dimensions['height'];
        $height_unit    = $dimensions['height_unit'];
        $width          = $dimensions['width'];
        $width_unit     = $dimensions['width_unit'];
        $length         = $dimensions['length'];
        $length_unit    = $dimensions['length_unit'];
        
        //convert to kg/cm
        $weight = $weight * 0.453592;
        if ( $height > 0 && $height_unit == 'in'  ) {
                $height = (float) $height * 2.54;
        }
        if ( $height > 0 && $height_unit == 'meter'  ) {
                $height = (float) $height * 100;
        }
        
        if ( $width > 0 && $width_unit == 'in'  ) {
                $width = (float) $width * 2.54;
        }
        if ( $width > 0 && $width_unit == 'meter'  ) {
                $width = (float) $width * 100;
        }
        
        if ( $length > 0 && $height_unit == 'in'  ) {
                $length = (float) $length * 2.54;
        }
        if ( $length > 0 && $length_unit == 'meter'  ) {
                $length = (float) $length * 100;
        }
       
        //max length limit for aust post is 105cm
        if( $length > 105 || $width > 105 || $height > 105 ) {
                return false;
        }
        
        //max weight limit for aust post 22kg
        if( $weight > 22 ) {
                return false;
        }
        
        //Maximum dimensions for aust post is 0.25 cubic metres
        $cubic_meter = ( $length/100 ) * ( $width/100 ) * ( $height/100 );
        if( $cubic_meter > 0.25 ) {
                return false;
        }
        
        return true;
}


#============filters for fixing the menu order

add_filter('manage_edit-wpsc-product_columns' , 'add_pond_product_columns',11);
function add_pond_product_columns($columns) {
    return array_merge($columns, 
              array('menu_order' => __('Product Sort')));
}

add_action( "wpsc_manage_products_column_menu_order",'pond_custom_column_data', 10,3);

function pond_custom_column_data($post, $post_id, $is_parent )
{
    echo $post->menu_order;
}

add_filter('wpsc_register_post_types_products_args','pond_wpsc_register_post_types_products_args',10,1);

function pond_wpsc_register_post_types_products_args($args)
{
    $args['supports'] = array_merge($args['supports'],array('page-attributes'));
    return $args;
}

#========end #