<?php
	// Setup globals
	// @todo: Get these out of template
	global $wp_query;
	// Setup image width and height variables
	// @todo: Investigate if these are still needed here
	$image_width  = get_option( 'single_view_image_width' );
	$image_height = get_option( 'single_view_image_height' );
?>
<div id="single_product_page_container">

	<?php
		//outputs the wp e-commerce breadcrumb
		wpsc_output_breadcrumbs();
		//Plugin hook for adding things to the top of the products page, like the live search
		do_action( 'wpsc_top_of_products_page' );
	?>

	<div class="single_product_display group margin_top_20">

		<?php
		//!!START PRODUCT LOOP HERE
		while ( wpsc_have_products() ) : wpsc_the_product(); ?>
		<div class="clearfix product_information width_100 padding_bottom_20">
		<div class="clearfix left images margin_right_15">
			<div class="imagecol">
				<?php
					//if free shipping
					$product_meta = get_post_meta( wpsc_the_product_id(), '_wpsc_product_metadata' );
					if( $product_meta[0]['no_shipping'] == 1) {
						$style="style='top: 41px;'";				
					?>
					<div class="el-no-shipping-product" style="top: 12px; <?php echo $display; ?>"></div>
					<?php }	?>
				
				<?php					

					//!!MARKER ITEM IS ON SALE
					if(wpsc_product_on_special()) :get_post_thumbnail_id()
				?>
					<div class="wpsc_sale_item_single" <?php echo $style; ?>></div>
				<?php endif; ?>
				<?php if ( wpsc_the_product_thumbnail() ) : ?>
					<a rel="<?php echo wpsc_the_product_title(); ?>" class="fancy-product image-holder" href="<?php echo wpsc_the_product_image(); ?>" title="<?php echo wpsc_the_product_title(); ?>" >
						<img class="product_image" id="product_image_<?php echo wpsc_the_product_id(); ?>" alt="<?php echo wpsc_the_product_title(); ?>" title="<?php echo wpsc_the_product_title(); ?>" src="<?php echo wpsc_the_product_image();?>" />
					</a>
				<?php else: ?>
					<a>
						<img class="no-image image-holder" id="product_image_<?php echo wpsc_the_product_id(); ?>" alt="No Image" title="<?php echo wpsc_the_product_title(); ?>" src="<?php bloginfo('template_url'); ?>/wpsc-noimage.gif" width="<?php echo $image_width; ?>" height="<?php echo $image_height; ?>" />
					</a>
				<?php endif; ?>
				</div><!--close imagecol-->
				<ul class="wpsc_product_gallery">
				<?php
					//$page_content = get_the_content(null,0);
					$thumb_image_arr = wp_get_attachment_image_src( get_post_thumbnail_id() );
					$thumb_image = $thumb_image_arr[ 0 ];
					$images = get_children( array(
					'post_parent' => get_the_ID(),
					'post_type' => 'attachment',
					'post_mime_type' => 'image',
					'order' => 'ASC',
					'orderby' => 'menu_order',
					'numberposts' => -1
					) );
					if( $images ) {
						foreach( $images as $image ) {
							$image_name = substr( $image->guid, strrpos( $image->guid, '/' ) + 1 );
							$image_name = substr( $image_name, 0, -4 );

							$gallery_thumb_array = wp_get_attachment_image_src( $image->ID, 'product-gallery-thumb' );
							$gallery_thumb_img = $gallery_thumb_array[ 0 ];

							if( strpos( $page_content, $image_name ) !== false ) {
							continue;
							}
							if ( strpos( $thumb_image, $image_name ) !== false ) {
							continue;
							}
                ?>
                <li class='wpsc_gallery_images'><a rel="<?php echo wpsc_the_product_title(); ?>" href="<?php echo wp_get_attachment_url( $image->ID ); ?>" class="fancy-product" title="<?php the_title(); ?>"><img src="<?php echo $gallery_thumb_img; ?>" alt=""></a></li>
                <?php
						}
					}
                ?>
                </ul>
			</div>
			<div class="clearfix left product_content">
			<div class="single-product-title product-title"><?php echo wpsc_the_product_title(); ?></div>
            <?php //!!PRODUCT SKU/CODE ?>
                <?php if ( wpsc_product_sku( wpsc_the_product_id())) { getProductCode(); } ?>

				<?php
				//!!IF PRICE IS NOT ZERO
				if (get_post_meta(wpsc_the_product_id(), '_wpsc_price', true)  >  0) {

					if(wpsc_product_is_donation()) { getDonationData(); }
					else { ?>
						<div class="price_display" id="product_price_<?php echo wpsc_the_product_id(); ?>"> <?php
							//!!IF THE ITEM IS ON SPECIAL
							if(wpsc_product_on_special()) { getOldPrice(); }
							if(wpsc_have_variation_groups()) { getVariationPrice(); }
							else { ?> <span class="price currentprice"><?php echo wpsc_the_product_price(); ?></span> <?php echo '<div class="gst"><span>incl GST </span></div>'; //getGST(); ?> <?php }
						?>
						</div> <?php
						//multi currency code
						if(wpsc_product_has_multicurrency()) { echo wpsc_display_product_multicurrency(); }

						//!!IF SHOW SHIPPING
						if(wpsc_show_pnp()) { getShippingPrice(); }

					}
				}
				elseif (wpsc_have_variation_groups()) { getVariationPrice(); }
				else { //!!IF PRICE IS ZERO ?>
					<span class="noprice"><em>Call for pricing</em></span> <?php
				}
				?>

            <div class="single-addcart left">
				<!-- Form data -->
				<form class="product_form" enctype="multipart/form-data" action="<?php echo wpsc_this_page_url(); ?>" method="post" name="1" id="product_<?php echo wpsc_the_product_id(); ?>">
					<div class="clearfix width_100 order_wrapper">
						<?php
							/*** Cart Options ***/
							/*
							* This to check whethere the product is for pick-up only
							* if condition is true then text "In store pick-up only" will display
							* otherwise the Add Cart button will display
							*/
							$store_pick_data = get_post_meta(wpsc_the_product_id(), 'store_pick-up_only', true);

							 do_action ( 'wpsc_product_form_fields_begin' );

							/** PRODUCT CUSTOM TEXT **/
							if ( wpsc_product_has_personal_text() ) { getCustomText(); }

							/** PRODUCT CUSTOM FILE **/
							if ( wpsc_product_has_supplied_file() ) { getCustomFile(); }

							/*** PRODUCT QUANTITY ***/
							if($store_pick_data != "Yes") {
								if(wpsc_has_multi_adding()) { getQuantity(); }
							}

							/*** PRODUCT VARIATION ***/
							if (wpsc_have_variation_groups()) {
                                                            getVariation();
                                                        }

							/** PRODUCT STOCK **/
							if(wpsc_show_stock_availability()) { ?>
								<div class="wpsc_product_price"> <?php
								if(wpsc_product_has_stock()) { ?>
									<div id="stock_display_<?php echo wpsc_the_product_id(); ?>" class="in_stock"><?php _e('Product in stock', 'wpsc'); ?></div> <?php
								}
								else {  ?>
									<div id="stock_display_<?php echo wpsc_the_product_id(); ?>" class="out_of_stock"><?php _e('Product not in stock', 'wpsc'); ?></div> <?php
								}
								?>
								</div> <?php
							}

							if($store_pick_data != "Yes") {
								?>
								<input type="hidden" value="add_to_cart" name="wpsc_ajax_action" />
								<input type="hidden" value="<?php echo wpsc_the_product_id(); ?>" name="product_id" /> <?php
							}

							if( wpsc_product_is_customisable() ) { ?>
								<input type="hidden" value="true" name="is_customisable"/> <?php
							}

							if(wpsc_product_external_link(wpsc_the_product_id()) != '') { ?>
								<div class="wpsc_external_link">
									<?php $action = wpsc_product_external_link( wpsc_the_product_id() ); ?>
									<a href="<?php echo $action; ?>" title="<?php echo wpsc_product_external_link_text(); ?>" target="_blank"><?php echo wpsc_product_external_link_text(); ?></a>
								</div> <?php
							}	              

							if($store_pick_data == "Yes") {
								echo '<div class="el-store-pick-container"><span>In store pick-up only, please enquire below whether shipping is available to your area</span></div>';
							}
							else {
								if((get_option('hide_addtocart_button') == 0) &&  (get_option('addtocart_or_buynow') !='1')) {
									getAddCartButton();
								}
							}
						?>
                            <div class="contact_pond_over_weight"><strong>Due to the size of your order, please contact us for an accurate shipping quote on 08 9250 6063 or <a href="/contact-us/">Email</a> for freight options and costs</strong></div>
                            <div class="contact_pond_store_pick_up_only"><strong>In store pick-up only, please enquire below whether shipping is available to your area</strong></div>
					</div>
					<?php do_action ( 'wpsc_product_form_fields_end' ); ?>
				</form>
				<!--close product_form-->
				<?php
					//!!SHOW FB LIKE
					if(wpsc_show_fb_like()) { showFacebookLike(); }

					//!!SHOW/HIDE ADD TO CART/BUY NOW BUTTON
					if ( (get_option( 'hide_addtocart_button' ) == 0 ) && ( get_option( 'addtocart_or_buynow' ) == '1' ) ) {
						echo wpsc_buy_now_button( wpsc_the_product_id() );
					}

					//!!PRODUCT RATING
					echo "<div id='wpsc_product_rating'>" .wpsc_product_rater() . "</div>";

					//!!ALSO BOUGHT
					echo wpsc_also_bought( wpsc_the_product_id() );

					//SHARETHIS
					if ( get_option( 'wpsc_share_this' ) == 1 ) { ?>
						<div class="st_sharethis" displayText="ShareThis"></div><?php
					}
				?>
			</div>
			<!--close single-addcart -->

			<div class="clear"></div>

			<form onsubmit="submitform(this);return false;" action="<?php echo wpsc_this_page_url(); ?>" method="post" name="product_<?php echo wpsc_the_product_id(); ?>" id="product_extra_<?php echo wpsc_the_product_id(); ?>">
				<input type="hidden" value="<?php echo wpsc_the_product_id(); ?>" name="prodid"/>
				<input type="hidden" value="<?php echo wpsc_the_product_id(); ?>" name="item"/>
			</form>
		</div>
		<div class="productcol width_100">

				<?php //START HOOK FOR BEFORE DESCRIPTION ?>
				<?php do_action('wpsc_product_before_description', wpsc_the_product_id(), $wp_query->post); ?>



				<?php //PRODUCT DESCRIPTION ?>
				<div class="product_description margin_top_10">
				<div class="single-product-descrition title">Description</div>
				<div class="product-details clearfix margin_top_10">
					<?php echo wpsc_the_product_description(); ?>
					</div>
				</div><!--close product_description -->

                <?php // SEO quick edit link ?>
                <?php edit_post_link(__('{Quick Edit}'), ''); ?>

				<?php //START HOOK FOR PRODUCT ADDONS ?>
				<?php do_action( 'wpsc_product_addons', wpsc_the_product_id() ); ?>

				<?php //IF THERE IS PRODUCT ADDITIONAL DESCRIPTION ?>
				<?php if ( wpsc_the_product_additional_description() ) : ?>
					<div class="single_additional_description">
						<p><?php echo wpsc_the_product_additional_description(); ?></p>
					</div><!--close single_additional_description-->
				<?php endif; ?>

				<?php //START HOOK FOR AFTER DESCRIPTION ?>
				<?php do_action( 'wpsc_product_addon_after_descr', wpsc_the_product_id() ); ?>

				<?php /*** Custom meta HTML and loop ***/ ?>
				<?php if (wpsc_have_custom_meta()) : ?>
				<!--
				<div class="custom_meta">
					<?php while ( wpsc_have_custom_meta() ) : wpsc_the_custom_meta(); ?>
						<strong><?php //echo wpsc_custom_meta_name(); ?>: </strong><?php //echo wpsc_custom_meta_value(); ?><br />
					<?php endwhile; ?>
				</div>
				-->
				<!--close custom_meta-->
				<?php endif; ?>

			</div> <!-- close productcol -->
		</div>
		<div class="clearfix width_100 margin_top_15 product_enquiry-wrapper padding_bottom_20">
		<div class='enquiry-title'>Product Enquiry Form</div>
		<div class="clearfix margin_top_15">
		<?php echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]'); ?>
		</div>
		</div>
		<?php  if(function_exists('wpsc_the_related_products')):?>
			<div class="clearfix related_products-wrapper margin_top_15 padding_bottom_20">
				<?php wpsc_the_related_products();?>
			</div>
		<?php endif; ?>

		<?php //if( function_exists( 'wpsc_the_related_products' ) ) wpsc_the_related_products(); ?>


		<?php
		endwhile;
		do_action( 'wpsc_theme_footer' );
		?>

	</div><!--close single_product_display-->
</div><!--close single_product_page_container-->

<?php
function getAddCartButton() {
	//!!THIS IS IF THERE IS STOCK
	if(wpsc_product_has_stock()) { ?>
		<div class="wpsc_buy_button_container wpsc-add-to-cart-button"><?php
			if (get_post_meta(wpsc_the_product_id(), '_wpsc_price', true)  !=  0) { getInputAddCartButton(); }
			elseif (wpsc_have_variation_groups()) { getInputAddCartButton(); } ?>
			<div class="wpsc_loading_animation" style="visibility: hidden;">
				<img title="Loading" alt="Loading" src="<?php echo wpsc_loading_animation_url(); ?>" />
				<?php _e('Updating cart...', 'wpsc'); ?>
			</div><!--close wpsc_loading_animation-->
		</div> 

		<p class="wpsc_soldout" style="display: none"><?php _e('This product is currently out of stock – Please enquire for next availability.', 'wpsc'); ?></p>
		<?php
	}
	else { //!!THIS IS IF THERE IS NO MORE STOCK
		?><p class="wpsc_soldout"><?php _e('This product is currently out of stock – Please enquire for next availability.', 'wpsc'); ?></p><?php
	}

} //end of function getAddCartButton()...

function getInputAddCartButton() { ?>
    <input type="submit" value="<?php _e('Add To Cart', 'wpsc'); ?>" name="Buy" class="wpsc_buy_button" id="product_<?php echo wpsc_the_product_id(); ?>_submit_button" />  
<?php
} //end of function.

function getVariation() { ?>
	<div id="wpsc_product_variation" class="wpsc_product_variation left">

		<div class="wpsc_variation_forms margin_top_5">
			<table>
			<?php while (wpsc_have_variation_groups()) : wpsc_the_variation_group(); ?>
				<tr><td class="col1"><label for="<?php echo wpsc_vargrp_form_id(); ?>"><?php echo wpsc_the_vargrp_name(); ?>:</label></td>
				<?php /** the variation HTML and loop */?>
				<td class="col2"><select class="wpsc_select_variation" name="variation[<?php echo wpsc_vargrp_id(); ?>]" id="<?php echo wpsc_vargrp_form_id(); ?>">
				<?php while (wpsc_have_variations()) : wpsc_the_variation(); ?>
                                        <option value="<?php echo wpsc_the_variation_id(); ?>" <?php echo wpsc_the_variation_out_of_stock(); ?>><?php echo wpsc_the_variation_name(); ?></option>
				<?php endwhile; ?>
				</select></td></tr>
			<?php endwhile; ?>
			</table>
		</div><!--close wpsc_variation_forms-->

	</div><?php
} //end of function..

function getQuantity() { ?>
	<div class="wpsc_quantity_update left">
		<div class="wpsc_quantity_title left"><?php _e('Qty', 'wpsc'); ?></div>
		<input type="text" id="wpsc_quantity_update_<?php echo wpsc_the_product_id(); ?>" name="wpsc_quantity_update" size="2" value="1" <?php if (get_post_meta(wpsc_the_product_id(), '_wpsc_price', true)  !=  0) { } else { echo "disabled=disabled"; } ?> />
		<input type="hidden" name="key" value="<?php echo wpsc_the_cart_item_key(); ?>"/>
		<input type="hidden" name="wpsc_update_quantity" value="true" />
	</div><!--close wpsc_quantity_update--><?php
} //end of function..

function getCustomFile() { ?>
	<div class="wpsc_custom_file left">
		<legend><?php _e( 'Upload a File', 'wpsc' ); ?></legend>
		<p><?php _e( 'Select a file from your computer to include with this purchase.', 'wpsc' ); ?></p>
		<input type="file" name="custom_file" />
	</div><?php
} //end of function..

function getCustomText() { ?>
	<div class="wpsc_custom_text left">
		<legend><?php _e( 'Personalize Your Product', 'wpsc' ); ?></legend>
		<p><?php _e( 'Complete this form to include a personalized message with your purchase.', 'wpsc' ); ?></p>
		<textarea cols='55' rows='5' name="custom_text"></textarea>
	</div><?php
} //end of function..

function getProductCode() { ?>
	<div class="wpsc_product_sku"><strong>Product Code:</strong> <?php echo wpsc_product_sku( wpsc_the_product_id() ) ?></div>
	<?php
} //end of function..

function getDonationData() { ?>
	<label for="donation_price_<?php echo wpsc_the_product_id(); ?>"><?php _e('Donation', 'wpsc'); ?>: </label>
	<input type="text" id="donation_price_<?php echo wpsc_the_product_id(); ?>" name="donation_price" value="<?php echo wpsc_calculate_price(wpsc_the_product_id()); ?>" size="6" />
	<?php
} //end of function..

function getOldPrice() { ?>
	<span class="oldprice" id="old_product_price_<?php echo wpsc_the_product_id(); ?>"><?php echo wpsc_product_normal_price(); ?></span> <?php   echo '<div class="gst"><span>incl GST </span></div>'; //getGST(); ?>
	<?php
}	//end of function..

function getShippingPrice() { ?>
	<div class="wpsc_you_save pricedisplay"><?php _e('Shipping', 'wpsc'); ?>:<span class="pp_price"><?php echo wpsc_product_postage_and_packaging(); ?></span> <?php  echo '<div class="gst"><span>incl GST </span></div>'; //getGST(); ?> </div>
	<?php
}	//end of function..

function getVariationPrice() { ?>
	<div class="price_display" id="product_price_<?php echo wpsc_the_product_id(); ?>">
		<span class="price currentprice"><?php echo wpsc_product_normal_price(); ?></span> <?php  echo '<div class="gst"><span>incl GST </span></div>'; //getGST(); ?>
	</div> <?php
}
function showFacebookLike() { ?>
	<div class="wpsc_fb_like">
		<iframe src="https://www.facebook.com/plugins/like.php?href=<?php echo wpsc_the_product_permalink(); ?>&amp;layout=standard&amp;show_faces=true&amp;width=435&amp;action=like&amp;font=arial&amp;colorscheme=light" frameborder="0"></iframe>
	</div>
	<?php
}	//end of function..

/*
function getGST() { 
	 echo '<div class="gst"><span>incl GST </span></div>';
}
*/
//return_weight( '8339', '301' );

?>
<script type="text/javascript">
jQuery(function() {
    
    jQuery('.wpsc_select_variation').on('change', function() {
        check_over_weight();
    });
    
    function check_over_weight() {
        
        if (jQuery('.wpsc_product_variation').length > 0) {

        	jQuery('.wpsc_buy_button_container').hide();
        	
            //Variable Product
            jQuery.ajax({
                type: "GET",
                url: wpsc_ajax.ajaxurl,
                dataType: 'json',
                data: {
	                action : 'return_weight',
	                ajax_request: true,
	                post_id : <?php global $post; print $post->ID; ?>,
	                variation : jQuery('.wpsc_select_variation').serialize()
                },
                async: false,
                success: function(data) {
            		//hide add to cart button if no stock
                    if( data.stock == 0 || data.over_weight == 'true') {
                    	jQuery('.wpsc_buy_button_container').hide();
                    	jQuery('.wpsc_soldout').show();

                    	if(data.over_weight == 'true')
                    		jQuery('.contact_pond_over_weight').show();

                    } else {
                    	if(data.error == 'false') {
                    		jQuery('.contact_pond_over_weight').hide();
                    		jQuery('.contact_pond_store_pick_up_only').hide();
                        	jQuery('.wpsc_buy_button_container').show();	
                        	jQuery('.wpsc_soldout').hide();
                    	}                    	
                    }

                    if(data.store_pick_up == 'true'){
                    	jQuery('.contact_pond_store_pick_up_only').show();
                    	jQuery('.wpsc_buy_button_container').hide();
                    }

                    if(data.no_shipping == 1) { //show free shipping
                    	jQuery('.el-no-shipping-product').show();
                    } else {
                    	jQuery('.el-no-shipping-product').hide();
                    }
            	}    
            });

			
        } else {
            //Single Product
            jQuery.ajax({
                type: "GET",
                url: wpsc_ajax.ajaxurl,
                dataType: 'json',
                data: {
                action : 'return_weight_single',
                ajax_request: true,
                post_id : <?php global $post; print $post->ID; ?>
                },
                success: function(data){

                    if (data.over_weight == 'true') {
                        jQuery('.contact_pond_over_weight').show();
                        jQuery('.wpsc_buy_button_container').hide();
                    } else {
                        jQuery('.contact_pond_over_weight').hide();
                        jQuery('.wpsc_buy_button_container').show();
                    }

                    if(data.store_pick_up == 'true'){
                    	jQuery('.contact_pond_store_pick_up_only').show();
                    	jQuery('.wpsc_buy_button_container').hide();
                    }

                }
            });
        }
       
    }
    
    check_over_weight();
    
});
</script>
<style>
    
    .contact_pond_over_weight {
        clear: both;
        display: none;
    }
    
    .wpsc_buy_button_container {
        display: none;
    }
</style>