<?php get_header(); ?>
<div class="main-content section-block-wrapper padding_left_18 left">
	<div id="sub-page" class="main-sub-page section-pad-wrapper">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<h1 class="page-title"><?php the_title(); ?></h1>
		<div class="latestNewsDate"><?php echo get_the_time('d-M-Y'); ?></div>
		<div class="clearfix margin_top_15 width_100">
			<?php if(has_post_thumbnail()) { ?>
				<div class="single-news-thumbnail right">
					<?php //echo the_post_thumbnail('product'); ?>
				</div>
			<?php } ?>
			<?php the_content(); ?>
		</div>
	<?php endwhile; else: ?>
		
	<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>