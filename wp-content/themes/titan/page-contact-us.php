<?php get_header(); ?>
	<div id="sub-page" class="main-content main-sub-page padding_left_18 left">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<h1 class="page-title"><?php the_title(); ?></h1>
		<?php the_content(); ?>
		<div class="contact-wrapper section-wrapper">
			<div class="section-pad-wrapper">
			<div class="contact-info width_100">
					<div class="contact-details section-pad-wrapper">
						<ul class="details section-pad-wrapper width_100">
							<li class="left">
								<h4>Phone</h4>
								<?php echo do_shortcode("[titan-business-details value=phone name=the-pond]") ?>								
							</li>
							<li class="left">
								
								<h4>Address</h4>
								<?php echo do_shortcode("[titan-business-details value=address-1 name=the-pond]") ?> <br />
								<?php echo do_shortcode("[titan-business-details value=suburb name=the-pond]") ?> <?php echo do_shortcode("[titan-business-details value=state name=the-pond]") ?> <?php echo do_shortcode("[titan-business-details value=postcode name=the-pond]") ?>
								<h4>Email</h4>
								<a href="mailto:<?php echo do_shortcode("[titan-business-details value=admin-email name=the-pond]") ?>" title="Email Us"><?php echo do_shortcode("[titan-business-details value=admin-email name=the-pond]") ?></a>	
								</li>
								<li class="right">
								<?php if(do_shortcode("[titan-business-details value=opening-mon-fri name=the-pond]")) { ?>
								<h4>Opening Hours</h4>
								<table width="100%">
									<tr>
										<td>Mon - Fri</td>
										<td><?php echo do_shortcode("[titan-business-details value=opening-mon-fri name=the-pond]") ?> </td>	
									</tr>
									<tr>
										<td>Sat</td>
										<td><?php echo do_shortcode("[titan-business-details value=opening-sat name=the-pond]") ?> </td>	
									</tr>
									
								</table>
								<?php } ?>
							</li>
							
						</ul>
					</div> <!-- contact-details -->
					<div class="google-maps section-block-wrapper">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3387.593718951346!2d116.03083111502694!3d-31.890462181248733!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2a32b80e858d9b2d%3A0xf308a77a54e62207!2sThe+Pond+Shop!5e0!3m2!1sen!2sid!4v1542011287598&key=AIzaSyAGsocAummajracEIkcPM7KwV1XY0UMRHE" width="705" height="230" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
					
				</div> <!-- contact-info -->
				<div class="contact-form margin_top_20">
					<div class="section-pad-wrappper">
					<?php gravity_form(1,false,false); ?>
					</div>
				</div>
				
			</div>
		</div> <!-- contact-wrapper -->
	<?php endwhile; else: ?>
		
	<?php endif; ?>
	</div> <!-- main-content -->
<?php get_footer(); ?>