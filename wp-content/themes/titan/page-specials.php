<?php get_header(); ?>
<div class="main-content section-block-wrapper padding_left_18 left">
	<div id="sub-page" class="main-sub-page section-pad-wrapper">
	<h1 class="page-title"><?php the_title(); ?></h1>
	<?php 
	
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

	$args = array(
		'post_type' => 'wpsc-product', 
		'orderby' => 'post_title', 'order' => 'ASC',
		'paged'=>$paged,
		'posts_per_page'=>-1,
		'post_status'     => 'publish',
		'meta_value'=>'0',
		'meta_compare' =>'!=',
		'meta_key'=>'_wpsc_special_price',
	);
	
	$loopb = new WP_Query( $args );
	
	?>
	<div class="margin_top_15 clearfix special_container width_100">
		<?php while ( $loopb->have_posts() ) : $loopb->the_post(); ?>
	<div class="special left margin_bottom_20 clearfix">
	<a class=" clearfix" href="<?php echo get_permalink()?>"><div class="product_name"><?php echo get_the_title();?></div>
			     <?php
			   		$product_meta = get_post_meta( wpsc_the_product_id(), '_wpsc_product_metadata' );
					if( $product_meta[0]['no_shipping'] == 1) {
						$style="style='top: 84px;'";
						echo '<div class="el-no-shipping-product" style="top: 57px;"></div>';
					}	
			    ?>
			    <?php if(wpsc_product_on_special( $post->ID)):?>
			    <div class="wpsc_sale_item" <?php echo $style; ?>></div>
			    <?php endif;?>
			    <div class="product_image_featured ">
			    <?php 
			    if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
			      the_post_thumbnail('product-listing');
			    } else {
			      echo '<div class="front-feat-img"><img src="';
			      bloginfo("template_url");
			      echo '/wpsc-noimage.gif" alt="" height="156" width="234" style="height:156px;width:234px;" /></div>';
			    }
			    
			  ?></div>
			  <div class="current-price"><?php 
			  $special=get_post_meta( $post->ID, '_wpsc_special_price', true );
			  
			  if($special>0):?><?php echo wpsc_currency_display(get_post_meta( $post->ID, '_wpsc_special_price', true ))?><?php else:?><?php if(get_post_meta( $post->ID, '_wpsc_price', true )>0 ):?><?php echo wpsc_currency_display(get_post_meta( $post->ID, '_wpsc_price', true ));?> <?php else:?><em>No Price Available</em><?php endif;?><?php endif;?></div>
			  <div class="price"><?php if(get_post_meta( $post->ID, '_wpsc_price', true )>0 && $special>0):?>was <?php echo wpsc_currency_display(get_post_meta( $post->ID, '_wpsc_price', true ));?><?php endif;?></div>
			  <div class="view_details"> View Details</div>
			  </a>
	</div>
	<?php  endwhile;?>
	<?php if($loopb->max_num_pages>1){ ?>
	<div class="page-nav-container clearfix">
		<div class="page-nav  margin_top_10 clearfix"><?php if($paged > 1):?><a href="<?php echo '/specials/'; ?>" title="First Page">« First</a><?php endif;?><?php if($paged > 1):?><a href="<?php echo '/specials/?paged=' . ($paged-1); ?>" title="Previous Page">&lt; Previous</a><?php endif;?><?php for($i=1; $i<=$loopb->max_num_pages; $i++): ?><?php if($paged == $i):?><span class="current"><?php echo $i;?></span><?php else:?><a href="<?php echo '/specials/?paged=' . $i; ?>" title="Page <?php echo $i?>"><?php echo $i;?></a><?php endif;?><?php endfor; ?><?php if($paged < $loopb->max_num_pages):?><a href="<?php echo '/specials/?paged=' .($paged+1); ?>" title="Next Page">Next &gt;</a><?php endif;?><?php if($paged < $loopb->max_num_pages):?><a href="<?php echo '/specials/?paged=' .$loopb->max_num_pages; ?>" title="Last Page">Last »</a><?php endif;?>
		</div>
		</div>
	<?php } ?>
	<?php wp_reset_query();	?>
	</div>
	</div>
</div>
<?php get_footer(); ?>