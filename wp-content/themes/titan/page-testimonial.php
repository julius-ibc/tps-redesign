<?php 
	get_header();
?>
<div class="main-content section-block-wrapper left">
	<div id="sub-page" class="main-sub-page section-pad-wrapper">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<h1 class="page-title"><?php the_title(); ?></h1>
		<?php the_content(); ?>
	<?php endwhile; ?>
	<?php
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$loopb = new WP_Query( array( 'post_type' => 'testimonial', 'posts_per_page' => 10, 'paged' => $paged ) );
	?>
	<?php while ( $loopb->have_posts() ) : $loopb->the_post(); ?>
		<div class="testimonialItem">
			<h3><?php the_title(); ?></h3>
			<div class="testimonialDate"><?php the_date(); ?></div>
			<div class="testimonialContent"><?php echo strip_tags(get_the_content()); ?></div>
		</div>
	<?php endwhile; ?>
	<?php
	if($loopb->max_num_pages>1){ ?>
		<div class="page-nav">
			<ul>
			<?php for($i=1; $i<=$loopb->max_num_pages; $i++){ ?>
				<li><a href="<?php echo '/testimonials/?paged=' . $i; ?>" <?php echo ($paged==$i)? 'class="selected"':'';?>><?php echo $i;?></a></li>
			<?php } ?>
			</ul>
		</div>
	<?php } ?>
    <?php wp_reset_query(); ?>
    </div>
</div> <!-- main-content -->
<?php get_footer(); ?>