<?php
if( is_front_page() ) {
   $pageClass = 'front-page';
} else {
   $pageClass = 'sub-page';
}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<!-- IBC -->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MTB7WKN');</script>
<!-- End Google Tag Manager -->
<!-- End IBC -->

<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<title><?php
	/*Print the <title> tag based on what is being viewed.*/
	global $page, $paged;
	wp_title( '|', true, 'right' );
	// Add the blog name.
	bloginfo( 'name' );
	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'titan' ), max( $paged, $page ) );
	?></title>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" type="image/x-icon"/>
<link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>/images/appleicon.png"/>
<?php wp_head(); ?>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/css/jquery.fancybox.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/css/jquery.jscrollpane.css" />
<!-- Google fonts -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow">
</head>
<?php flush(); ?>
<body <?php body_class(); ?>>
<!-- IBC -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MTB7WKN"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- IBC -->

<!--[if lte IE 6]>
<div id="ie6alert">
	<div><strong>Your browser (Internet Explorer 6) is no longer supported.</strong>
        <p>IE6 has since been superceded twice. Please upgrade to any of these free browsers</p>
        <ul>
            <li><a href="http://www.getfirefox.com" title="Firefox">Firefox</a></li>
            <li><a href="http://www.google.com/chrome" title="Chrome">Chrome</a></li>
            <li><a href="http://www.apple.com/safari/" title="Safari">Safari</a></li>
            <li><a href="http://www.microsoft.com/windows/internet-explorer/" title="Internet Explorer">Internet Explorer</a></li>
        </ul>
    </div>
</div>
<![endif]-->

<div class="outer-wrapper section-wrapper globalwidth_980">
	<div class="outer-pad section-pad-wrapper">
	<div class="header-top-group-wrapper  section-wrapper">
	<div class="header-top-group-outer  section-wrapper">
	
	
	
	
	
	<div class="header-top-group-inner section-wrapper  ">
	
    <div id="header" class="header-wrapper section-wrapper globalwidth">
      <div class="header-pad section-pad-wrapper">

        <div class="logo-wrapper section-block-wrapper left">
          <div id="logo" class="logo section-pad-wrapper">
            <a href="/" title="<?php bloginfo('name'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="<?php bloginfo('name'); ?>" /></a>
          </div>
        </div>
        
        <div class="headinfo-wrapper section-block-wrapper right">
          <div class="headinfo  section-pad-wrapper ">
          <div class="address margin_top_25 left" ><?php echo do_shortcode("[titan-business-details value=address-1 name=the-pond]") ?>, <?php echo do_shortcode("[titan-business-details value=suburb name=the-pond]") ?>, <?php echo do_shortcode("[titan-business-details value=state name=the-pond]") ?> <?php echo do_shortcode("[titan-business-details value=postcode name=the-pond]") ?></div>
          <div class="head_links ">
           <?php if ( is_user_logged_in() ) :   ?> 
           <?php 
           $current_user=wp_get_current_user();
          
           ?>
           <span>Hi, <?php echo $current_user->display_name?></span>
           <span class="my_account"><a href="/products/my-account">My Account</a></span>
           <span><a href="<?php echo wp_logout_url(get_bloginfo('url'));?>">Log Out</a></span>
           <?php else:?>
          <span><a href="/client-login">Client Login</a></span>
          <span class="my_account"><a href="/products/my-account">My Account</a></span>
          <span><a href="/sign-in">Sign In</a></span>
          <?php endif;?>
          </div>
         <div class="social-links clearfix margin_top_20 right"> 
			<?php 
			if ( $s_linkedin = do_shortcode("[titan-business-details value=social-linkedin name=the-pond]") ) {
			?>
              <div class="linkedin clearfix left margin_right_5">
				  <a target="_blank" href="<?php echo $s_linkedin ?>"></a>
			 </div>
              <?php 
			  }
			  if ( $_facebook = do_shortcode("[titan-business-details value=social-facebook name=the-pond]") ) {
			  ?>
              <div class="facebook clearfix left margin_right_5">
				  <a target="_blank" href="<?php echo $_facebook ?>"></a>
			 </div>
			  <?php
			  }
			  if ( $s_googleplus = do_shortcode("[titan-business-details value=cat_social_google-plus name=the-pond]") ) {
			  ?>
              <div class="googleplus clearfix left margin_right_5">
				  <a target="_blank" href="<?php echo $s_googleplus ?>"></a>
			 </div>
              <?php 
			  }
			  if ( $s_twitter = do_shortcode("[titan-business-details value=social-twitter name=the-pond]") ) {
			  ?>
              <div class="twitter clearfix left margin_right_5">
				  <a target="_blank" href="<?php echo $s_twitter ?>"></a>
			 </div>
              <?php 
			  } 
			  ?>
               <div class="phone margin_top_10 right" ><span>Telephone: </span><?php echo do_shortcode("[titan-business-details value=phone  name=the-pond]") ?></div>
           </div>
          
          
						
			
			
			
		
          </div>
        </div>
        <div class="header-search-wrapper clearfix">
			<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
 
  <input type="text" value="Type your search here" onfocus="if( this.value == this.defaultValue ) this.value = ''" onblur="if( this.value == '' ) this.value = this.defaultValue" name="s" id="s" />
  <button type="submit" id="searchsubmit" name="searchsubmit"></button>
</form>
			</div>		
        

      </div>
    </div>
	<div class="menu-wrapper main-menu section-wrapper ">
      <div class="menu-pad section-wrapper  ">
      <?php
        wp_nav_menu( array( 'menu' => 'Top Menu', 'container_class' => 'top-menu section-wrapper clearfix', 'container_id' => 'top-menu', 'theme_location' => 'primary' ) );
      ?>
      </div>
    </div>
 	<div class="sidebar_extra left"></div>
	
    </div>
   
    
    </div>
    </div>
    
    <div class="mid-wrapper section-wrapper ">
    <div class="mid-group-wrapper  section-wrapper">
	<div class="mid-group-outer  section-wrapper">
	

	
      <div class="mid-pad section-wrapper    ">

        <div id="content" class="content-wrapper section-wrapper left  width_100">
          <div class="content-pad section-pad-wrapper width_100 ">

            <div class="content <?php echo $pageClass; ?> width_100 ">
            <?php get_sidebar();?>