<?php get_header(); ?>
<div class="main-content section-block-wrapper padding_left_18 left">
	<div id="sub-page" class="main-sub-page section-pad-wrapper">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<h1 class="page-title"><?php the_title(); ?></h1>
		<div class="clearfix margin_top_15 width_100">
		<?php 
		
		$thumb_image_arr = wp_get_attachment_image_src(get_post_thumbnail_id(),'page');
		
	$thumb_image = $thumb_image_arr[0];
		
	global $post;
	?>	<?php if($thumb_image && $post->post_type !="wpsc-product"):?>
	<img src="<?php echo $thumb_image?>" class="right page-featured" alt="<?php the_title()?>"/>
	<?php else:?>
	<?php 
	
	
	//custom_product_breadcrumbs();
	
	
		//custom_taxonomy_breadcrumbs()?>
	<?php endif;?>
		<?php the_content(); ?>
		</div>
	<?php endwhile; else: ?>
		
	<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>