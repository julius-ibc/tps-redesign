<?php 
		if(!is_user_logged_in()):
		wp_redirect(get_bloginfo('url')."/sign-in"); 
		
		exit;
		endif;
?>
<?php get_header(); ?>
<div class="main-content section-block-wrapper padding_left_18 left">
	<div id="sub-page" class="main-sub-page section-pad-wrapper">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<h1 class="page-title"><?php the_title(); ?></h1>
		<div class="clearfix margin_top_15 width_100">
		<?php the_content(); ?>
		
		</div>
	<?php endwhile; else: ?>
		
	<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>