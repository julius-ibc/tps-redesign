<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
  <label class="screen-reader-text" for="s">Search for:</label>
  <input type="text" value="Search" onfocus="if( this.value == this.defaultValue ) this.value = ''" onblur="if( this.value == '' ) this.value = this.defaultValue" name="s" id="s" />
  <button type="submit" id="searchsubmit" name="searchsubmit">Search</button>
</form>