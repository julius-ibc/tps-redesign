<?php get_header(); ?>
<div class="main-content section-block-wrapper padding_left_18 left">
	<div id="sub-page" class="main-sub-page section-pad-wrapper">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<h1 class="page-title"><?php the_title(); ?></h1>
		<div class="breadcrumbs margin_top_10"><a href="<?php bloginfo('url');?>">Home</a> &nbsp;<span class="arrow">»</span>&nbsp; <a href="<?php bloginfo('url');?>/help-centre">Help Centre</a> &nbsp;<span class="arrow">»</span>&nbsp; <span class="current"><?php the_title();?></span> </div>
		<div class="clearfix margin_top_15 width_100">
		
		<?php the_content(); ?>
		</div>
		<div class="pdf_download_container margin_top_15 ">
			<?php 			
				$pdf_files=get_field('pdf_download_repeater',$post->ID);
				
			?>
			<ul>
				<?php foreach($pdf_files as $pdf_file):?>
				<?php if($pdf_file['pdf_download']['url'] != '' &&  $pdf_file['pdf_download']['title'] != '') { ?>
				<li><div class="download_link"><span class="pdf_bg"></span><span class="pdf_content"><a target="_blank" href="<?php echo $pdf_file['pdf_download']['url']?>"><?php echo $pdf_file['pdf_download']['title']?></a></span></div></li>
				<?php } ?>
			<?php endforeach;?>
			</ul>
			
		</div>
		<div class="pdf_download_container margin_top_15 ">
			<?php 
			
				$pdf_files=get_field('info_sheet_download_repeater',$post->ID);		
				
			?>
			<ul>
				<?php foreach($pdf_files as $pdf_file):?>
				<?php if($pdf_file['info_sheet_download']['url'] != '' &&  $pdf_file['info_sheet_download']['title'] != '') { ?>
				<li>
					<div class="download_link clearfix">
						<span class="pdf_bg"></span>
						<span class="pdf_content">Info Sheet for <a class="" href="<?php echo $pdf_file['info_sheet_download']['url']?>" target="_blank">
							<?php echo $pdf_file['info_sheet_download']['title']?></a>
						</span>
					</div>
				</li>
				<?php } ?>
			<?php endforeach;?>
			</ul>
			
		</div>
	<?php endwhile; else: ?>
		
	<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>