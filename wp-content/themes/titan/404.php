<?php 
//Default 404 Page.
get_header();
?>
<div class="main-content section-block-wrapper padding_left_18 left">
	<div id="sub-page" class="main-sub-page section-pad-wrapper">
		<h1 class="page-title">Page Not Found</h1>
		<div class="error404_msg margin_top_15">
			<p>Sorry, but the page you are looking for has not been found. Try checking the URL for errors, then click the refresh button on your browser.</p><p>You could go back to our <a href="/" title="<?php bloginfo('name'); ?>">homepage</a>, <strong>OR</strong> you could try to search our site.</p><br />
			<?php //get_search_form(); ?>
		</div><!-- .error404_msg -->
		
	</div>
</div>
<?php get_footer(); ?>