            </div><!-- /content -->

          </div><!-- /content-pad -->
        </div><!-- /content-wrapper -->
	
		
      </div><!-- /mid-pad -->
      
      	
      </div><!-- /mid-group-outer-->
      </div><!-- /mid-group-wrapper -->
    </div><!-- /mid-wrapper -->
	<?php get_footer('menu_group');?>

    <div id="footer" class="footer-wrapper section-wrapper ">
    <div class="footer-group-outer  section-wrapper">
	
	
	
	
      <div class="footer-pad   footer-group-inner section-wrapper  ">

        <div class="section-block-wrapper   clearfix ">
          <div class="footer clearfix">

            <div class="titan-wrapper section-block-wrapper ">
              <div class="titan-badge clearfix">
                <div class="titan-title">Website design by </div><a href="http://www.titanweb.com.au/" title="Titanweb Design Perth" class="titan-logo" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/titanweb-logo-light.png " alt="Titanweb Design Perth" /></a>
              </div>
            </div>

            <div class="copyright-wrapper section-block-wrapper left">
              <div class="copyright">
               &copy;  <?php echo date('Y'); ?> <?php bloginfo("name"); ?>.
              </div>
            </div>

            <div class="menu-wrapper footer-menu-wrapper section-block-wrapper left">
              <?php wp_nav_menu( array( 'menu' => 'Footer Menu', 'container_class' => 'footer-menu', 'container_id' => 'footer-menu', 'theme_location' => 'footer-nav' ) ); ?>
            </div>

          </div>
        </div>

      </div><!-- /footer-pad -->
    
	</div>
    </div><!-- /footer -->
   
  </div><!-- /outer-pad -->
</div><!-- /outer-wrapper -->

<?php wp_footer(); ?>
<?php
$url = explode('/', $_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI]);
?>
<?php if( $url[1] == 'project-gallery' || $url[1] == 'our-showroom') : ?>


<!-- comment out which js option you want to use for the banner slide
<script type="text/javascript" src="<?php // bloginfo('template_url'); ?>/js/jquery.carouFredSel-6.2.0-packed.js"></script>
-->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="<?php  bloginfo('template_url'); ?>/js/jquery.cycle.js"></script>

<script src="<?php  bloginfo('template_url'); ?>/js/alertbox/mootools.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php  bloginfo('template_url'); ?>/js/alertbox/sexyalertbox.css" type="text/css" media="all" />
<script src="<?php  bloginfo('template_url'); ?>/js/alertbox/sexyalertbox.packed.js" type="text/javascript"></script>
<script src="<?php  bloginfo('template_url'); ?>/js/jquery.read.slide.more.js" type="text/javascript"></script>

<?php else: ?>

<!-- Scripts -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.fancybox.pack.js"></script>
<!-- comment out which js option you want to use for the banner slide
<script type="text/javascript" src="<?php // bloginfo('template_url'); ?>/js/jquery.carouFredSel-6.2.0-packed.js"></script>
-->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="<?php  bloginfo('template_url'); ?>/js/jquery.cycle.js"></script>

<script src="<?php  bloginfo('template_url'); ?>/js/alertbox/mootools.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php  bloginfo('template_url'); ?>/js/alertbox/sexyalertbox.css" type="text/css" media="all" />
<script src="<?php  bloginfo('template_url'); ?>/js/alertbox/sexyalertbox.packed.js" type="text/javascript"></script>
<script src="<?php  bloginfo('template_url'); ?>/js/jquery.read.slide.more.js" type="text/javascript"></script>

<?php endif; ?>

<script type="text/javascript">
var jq = jQuery.noConflict();
jq(document).ready(function() {
	//pagination fixes
	 if(jq('.wpsc_paging_bottom').length) {
	    jq('.wpsc_paging_bottom a').each(function() {
	      title = jq(this).attr("title");
	      if(title == 'First Page' || title == 'Last Page')
	           jq(this).remove();

	      if(title == 'Next Page') {
	        jq(this).html("&raquo;");
	        jq(this).addClass('next');
	      }

	      if(title == 'Previous Page') {
	        jq(this).html("&laquo;");
	        jq(this).addClass('prev');
	      }

	    });

	    jq('.wpsc_paging_bottom').html(jq('.wpsc_paging_bottom').html().replace('Pages:',''));
	  }

	//hide text under Calculate Shipping Price
	if(jq('.wpsc_shipping_info').length > 0) {
		jq(jq('.wpsc_shipping_info')[0].nextSibling).wrap('<span style="display:none"></style>');
		jq('.wpsc_shipping_info').next().next().hide();	
		jq(".australiapost_0").after('<tr><td>Please allow an extra day for handling</td></tr>')
	}
	

	jq(".fancy-video").fancybox({
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'type'				: 'iframe',
		'scrolling'   		: 'no'	
	});
	jq(".fancy-product, .ngg-gallery-thumbnail a").fancybox({
		nextEffect	: 'fade',
		prevEffect	: 'fade'
	});



	jq(".banner-slide").cycle({
		fx:'fade',
		speed:370,
		timeout:4000,
		height:434,
		fit:true,
		width:708,
		cleartypeNoBg:true,
		pager:  '.banner-nav'
	});

	if( jq('.featured-products').length >0 ){
		
		//Leadins Product Cat
		$child = jq('.featured-product-container').children().length * 186;
		//console.debug($child);
		jq('.featured-product-container').css('width', $child + 'px');
		jq('.featured-products').jScrollPane({autoReinitialise: true,horizontalDragMaxWidth:1000});
		
		if( jq('.jspDrag').length>0){
			
			
			jq('.j_nav').width(jq('.jspDrag').width()-30);
			
			
		}
	}
	jq('.sidebar-product-categories ul li a').prepend("</span><span>");	
	/* basic code depending on which js query you are using
		jq(".banner-slide").cycle({
			fx:'fade',
			speed:370,
			timeout:4000,
			cleartypeNoBg:true
		});
		
	*/

	jq('.wpsc-breadcrumbs a:first-child').text('Home');
    
    <?php if( $url[1] == 'products' && $url[2] == 'checkout' ) : ?>
    jq('#australiapost_0').click();
	jq('#australiapost_0').prop('checked', true);
    <?php endif; ?>
	
	jq('.header-search-wrapper #searchsubmit').hover(function(){
			jq('.header-search-wrapper').addClass('header-search-wrapper-over');
		},function(){
			jq('.header-search-wrapper').removeClass('header-search-wrapper-over');
		});

	if(jq('.category_thumb_img').length>0){
		jq.each(jq('.category_thumb_img'),function(index,val){
			if(jq(this).html().length == 0)
			{
				jq(this).remove();
			}else{
				jq(this).parent().find('.category-noimg').remove();
			}
			
			
		});
	}
	if(jq('.wpsc_category_grid_item').length>0){
		jq.each(jq('.wpsc_category_grid_item'),function(index,val){
			if(jq(this).html().length == 0)
			{
				jq(this).remove();
			}
			
			
		});
	}
	if(jq('.wpsc_category_decription').length>0){
		jq.each(jq('p'),function(index,val){
			if(jq(this).html().length == 0)
			{
				jq(this).remove();
			}
			
			
		});
	}

	if(jq('.wpsc_main_category_wrap ').length>0){
		var ctr=0;

		jq.each(jq('.wpsc_main_category_wrap '),function(index,val){
			if(++ctr%5 == 0)
			{
				
				jq(this).addClass('margin_right_0');
			}
			
			
		});
	}
	if(jq('.default_product_display ').length>0){
		var ctr=0;

		jq.each(jq('.default_product_display '),function(index,val){
			if(++ctr%4 == 0)
			{
				
				jq(this).addClass('margin_right_0');
			}
			
			
		});
	}
	if(jq('.related_product ').length>0){
		var ctr=0;

		jq.each(jq('.related_product '),function(index,val){
			if(++ctr%4 == 0)
			{
				jq(this).addClass('margin_right_0');
			}
			
			
		});
	}
	if(jq('.special ').length>0){
		var ctr=0;

		jq.each(jq('.special '),function(index,val){
			if(++ctr%4 == 0)
			{
				
				jq(this).addClass('margin_right_0');
			}
			
			
		});
	}
	if(jq('.wpsc_categories').length>0){
		
		jq('.wpsc_categories > p').remove();
		jq('.wpsc_categories .wpsc_main_category_wrap .wpsc_category_grid_item  > p').remove();
	}
	if(jq('.wpsc_gallery_images').length>0){
	var ctr=0;
	jq.each(jq('.wpsc_gallery_images'),function(index,val){
		if(++ctr%4 == 0)
		{
			jq(this).addClass('margin_right_0');
		}
		if(ctr>4){
			jq(this).addClass('margin_top_5');
		}
		
	});

	}
	
	if(jq('.gform_body').length>0){
		jq('.gform_body input[type="text"]').focusin(function(){
			
			jq(this).parent().parent().find('.gfield_label').addClass('gfield_label_focusin');
			
			jq(this).css('border','1px solid #127cc0');
		});
		jq('.gform_body input[type="text"]').focusout(function(){
			jq(this).css('border','1px solid #9dbfe3');
			jq(this).parent().parent().find('.gfield_label').removeClass('gfield_label_focusin');
		});
		jq('.gform_body textarea').focusin(function(){
			jq(this).css('border','1px solid #127cc0');
			jq(this).parent().parent().find('.gfield_label').addClass('gfield_label_focusin');
		});
		jq('.gform_body textarea').focusout(function(){
			
			jq(this).css('border','1px solid #9dbfe3');
			jq(this).parent().parent().find('.gfield_label').removeClass('gfield_label_focusin');
		});
	}
	
	if(jq('.gform_footer').length >0){
		//jq('.gform_footer').addClass('clearfix');
	}
	if(jq('.gform_body').length >0){
		//jq('.gform_body').addClass('clearfix');
	}
	// if(jq('.page-nav').length>0){
	// 	jq('.page-nav').children().last().css('border-right','0');
	// }
	// if(jq('.ngg-navigation').length>0){
	// 	jq('.ngg-navigation').children().last().css('border-right','0');
	// }
	if(jq('.ngg-navigation .next').parent().length >0){
		jq('.ngg-navigation .next').text('Next');
	}
	if(jq('.ngg-navigation .prev').parent().length >0){
		jq('.ngg-navigation .prev').text('Prev');
	}
	
	// Front page read more
	jq('.front-page-content').titanSlideMore(4);
	
	if( jq.browser.msie){
		if(parseInt(jq.browser.version,10) == 7){
			jq('.sidebar-product-categories ul li').css('padding-left', 0);
			jq('.sidebar-product-categories ul li').css('margin-left', -16);
			jq('.sidebar-product-categories ul li').css('width', 233);
			jq('.sidebar-product-categories ul li a').css('width', 200);
			jq('.sidebar-product-categories ul li a').css('margin-left', 10);
			jq('.sidebar-product-categories').css('margin-top', -1);
			jq('.pdf_download_container ul li').css('margin-left', -15);
			jq('#searchform input[type="text"]').css('padding-top', 7);
			jq('#searchform input[type="text"]').css('height', 22);			
		}else if(parseInt(jq.browser.version,10) == 8){

		}
	}
	jq.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase()); 
	if(jq.browser.chrome){
		jq('.outer-wrapper').css('left','-1px');
	}	
	
	
});

/* for Add Cart alert box (wpsc_buy_button) */
jq('.wpsc-add-to-cart-button input[type="submit"], .wpsc_buy_button').click(function() {
    console.log('test');
    jq('.wpsc_loading_animation').attr("style", "visibility: visible");
    jq('#fancy_notification').attr("style", "visibility: visible");
    niceAlertBox();
});

/* Remove text 'from' */
jq('span.el-sale-price').each(function() {
    var textReplace = jq(this).text();
    jq(this).text(textReplace.replace('from', '')); 
});
/* Remove text */
jq('div.wrap p').each(function() {
    var textReplace = jq(this).text();
    jq(this).text(textReplace.replace(", any items that can be downloaded can be downloaded using the links on this page.", ". ")); 
});
/* Replace text 'zipcode' to 'postcode' */

window.addEvent('domready', function() {
    Sexy = new SexyAlertBox();
});
function niceAlertBox() {
    
    setTimeout(function(){
        
        
        jq.ajax({
                type: "GET",
                url: wpsc_ajax.ajaxurl,
                dataType: 'json',
                data: {
                action : 'theme_cart_update_custom'
            },
            success: function(data){

                jq("#cart-item-count .highlight-items").text(data.cart_count);
                jq("#cart-total .pricedisplay").html(data.total);
                
                Sexy.alert('<br>Item added to cart. <a href="<?php echo site_url(); ?>/products/checkout">Click here</a> to checkout.');
                jq('.wpsc_loading_animation').attr("style", "visibility: hidden");
                jq('#fancy_notification').attr("style", "visibility: hidden");
                
            }

    	});
        
        
        
        
        
    },1000);
	
        
}

</script>
</body>
</html>