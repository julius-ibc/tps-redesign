<?php 
/*
Template Name: News Template
*/
get_header(); 

?>
<div class="main-content section-block-wrapper padding_left_18 left">
	<div id="sub-page" class="main-sub-page section-pad-wrapper">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<h1 class="page-title"><?php the_title(); ?></h1>
		<div class="clearfix margin_top_15 width_100">
			<?php the_content(); ?>
		</div>
	<?php endwhile; else: ?>
	
	<?php endif; ?>
	
	<?php
		$i_paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$args = array('post_type' => 'news-list', 'orderby' => 'date', 'order' => 'DESC','paged'=>$i_paged, 'posts_per_page'=>10);
		$loopb = new WP_Query( $args );
	?>

	<div class="parent_container"> <!-- margin_top_20 -->
	<?php if($loopb->have_posts()): ?>
		<?php while ( $loopb->have_posts() ) : $loopb->the_post(); ?>
			<div class="news-item">
				<h3 class="page-title"><?php the_title(); ?></h3>
				<div class="latestNewsDate"><?php echo get_the_time('d-M-Y'); ?></div>
				<div class="latestNewsContent">
					<div class="news-thumb">
						<?php echo the_post_thumbnail('product-listing'); ?>
					</div>
				<?php echo ShortenText(strip_tags(get_the_content()),300); ?>
					
				</div>
				<div class="read_more"><a href="<?php echo get_permalink(); ?>">Read More</a></div>
			</div>
		<?php endwhile; ?>
	<?php else: ?>
		<p>No news as of the moment.</p>
	<?php endif; ?>
		<div class="page-nav-container clearfix">
			<div class="page-nav  margin_top_10 clearfix">
				<?php
					 $a_args = array( 
		                'base'          => str_replace( 999999, '%#%', esc_url( get_pagenum_link( 999999 ) ) ), // 99999 is used since the function get_pagenum_link() only accepts integer as param
		                'current'       => max( 1, $i_paged ),
		                'total'         => $loopb->max_num_pages,
		                'prev_text'     => '&laquo;',
		                'next_text'     => '&raquo;',
		                'type'          => 'plain',
		                'end_size'      => 3,
		                'mid_size'      => 3
		            );

					echo paginate_links( apply_filters( 'titan_pagination_args', $a_args ) );
		        ?>
			</div>
		</div>
    <?php wp_reset_query(); ?>
	</div>
	</div>
</div>
<?php get_footer(); ?>