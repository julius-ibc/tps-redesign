<?php get_header(); ?>
<div class="main-content section-block-wrapper padding_left_18 left">
	<div id="front-page" class="main-front-page section-pad-wrapper">
	
    <div class="banner-wrapper section-wrapper globalwidth margin_top_10">
      <div class="banner-pad section-pad-wrapper clearfix">

        <div id="banner" class="banner-wrap section-block-wrapper left">
          <div class="banner banner-slide">
             <?php 
				$args = array( 'post_type' => 'banner-slide', 'posts_per_page' => -1, 'orderby' => 'menu_order', 'order'=>'ASC');
				$loops = new WP_Query( $args );
				
				while ( $loops->have_posts() ) : $loops->the_post();
					
					echo "<div class='banner-image'>";
					$thumb_image_arr = wp_get_attachment_image_src(get_post_thumbnail_id(),'banner-slide');
					$thumb_image = $thumb_image_arr[0];
					if(function_exists('get_field')){$posturl =get_field('page_url');}
					echo '<div class="img_c">';					
					echo "<img class='" . basename(get_permalink()) . "' src='". $thumb_image ."' alt='".get_the_title()."'>";
					echo "<div class='banner-bg'></div>";
					echo "<div class='banner-text'>";
					echo "<div class='banner-title'>".get_the_title()."</div>";
					echo "<div class='banner-content'>".get_the_content()."</div>";
					echo "</div>";
					echo '</div>';
					
					echo "</div>";		
				endwhile;
				//wp_reset_query();
              ?>
          </div>
          <div class="banner-nav"></div>
          
         
          
         
        </div>

      </div>
    </div>
 	<div class="featured-wrapper margin_top_10 width_100 clearfix">
        <div class="title">Featured Products</div>
            <div class="featured-products horizontal-only jspScrollable margin_top_15">
            <?php
                $featured_products = get_option( 'sticky_products' );
                /* Sort the stickies with the newest ones at the top */
                /*$paged = (get_query_var('paged')) ? get_query_var('paged') : 1; */
             
                /* Query sticky posts */
                query_posts(array( 
                    /* 'caller_get_posts' => 1, */
                    'post_type' => 'wpsc-product',
                    'post__in' => $featured_products,
                    'order_by' => 'post_title',
                    'post_status'     => 'publish',
                    'order' =>'ASC'
                ));
            ?>
                <div class="featured-product-container  clearfix">
                    <?php while ( have_posts() ) : the_post(); ?>
                    <a class="left clearfix" href="<?php echo get_permalink()?>">
                        <div class="product_name"><?php echo get_the_title();?></div>
                        
                        <?php if(wpsc_product_on_special( $post->ID)):?>
                        <div class="wpsc_sale_item"></div>
                        <?php endif;?>
                        
                        <div class="product_image_featured ">
                        <?php 
                        if ( has_post_thumbnail() ) {
                            the_post_thumbnail('product-listing');
                        } else {
                            echo '<div class="front-feat-img"><img src="';
                            bloginfo("template_url");
                            echo '/wpsc-noimage.gif" alt="" height="156" width="156" /></div>';
                        }
                        ?>
                        </div>
                        
                        <?php
                        global $wp_query, $wpsc_query, $wpdb;
                        $special=get_post_meta( $post->ID, '_wpsc_special_price', true );
                        $special_price = 0;
                        $lowest_variation_price = 0;
                        $lowest_variation_special_price = 0;
                        $variation_count = 0;
                        /*$variation = new wpsc_variations($post->ID);
                        $variation->wpsc_variations($post->ID);
                        if( $variation->all_associated_variations ) {
                            foreach( $variation->all_associated_variations as $variationGroups ) {
                                foreach ($variationGroups as $variationChildren) {
                                
                                    if ( isset($variationChildren->term_taxonomy_id) ) {
                                        $variation_count = count($variationChildren->term_taxonomy_id); 
                                        $sql = 'SELECT
                                            wp_postmeta.meta_value
                                            FROM
                                            wp_term_relationships
                                            INNER JOIN wp_postmeta ON wp_term_relationships.object_id = wp_postmeta.post_id
                                            WHERE term_taxonomy_id = ' . $variationChildren->term_taxonomy_id . '
                                            AND meta_key LIKE "_wpsc_price"
                                            AND meta_value != 0
                                            order by meta_value
                                            limit 1';
                                            
                                        $variation_price = $wpdb->get_results( $sql, ARRAY_A );
                                    
                                        if ($variation_price[0]['meta_value'] <= $lowest_variation_price || $lowest_variation_price == 0) {
                                            $lowest_variation_price = $variation_price[0]['meta_value'];
                                        }
                                        
                                        $sql_special = 'SELECT
                                            wp_postmeta.meta_value
                                            FROM
                                            wp_term_relationships
                                            INNER JOIN wp_postmeta ON wp_term_relationships.object_id = wp_postmeta.post_id
                                            WHERE term_taxonomy_id = ' . $variationChildren->term_taxonomy_id . '
                                            AND meta_key LIKE "_wpsc_special_price"
                                            AND meta_value != 0
                                            order by meta_value
                                            limit 1';
                                            
                                        $variation_special_price = $wpdb->get_results( $sql_special, ARRAY_A );
                                    
                                        if ($variation_special_price[0]['meta_value'] <= $special || $special <= 0) {
                                            $special_price = $variation_special_price[0]['meta_value'];
                                        }
                                    }
                                }
                            }
                        }*/
                        ?>
                        <div class="current-price"><?php 
                            if($special>0 || $special_price>0):?>
                            <?php
                            if ($special_price > 0) {
                                echo wpsc_currency_display($special_price);
                            } else {
                                echo wpsc_currency_display(get_post_meta( $post->ID, '_wpsc_special_price', true ));
                            }
                            ?>
                            <?php else:?>
                            <?php if(get_post_meta( $post->ID, '_wpsc_price', true )>0 || $lowest_variation_price > 0):?>
                            <?php
                            
                            if ($lowest_variation_price > 0) {
                                print 'Starting from ' . wpsc_currency_display($lowest_variation_price);
                            } else {
                                echo wpsc_currency_display(get_post_meta( $post->ID, '_wpsc_price', true ));
                            }
                            
                            ?> 
                            <?php else:?><em>Call for pricing</em><?php endif;?><?php endif;?>
                        </div>
                        <div class="price">
                            <?php if((get_post_meta( $post->ID, '_wpsc_price', true )>0 && $special > 0) || ($special_price > 0 && $lowest_variation_price )):?>
                            <?php
                                if ($lowest_variation_price > 0) {
                                    print 'Starting from ' . wpsc_currency_display($lowest_variation_price);
                                } else {
                                    echo wpsc_currency_display(get_post_meta( $post->ID, '_wpsc_price', true ));
                                }
                            ?>
                            <?php endif;?>
                        </div>
                        <div class="view_details"> View Details</div>
                    </a>
                    <?php 
                        endwhile;
                        wp_reset_query();
                    ?>
                </div>
            </div>
        </div>
 	<div class="content left_c clearfix left margin_top_10">
 	<?php
	$my_id = get_option('page_on_front');;
	$post_home = get_post($my_id); 
	
	?> 
	
		<h1 class="page-title"><?php echo $post_home->post_title ?></h1>
		<div class="front-page-content clearfix margin_top_10">
			<?php echo apply_filters('the_content',$post_home->post_content); ?>
		</div>

        <div class="front-news-wrapper">            
            <?php
                $loopb = new WP_Query( array( 'post_type' => 'news-list', 'posts_per_page' => 2, ) );
            ?>
            <?php if($loopb->have_posts()): ?>
                <h2 class="news-title">LATEST NEWS</h2>
                <?php while ( $loopb->have_posts() ) : $loopb->the_post(); ?>
                    <div class="news-item">
                        <h3 class="news-title"><?php the_title(); ?></h3>
                        <div class="latestNewsContent"><?php echo ShortenText(strip_tags(get_the_content()),150); ?></div>
                        <div class="latestNewsLink"><a class="read_more" href="<?php echo get_permalink(); ?>">read more ›</a></div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>


	</div>
	<div class="right_c clearfix right margin_top_10 margin_left_10">
	
	<div class="side_item shipping_info_c width_100 clearfix"><div class="right side_content"><div class="side_item_title">Shipping Info</div><a class="clearfix read_more margin_top_5" href="/shipping-information">view more ›</a></div></div>
	<!--<div class="side_item help_centre_c width_100 clearfix"><div class="right side_content"><div class="side_item_title">Help Centre</div><a class="clearfix read_more margin_top_5" href="/help-centre">view more ›</a></div></div>-->
	</div>
	</div>
</div>
<?php get_footer(); ?>