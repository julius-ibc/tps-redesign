<?php get_header(); ?>
<div class="main-content section-block-wrapper padding_left_18 left">
	<div id="sub-page" class="main-sub-page section-pad-wrapper">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<h1 class="page-title"><?php the_title(); ?></h1>
		<div class="clearfix margin_top_15 width_100">
		
		<?php the_content(); ?>
		</div>
	<?php endwhile; else: ?>
		
	<?php endif; ?>
	
	<?php

		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$args = array('post_type' => 'design-service-list', 'orderby' => 'menu_order', 'order' => 'ASC','paged'=>$paged,'posts_per_page'=>1);
		$loopb = new WP_Query( $args );
	
	?>
	<div class="parent_container margin_top_20">
	<?php while ( $loopb->have_posts() ) : $loopb->the_post(); ?>
		<div class="child-item margin_bottom_50 clearfix">
			
			<div class="content_container right clearfix">
			<div class="child-title"><?php the_title(); ?></div>
		
			<div class="content margin_top_10"><?php echo ShortenText(strip_tags(get_the_content()),450); ?></div>
			<div class="content_link margin_top_10"><a class="read_more right" href="<?php echo get_permalink(); ?>">Read More ›</a></div>
			</div>
		</div>
	
	<?php endwhile; ?>
	<?php if($loopb->max_num_pages>1){ ?>
	<div class="page-nav-container clearfix">
		<div class="page-nav  margin_top_10 clearfix"><?php if($paged > 1):?><a href="<?php echo '/design-services/'; ?>" title="First Page">« First</a><?php endif;?><?php if($paged > 1):?><a href="<?php echo '/design-services/?paged=' . ($paged-1); ?>" title="Previous Page">&lt; Previous</a><?php endif;?><?php for($i=1; $i<=$loopb->max_num_pages; $i++): ?><?php if($paged == $i):?><span class="current"><?php echo $i;?></span><?php else:?><a href="<?php echo '/design-services/?paged=' . $i; ?>" title="Page <?php echo $i?>"><?php echo $i;?></a><?php endif;?><?php endfor; ?><?php if($paged < $loopb->max_num_pages):?><a href="<?php echo '/design-services/?paged=' .($paged+1); ?>" title="Next Page">Next &gt;</a><?php endif;?><?php if($paged < $loopb->max_num_pages):?><a href="<?php echo '/design-services/?paged=' .$loopb->max_num_pages; ?>" title="Last Page">Last »</a><?php endif;?>
		</div>
		</div>
	<?php } ?>
    <?php wp_reset_query(); ?>
	</div>
	</div>
</div>
<?php get_footer(); ?>