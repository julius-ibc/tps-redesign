<?php
/**
 * The template for displaying all pages.
 *
 * @package flatsome
 */


if(flatsome_option('pages_template') != 'default') {
	
	// Get default template from theme options.
	get_template_part('page', flatsome_option('pages_template'));
	return;

} else {

get_header();
do_action( 'flatsome_before_page' ); ?>
<div id="content" class="content-area page-wrapper" role="main">
	<div class="row row-main">
		<div class="large-12 col">
			<div class="col-inner">
				
				<?php if(get_theme_mod('default_title', 0)){ ?>
				<header class="entry-header">
					<h1 class="entry-title mb uppercase"><?php the_title(); ?></h1>
				</header><!-- .entry-header -->
				<?php } ?>

				<?php while ( have_posts() ) : the_post(); ?>
					<?php do_action( 'flatsome_before_page_content' ); ?>
					
                        <?php the_content(); ?>
                        
                        <?php

                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $args = array('post_type' => 'design-service-list', 'orderby' => 'menu_order', 'order' => 'ASC','paged'=>$paged,'posts_per_page'=>1);
                        $loopb = new WP_Query( $args );
                    
                    ?>
                    <div class="parent_container margin_top_20">
                    <?php while ( $loopb->have_posts() ) : $loopb->the_post(); ?>
                        <div class="child-item margin_bottom_50 clearfix">
                            
                            <div class="content_container right clearfix">
                            <div class="child-title"><?php the_title(); ?></div>
                        
                            <div class="content margin_top_10"><?php echo ShortenText(strip_tags(get_the_content()),450); ?></div>
                            <div class="content_link margin_top_10"><a class="read_more right" href="<?php echo get_permalink(); ?>">Read More ›</a></div>
                            </div>
                        </div>
                    
                    <?php endwhile; ?>
                    <?php if($loopb->max_num_pages>1){ ?>
                    <div class="page-nav-container clearfix">
                        <div class="page-nav  margin_top_10 clearfix"><?php if($paged > 1):?><a href="<?php echo '/design-services/'; ?>" title="First Page">« First</a><?php endif;?><?php if($paged > 1):?><a href="<?php echo '/design-services/?paged=' . ($paged-1); ?>" title="Previous Page">&lt; Previous</a><?php endif;?><?php for($i=1; $i<=$loopb->max_num_pages; $i++): ?><?php if($paged == $i):?><span class="current"><?php echo $i;?></span><?php else:?><a href="<?php echo '/design-services/?paged=' . $i; ?>" title="Page <?php echo $i?>"><?php echo $i;?></a><?php endif;?><?php endfor; ?><?php if($paged < $loopb->max_num_pages):?><a href="<?php echo '/design-services/?paged=' .($paged+1); ?>" title="Next Page">Next &gt;</a><?php endif;?><?php if($paged < $loopb->max_num_pages):?><a href="<?php echo '/design-services/?paged=' .$loopb->max_num_pages; ?>" title="Last Page">Last »</a><?php endif;?>
                        </div>
                        </div>
                    <?php } ?>
                    <?php wp_reset_query(); ?>
                    </div>

						<?php if ( comments_open() || '0' != get_comments_number() ){
							comments_template(); } ?>

					<?php do_action( 'flatsome_after_page_content' ); ?>
				<?php endwhile; // end of the loop. ?>
			</div><!-- .col-inner -->
		</div><!-- .large-12 -->
	</div><!-- .row -->
</div>

<?php
do_action( 'flatsome_after_page' );
get_footer();

}