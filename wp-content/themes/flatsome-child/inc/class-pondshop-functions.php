<?php

class PondshopFunctions
{
    public function __construct()
    {
        $this->hideAdditionalInformation();
        $this->productPriceEmpty();
        $this->productRewrite();
        $this->addCanonical();
    }

    protected function hideAdditionalInformation()
    {
        add_filter('woocommerce_product_tabs', function ($tabs) {
            unset($tabs['additional_information']);

            return $tabs;
        }, 9999);
    }

    protected function productPriceEmpty()
    {
        add_filter('woocommerce_get_price_html', function ($price, $_product) {
            if ($_product->is_type('simple') && empty($_product->get_price())) {
                return  'Call for pricing';
            }

            return $price;
        }, 10, 2);

        add_action('woocommerce_single_product_summary', function () {
            global $product;

            if (empty($product->get_price())) {
                //Hide add to cart button and price
                remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
                if (class_exists('YITH_WCWL_Init')) {
                    remove_action('woocommerce_single_product_summary', [YITH_WCWL_Init::get_instance(), 'print_button'], 31);
                    remove_action('woocommerce_product_thumbnails', [YITH_WCWL_Init::get_instance(), 'print_button'], 21);
                    remove_action('woocommerce_after_single_product_summary', [YITH_WCWL_Init::get_instance(), 'print_button'], 11);
                }
            }
            //Store pick up only
            if ($product->get_shipping_class() == 'store-pick-up-only' || get_post_meta($product->get_id(), 'store_pick-up_only', true) == 'Yes') {
                remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
                add_action('woocommerce_single_product_summary', function () {
                    echo '<p>In store pick-up only, please enquire below whether shipping is available to your area</p>';
                }, 31);
            }
        }, 10);

        add_filter('woocommerce_add_to_cart_validation', function ($passed, $added_product_id) {
            $product = wc_get_product($added_product_id);

            if (empty($product->get_price()) || ($product->get_shipping_class() == 'store-pick-up-only' || get_post_meta($product->get_id(), 'store_pick-up_only', true) == 'Yes')) {
                return false;
            }

            return $passed;
        }, 99, 2);
    }

    protected function productRewrite()
    {
        add_filter('rewrite_rules_array', function ($rules) {
            global  $wp_rewrite;
            $feed = '('.trim(implode('|', $wp_rewrite->feeds)).')';

            $terms = get_categories(array(
                'taxonomy' => 'product_cat',
                'hide_empty' => false,
            ));

            $customRules = [];

            foreach ($terms as $term) {
                $slug = $this->buildTermPath($term);
                $customRules["products/{$slug}/?\$"] = 'index.php?product_cat='.$term->slug;
                $customRules["products/{$slug}/embed/?\$"] = 'index.php?product_cat='.$term->slug.'&embed=true';
                $customRules["products/{$slug}/{$wp_rewrite->feed_base}/{$feed}/?\$"] = 'index.php?product_cat='.$term->slug.'&feed=$matches[1]';
                $customRules["products/{$slug}/{$feed}/?\$"] = 'index.php?product_cat='.$term->slug.'&feed=$matches[1]';
                $customRules["products/{$slug}/{$wp_rewrite->pagination_base}/?([0-9]{1,})/?\$"] = 'index.php?product_cat='.$term->slug.'&paged=$matches[1]';
            }

            return $customRules + $rules;
        });
    }

    protected function addCanonical()
    {
        add_filter('wpseo_canonical', '__return_false'); //Disable yoast canonical
        add_action('wp_head', function () {
            $canonical = $this->getCanonical();
            if (!empty($canonical)) {
                echo  '<link rel="canonical" href="'.esc_url($canonical).'" />'."\n";
            }
        });
    }

    private function buildTermPath($term)
    {
        $slug = urldecode($term->slug);

        if ($term->parent) {
            $ancestors = get_ancestors($term->term_id, 'product_cat');
            foreach ($ancestors as $ancestor) {
                $ancestor_object = get_term($ancestor, 'product_cat');
                $slug = urldecode($ancestor_object->slug).'/'.$slug;
            }
        }

        return $slug;
    }

    private function getCanonical($useCommentsPagination = false)
    {
        global  $wp_rewrite;
        $qo = get_queried_object();
        $canonical = null;

        if ($qo instanceof WP_Term) {
            $canonical = get_term_link($qo);
            $paged = get_query_var('paged');
            if ($paged > 1) {
                $canonical = trailingslashit($canonical).trailingslashit($wp_rewrite->pagination_base).$paged;
            }
        } elseif ($qo instanceof WP_Post) {
            $canonical = get_permalink($qo);

            if ($useCommentsPagination) {
                $page = get_query_var('cpage');
                if ($page > 1) {
                    $canonical = trailingslashit($canonical).$wp_rewrite->comments_pagination_base.'-'.$page;
                }
            }
        }

        if ($canonical) {
            return user_trailingslashit($canonical);
        }
    }
}
