<?php
require get_stylesheet_directory() . '/inc/class-pondshop-functions.php';
new PondshopFunctions();

// add_filter( 'woocommerce_shipping_calculator_enable_country', '__return_false' );
add_filter( 'woocommerce_shipping_calculator_enable_state', '__return_false' );
add_filter( 'woocommerce_shipping_calculator_enable_city', '__return_false' );

/**
 * Enqueue scripts and styles.
 */
function wpdocs_theme_name_scripts()
{
    wp_enqueue_script('custom-script', get_stylesheet_directory_uri() . '/js/script.js', array('jquery'), '1.0.0', true);
}
add_action('wp_enqueue_scripts', 'wpdocs_theme_name_scripts');

add_action('flatsome_after_product_page', 'product_enquiry_form');

function product_enquiry_form()
{
    ?>
    <div class="container">
        <h3>Product Enquiry Form</h3>
        <?=do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]');?>
    </div>

<?php
}

function pond_sub_categories($product_categories)
{
    ob_start();

    $classes_box = array('box', 'box-category', 'has-hover');
    $classes_image = array();
    $classes_text = array();

    extract(array(
        // Meta
        'number' => null,
        '_id' => 'cats-' . rand(),
        'ids' => false, // Custom IDs
        'title' => '',
        'cat' => '',
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'hide_empty' => 1,
        'parent' => 'false',
        'offset' => '',
        'show_count' => 'true',
        'class' => '',
        'visibility' => '',
        'tag' => '',

        // Layout
        'style' => 'normal',
        'columns' => '4',
        'columns__sm' => '',
        'columns__md' => '',
        'col_spacing' => 'small',
        'type' => 'row', // slider, row, masonery, grid
        'width' => '',
        'grid' => '1',
        'grid_height' => '600px',
        'grid_height__md' => '500px',
        'grid_height__sm' => '400px',
        'slider_nav_style' => 'reveal',
        'slider_nav_color' => '',
        'slider_nav_position' => '',
        'slider_bullets' => 'false',
        'slider_arrows' => 'true',
        'auto_slide' => 'false',
        'infinitive' => 'true',
        'depth' => '',
        'depth_hover' => '',

        // Box styles
        'animate' => '',
        'text_pos' => '',
        'text_padding' => '',
        'text_bg' => '',
        'text_color' => '',
        'text_hover' => '',
        'text_align' => 'center',
        'text_size' => '',

        'image_size' => '',
        'image_mask' => '',
        'image_width' => '',
        'image_hover' => '',
        'image_hover_alt' => '',
        'image_radius' => '',
        'image_height' => '',
        'image_overlay' => '',

        // depricated
        'bg_overlay' => '#000',
    ));

    // Create Grid
    if ($type == 'grid') {
        $columns = 0;
        $current_grid = 0;
        $grid = flatsome_get_grid($grid);
        $grid_total = count($grid);
        flatsome_get_grid_height($grid_height, $_id);
    }

    // Add Animations
    if ($animate) {
        $animate = 'data-animate="' . $animate . '"';
    }

    // Set box style
    if ($style) {
        $classes_box[] = 'box-' . $style;
    }
    if ($style == 'overlay') {
        $classes_box[] = 'dark';
    }
    if ($style == 'shade') {
        $classes_box[] = 'dark';
    }
    if ($style == 'badge') {
        $classes_box[] = 'hover-dark';
    }
    if ($text_pos) {
        $classes_box[] = 'box-text-' . $text_pos;
    }
    if ($style == 'overlay' && !$image_overlay) {
        $image_overlay = true;
    }

    // Set image styles
    if ($image_hover) {
        $classes_image[] = 'image-' . $image_hover;
    }
    if ($image_hover_alt) {
        $classes_image[] = 'image-' . $image_hover_alt;
    }
    if ($image_height) {
        $classes_image[] = 'image-cover';
    }

    // Text classes
    if ($text_hover) {
        $classes_text[] = 'show-on-hover hover-' . $text_hover;
    }
    if ($text_align) {
        $classes_text[] = 'text-' . $text_align;
    }
    if ($text_size) {
        $classes_text[] = 'is-' . $text_size;
    }
    if ($text_color == 'dark') {
        $classes_text[] = 'dark';
    }

    $css_args_img = array(
        array('attribute' => 'border-radius', 'value' => $image_radius, 'unit' => '%'),
        array('attribute' => 'width', 'value' => $image_width, 'unit' => '%'),
    );

    $css_image_height = array(
        array('attribute' => 'padding-top', 'value' => $image_height),
    );

    $css_args = array(
        array('attribute' => 'background-color', 'value' => $text_bg),
        array('attribute' => 'padding', 'value' => $text_padding),
    );

    // Repeater options
    $repater['id'] = $_id;
    $repater['class'] = $class;
    $repater['visibility'] = $visibility;
    $repater['tag'] = $tag;
    $repater['type'] = $type;
    $repater['style'] = $style;
    $repater['format'] = $image_height;
    $repater['slider_style'] = $slider_nav_style;
    $repater['slider_nav_color'] = $slider_nav_color;
    $repater['slider_nav_position'] = $slider_nav_position;
    $repater['slider_bullets'] = $slider_bullets;
    $repater['auto_slide'] = $auto_slide;
    $repater['row_spacing'] = $col_spacing;
    $repater['row_width'] = $width;
    $repater['columns'] = $columns;
    $repater['columns__sm'] = $columns__sm;
    $repater['columns__md'] = $columns__md;
    $repater['depth'] = $depth;
    $repater['depth_hover'] = $depth_hover;

    get_flatsome_repeater_start($repater);

    if ($product_categories) {
        foreach ($product_categories as $category) {
            $classes_col = array('product-category', 'col');

            $thumbnail_size = apply_filters('single_product_archive_thumbnail_size', 'woocommerce_thumbnail');

            if ($image_size) {
                $thumbnail_size = $image_size;
            }

            if ($type == 'grid') {
                if ($grid_total > $current_grid) {
                    ++$current_grid;
                }
                $current = $current_grid - 1;
                $classes_col[] = 'grid-col';
                if ($grid[$current]['height']) {
                    $classes_col[] = 'grid-col-' . $grid[$current]['height'];
                }
                if ($grid[$current]['span']) {
                    $classes_col[] = 'large-' . $grid[$current]['span'];
                }
                if ($grid[$current]['md']) {
                    $classes_col[] = 'medium-' . $grid[$current]['md'];
                }

                // Set image size
                if ($grid[$current]['size'] == 'large') {
                    $thumbnail_size = 'large';
                }
                if ($grid[$current]['size'] == 'medium') {
                    $thumbnail_size = 'medium';
                }
            }

            $thumbnail_id = get_term_meta($category->term_id, 'thumbnail_id', true);

            if ($thumbnail_id) {
                $image = wp_get_attachment_image_src($thumbnail_id, $thumbnail_size);
                $image = $image[0];
            } else {
                $image = wc_placeholder_img_src();
            }?>
        <div class="<?php echo implode(' ', $classes_col); ?>" <?php echo $animate; ?>>
            <div class="col-inner">
              <?php do_action('woocommerce_before_subcategory', $category);?>
                <div class="<?php echo implode(' ', $classes_box); ?> ">
                <div class="box-image" <?php echo get_shortcode_inline_css($css_args_img); ?>>
                  <div class="<?php echo implode(' ', $classes_image); ?>" <?php echo get_shortcode_inline_css($css_image_height); ?>>
                  <?php echo '<img src="' . esc_url($image) . '" alt="' . esc_attr($category->name) . '" width="300" height="300" />'; ?>
                  <?php if ($image_overlay) {
                ?><div class="overlay" style="background-color: <?php echo $image_overlay; ?>"></div><?php
}?>
                  <?php if ($style == 'shade') {
                ?><div class="shade"></div><?php
}?>
                  </div>
                </div><!-- box-image -->
                <div class="box-text <?php echo implode(' ', $classes_text); ?>" <?php echo get_shortcode_inline_css($css_args); ?>>
                  <div class="box-text-inner">
                      <h5 class="uppercase header-title">
                              <?php echo $category->name; ?>
                      </h5>
                      <?php if ($show_count) {
                ?>
                      <p class="is-xsmall uppercase count <?php if ($style == 'overlay') {
                    echo 'show-on-hover hover-reveal reveal-small';
                }?>">
                          <?php if ($category->count > 0) {
                    echo apply_filters('woocommerce_subcategory_count_html', $category->count . ' ' . ($category->count > 1 ? __('Products', 'woocommerce') : __('Product', 'woocommerce')), $category);
                }?>
                      </p>
                      <?php
}?>
                      <?php
/**
             * woocommerce_after_subcategory_title hook.
             */
            do_action('woocommerce_after_subcategory_title', $category);?>

                  </div><!-- .box-text-inner -->
                </div><!-- .box-text -->
                </div><!-- .box -->
            <?php do_action('woocommerce_after_subcategory', $category);?>
            </div><!-- .col-inner -->
            </div><!-- .col -->
        <?php
}
    }
    woocommerce_reset_loop();

    get_flatsome_repeater_end($repater);

    $content = ob_get_contents();
    ob_end_clean();

    return $content;
}

function pond_get_flatsome_icon($name, $size = null, $title = null)
{
    if ($size) {
        $size = 'style="font-size:' . $size . ';"';
    }

    if ($title) {
        return '<i title="' . $title . '" class="tooltip ' . $name . '" ' . $size . '></i>';
    }

    return '<i class="' . $name . '" ' . $size . '></i>';
}

//REMOVE Related products by tag and category
/*
 * Does not filter related products by tag
 */
//add_filter('woocommerce_product_related_posts_relate_by_tag', '__return_false');

/*
 * Does not filter related products by category
 */
//add_filter('woocommerce_product_related_posts_relate_by_category', '__return_false');

add_action('init', 'register_design_services', 1);
function register_design_services()
{
    // A unique name for our function
    $labels = array(
        'name' => _x('Design Services', 'post type general name'),
        'singular_name' => _x('Design Service List', 'post type singular name'),
        'add_new' => _x('Add New', 'Design Service'),
        'add_new_item' => __('Add New Design Service'),
        'edit_item' => __('Edit Design Service'),
        'new_item' => __('New Design Service'),
        'view_item' => __('View Design Service'),
        'search_items' => __('Search Design Service'),
        'not_found' => __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => 'true', // Activate the archive
        'supports' => array('title', 'editor', 'thumbnail', 'page-attributes', 'custom-fields'),
        'taxonomies' => array('category'), // this is IMPORTANT
    );
    register_post_type('design-service-list', $args);


    // DOWNLOADS POST TYPE
    $labels = array(
        'name' => _x('Downloads', 'post type general name'),
        'singular_name' => _x('Download', 'post type singular name'),
        'add_new' => _x('Add New', 'Download'),
        'add_new_item' => __('Add New Download'),
        'edit_item' => __('Edit Download'),
        'new_item' => __('New Download'),
        'view_item' => __('View Download'),
        'search_items' => __('Search Downloads'),
        'not_found' => __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => 'false',
        'supports' => array('title', 'thumbnail')
    );
    register_post_type('downloads', $args);
}
add_action('init', 'register_help_centres', 1);
function register_help_centres()
{
    // A unique name for our function
    // $labels = array(
    //     'name' => _x('Help Centres', 'post type general name'),
    //     'singular_name' => _x('Help Centre List', 'post type singular name'),
    //     'add_new' => _x('Add New', 'Help Centre'),
    //     'add_new_item' => __('Add New Help Centre'),
    //     'edit_item' => __('Edit Help Centre'),
    //     'new_item' => __('New Help Centre'),
    //     'view_item' => __('View Help Centre'),
    //     'search_items' => __('Search Help Centre'),
    //     'not_found' => __('Nothing found'),
    //     'not_found_in_trash' => __('Nothing found in Trash'),
    // );
    // $args = array(
    //     'labels' => $labels,
    //     'public' => true,
    //     'has_archive' => 'true', // Activate the archive
    //     'supports' => array('title', 'editor', 'thumbnail', 'page-attributes', 'custom-fields'),
    //     'taxonomies' => array('category'), // this is IMPORTANT
    // );
    // register_post_type('help-centre-list', $args);
}

/* Titan Truncate */
function ShortenText($text, $chars_limit = 10, $after = '...')
{
    $chars_text = strlen($text);
    $text = $text . ' ';
    $text = substr($text, 0, $chars_limit);
    $text = substr($text, 0, strrpos($text, ' '));
    /* $text = strip_tags($text); */
    if ($chars_text > $chars_limit) {
        $text = $text . $after;
    }

    return $text;
}

add_action('woocommerce_checkout_after_order_review', 'pondshop_woocommerce_checkout_after_order_review');

function pondshop_woocommerce_checkout_after_order_review()
{
    ?>
        <div id="eWAYBlock">
            <div style="text-align:center;">
                <a href="https://www.eway.com.au/secure-site-seal?i=12&se=3&theme=0" title="eWAY Payment Gateway" target="_blank" rel="nofollow">
                    <img alt="eWAY Payment Gateway" src="https://www.eway.com.au/developer/payment-code/verified-seal.php?img=12&size=3&theme=0" />
                </a>
            </div>
        </div>
        <?php
}

remove_filter( 'the_content', 'wptexturize' );
remove_filter( 'the_excerpt', 'wptexturize' );

function pageBoxes_shortcode( $atts ) {
    $a = shortcode_atts( array(
        'parent' => '-1'
    ), $atts );

    ob_start();

    $args = array(
        'post_type'      => 'page',
        'posts_per_page' => -1,
        'post_parent'    => $a['parent'],
        'order'          => 'ASC',
        'orderby'        => 'menu_order'
    );
    $parent = new WP_Query( $args );
    ?> 
    <div class="row">
        <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
        <div class="col medium-6 small-12 large-3">
            <div class="col-inner">
                <div class="box has-hover   has-hover box-badge hover-dark box-text-bottom">
                    <div class="box-image">
                        <a href="<?php the_permalink(); ?>">            
                            <div class="">
                                <?php $featured_img_url = !empty(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full')) ? wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full')[0] : '/wp-content/themes/flatsome/assets/img/missing.jpg';  ?>
                                <img data-src="<?php echo $featured_img_url; ?>" src="<?php echo $featured_img_url; ?>">
                            </div>
                        </a>        
                    </div>
                    <div class="box-text text-center">
                        <div class="box-text-inner">
                            <h4><?php the_title(); ?></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
    </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'pageboxes', 'pageBoxes_shortcode' );

function downloadBoxes_shortcode( $atts ) {
    ob_start();
    $args = array(
        'post_type'      => 'downloads',
        'posts_per_page' => -1,
        'order'          => 'ASC',
        'orderby'        => 'menu_order'
    );
    $download = new WP_Query( $args );
    ?> 
    <div class="row download-boxes">
        <?php while ( $download->have_posts() ) : $download->the_post(); ?>
        <?php
            $fileType = get_field('type'); 
            $file = ($fileType == 'File') ? get_field('file') : get_field('url');
        ?>
        <div class="col medium-6 small-12 large-3">
            <div class="col-inner">
                <div class="box box-badge hover-dark box-text-bottom">
                    <div class="box-image">
                        <a href="<?= $file; ?>" target="_blank">            
                            <div class="">
                                <?php $featured_img_url = !empty(wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full')) ? wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full')[0] : '/wp-content/themes/flatsome/assets/img/missing.jpg';  ?>
                                <img data-src="<?php echo $featured_img_url; ?>" src="<?php echo $featured_img_url; ?>">
                            </div>
                        </a>        
                    </div>
                </div>
                <div class="box-text text-center">
                    <div class="box-text-inner">
                        <h4><?php the_title(); ?></h4>
                    </div>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
    </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'downloadBoxes', 'downloadBoxes_shortcode' );

function wcerbe_woocommerce_product_bulk_edit_end() {
    $output = '<label><span class="title">' . esc_html__( "Enable reviews", "woocommerce" ) . '?</span>';
    $output .= '<span class="input-text-wrap"><select class="reviews_allowed" name="_reviews_allowed">';
    $options = array(
        ''    => __( '— No change —', 'woocommerce' ),
        'yes' => __( 'Yes', 'woocommerce' ),
        'no'  => __( 'No', 'woocommerce' ),
    );
    foreach ( $options as $key => $value ) {
        $output .= '<option value="' . esc_attr( $key ) . '">' . esc_html( $value ) . '</option>';
    }
    $output .= '</select></span></label>';
    echo $output;
}
add_action( 'woocommerce_product_bulk_edit_end', 'wcerbe_woocommerce_product_bulk_edit_end' );
function wcerbe_woocommerce_product_bulk_edit_save( $product ) {
    // Enable reviews
    if ( ! empty( $_REQUEST['_reviews_allowed'] ) ) {
        if ( 'yes' === $_REQUEST['_reviews_allowed'] ) {
            $product->set_reviews_allowed( 'yes' );
        } else {
            $product->set_reviews_allowed( '' );
        }
    }
    $product->save();
}
add_action( 'woocommerce_product_bulk_edit_save', 'wcerbe_woocommerce_product_bulk_edit_save', 10, 1 );

// Filter Searches to Products only
function lw_search_filter_pages($query) {
    //if (!is_admin() && $query->is_search) {
    //    $query->set('post_type', 'product');
    //    $query->set( 'wc_query', 'product_query' );
    //}
    return $query;
}
add_filter('pre_get_posts','lw_search_filter_pages');


function change_default_checkout_country( $country ) {
    if ( WC()->customer->get_is_paying_customer() ) {
        return $country;
    }

    return 'AU';
}
add_filter( 'default_checkout_shipping_country', 'change_default_checkout_country', 10, 1 );
add_filter( 'default_checkout_billing_country', 'change_default_checkout_country' );