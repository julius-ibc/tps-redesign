var $ = jQuery;

function contentSlider() {
    $('.id-slider a, a.id-slider').on('click', function(e) {
        e.preventDefault();

        var _this = $(this);
        console.log(_this.attr('href'));
        $('html, body').animate({
            scrollTop: $( _this.attr('href') ).offset().top - 150
        }, 2000);
    });
}

function addShippingNotes() {
    if ( $('label[for="shipping_method_0_aus_parcel_regular"]').length ) {
        $('label[for="shipping_method_0_aus_parcel_regular"]').append(
                '<p>Please allow an extra day for handling</p>'
            );
    }
}

function categoryBoxResize() {
	if ( $('.box-category > .box-image').length ) {
		var minHeight = 200;
        $('.box-category > .box-image').each(function(e, i) {
        	minHeight = ( $(this).outerHeight() > minHeight ) ? $(this).outerHeight() : minHeight;
        }).css('min-height', minHeight+'px');
    }
	if ( $('.box-category > .box-text').length ) {
		var minHeight = 50;
        $('.box-category > .box-text').each(function(e, i) {
        	minHeight = ( $(this).outerHeight() > minHeight ) ? $(this).outerHeight() : minHeight;
        }).css('min-height', minHeight+'px');
    }
}

$(document).ready(function() {
    contentSlider();
    categoryBoxResize();


    $('body').on( 'updated_checkout', function(){
        addShippingNotes();
    });

    if ( $('.cart-container').length ) {
        addShippingNotes();

        $('body').on( 'updated_cart_totals', function(){
            addShippingNotes();
        });
    }
});
$(window).load(function() {
    categoryBoxResize();
});