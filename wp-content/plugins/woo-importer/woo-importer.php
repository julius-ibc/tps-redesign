<?php

/**
 * Plugin Name: Woo Importer
 */

class WooImporter
{
    function __construct()
    {
        add_action( 'wp_ajax_wooimporter', [$this, 'ajax']);
    }

    public function ajax()
    {
        switch ($_REQUEST['task'])
        {
            case 'try_update_product_id':


                $exist = get_post($_REQUEST['to_id']);

                if ($exist) {
                    throw new Error('Exist');
                }

                try {

                    global $wpdb;

                    $wpdb->query($wpdb->prepare("UPDATE {$wpdb->posts} set ID = %d WHERE ID = %d", $_REQUEST['to_id'], $_REQUEST['from_id']));
                    $wpdb->query($wpdb->prepare("UPDATE {$wpdb->posts} set post_parent = %d WHERE post_parent = %d", $_REQUEST['to_id'], $_REQUEST['from_id']));
                    $wpdb->query($wpdb->prepare("UPDATE {$wpdb->postmeta} set post_id = %d WHERE post_id = %d", $_REQUEST['to_id'], $_REQUEST['from_id']));

                    wp_send_json_success();

                } catch (Exception $e) {
                    wp_send_json_error($e->getMessage(), 422);
                }

                
            break;
        }

        exit();
    }
}

new WooImporter;