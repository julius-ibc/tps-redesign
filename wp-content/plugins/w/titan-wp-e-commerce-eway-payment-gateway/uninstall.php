<?php
include_once( 'includes/functions.php' );

switch( wpsc_get_major_version() ) {

	case '3.7':
		chdir( WP_PLUGIN_DIR . '/wp-e-commerce/merchants/' );
		break;

	case '3.8':
		chdir( WP_PLUGIN_DIR . '/wp-e-commerce/wpsc-merchants/' );
		break;

}
if( file_exists( 'eway.php' ) )
	@unlink( 'eway.php' );
?>