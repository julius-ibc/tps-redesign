<?php

/*
Plugin Name: Titan WP E-Commerce eWay Gateway
Plugin URI: http://www.titaninteractive.com.au/plugins/eway
Description: Allows WP e-Commerce store owners to utilise the eWay gateway facility
Version: 1.0
Author: Titan Interactive
Author URI: http://www.titaninteractive.com.au
*/

load_plugin_textdomain( 'titan_eway',null,basename( dirname( __FILE__ ) ) );

include_once( 'includes/functions.php' );

if( is_admin() )
{

	function titan_eway_admin_init()
	{
		global $titan_eway_pluginname, $titan_eway_menuname, $titan_eway_plugindir, $titan_eway_localversion;

		$titan_eway_localversion = '1.0.0';
		$titan_eway_pluginname = __( 'Titan WP E-Commerce eWay Gateway','titan_eway' );
		//$titan_eway_menuname = __( 'Titan WP E-Commerce eWay Gateway','titan_eway' );
		$titan_eway_plugindir = basename( dirname( __FILE__ ) );
	}
	add_action( 'admin_init','titan_eway_admin_init' );

	function titan_eway_check_plugin_version( $plugin )
	{
		global $titan_eway_pluginname, $titan_eway_plugindir, $titan_eway_localversion;

		if( strpos( $titan_eway_plugindir . '/eway.php',$plugin ) !== false )
		{
			//$check = wp_remote_fopen( 'http://local.webbshed.com/wp-plugin-versions.php?tw-plugin=EWAY-PAY' );
			$check = false;
			if( $check )
			{
				$status = explode( '@',$check );
				if( ( version_compare( strval( $status[0] ),strval( $titan_eway_localversion ),'>' ) == 1 ) )
				{
					echo '<td colspan="5" class="plugin-update" style="line-height:1.2em; font-size:11px; padding:1px;"><div style="color:#000; font-weight:bold; margin:4px; padding:6px 5px; background-color:#fffbe4; border-color:#dfdfdf; border-width:1px; border-style:solid; -moz-border-radius:5px; -khtml-border-radius:5px; -webkit-border-radius:5px; border-radius:5px;">' . __( "There is a new version of " . $titan_eway_pluginname . " available.","titan_eway" ) . ' <a href="' . $status[1] . '">' . __( "View version ","titan_eway" ) . $status[0] . __( " details","titan_eway" ) . '</a>.</div></td>';
				} else {
					return;
				}
			}
		}

	}
	//This has been commented out as we currently dont have a web page for version control
	//add_action( 'after_plugin_row','titan_eway_check_plugin_version' );

	function titan_eway_admin_notice()
	{

		if( titan_eway_check_merchant_exists() )
		{
			titan_eway_move_gateway();
			$message = "<p><strong>The merchant file 'eway.php' is missing from the WP e-Commerce Merchants directory.</strong> Please re-activate Titan WP E-Commerce eWay Gateway from the Plugins page.";
			$output = "<div class='updated fade'>" . $message . "</p></div>";
			echo $output;
		}

	}
	add_action( 'admin_notices','titan_eway_admin_notice' );

	function titan_eway_install()
	{
		titan_eway_move_gateway();
	}
	
	register_activation_hook( __FILE__,'titan_eway_install' );

	function titan_eway_move_gateway()
	{

		switch( wpsc_get_major_version() )
		{
			case '3.7':
				copy( dirname( __FILE__ ) . '/merchants/eway.php',WP_PLUGIN_DIR . '/wp-e-commerce/merchants/eway.php' );
				break;

			case '3.8':
				copy( dirname( __FILE__ ) . '/merchants/eway.php',WP_PLUGIN_DIR . '/wp-e-commerce/wpsc-merchants/eway.php' );
				break;

		}

	}

} //End is admin

?>