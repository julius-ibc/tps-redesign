<?php
//Test Credit Card: 4444333322221111
//5123456789012346

$nzshpcrt_gateways[$num]['name'] = 'eWay';
$nzshpcrt_gateways[$num]['internalname'] = 'eway';
$nzshpcrt_gateways[$num]['function'] = 'gateway_eway';
$nzshpcrt_gateways[$num]['form'] = "form_eway";
$nzshpcrt_gateways[$num]['submit_function'] = "submit_eway";

function form_eway()
{
	$select_currency[get_option('eway_currency_code')] = "selected='selected'";
	$eway_url = ( get_option('eway_posturl') == '' ? 'https://au.ewaygateway.com/Request/' : get_option('eway_posturl') );
	update_option('eway_posturl', $eway_url);
	
	$eway_resultsurl = ( get_option('eway_resultsurl') == '' ? 'https://au.ewaygateway.com/Result/' : get_option('eway_resultsurl') );
	update_option('eway_resultsurl', $eway_resultsurl);
	
	

	if (!isset($select_currency['AUS'])) $select_currency['AUS'] = ''; 
	if (!isset($select_currency['USD'])) $select_currency['USD'] = ''; 

	$eway_debug = get_option('eway_debug');
	$eway_debug1 = "";
	$eway_debug2 = "";
	switch($eway_debug)
	{
		case 0:
			$eway_debug2 = "checked ='checked'";
			break;
		case 1:
			$eway_debug1 = "checked ='checked'";
			break;
	}
	
	$output = "	<tr>\n\r
					<td width='100'><label for='eway_customerid'>Customer ID</label></td>\n\r
					<td><input name='eway_customerid' type='text' value='".get_option('eway_customerid')."' /></td>\n\r
				</tr>\n\r";
	
	$output .="	<tr>\n\r
					<td><label for='eway_username'>User Name</label></td>\n\r
					<td><input name='eway_username' type='text' value='".get_option('eway_username')."' /></td>\n\r
				</tr>\n\r";
				
	$output .="	<tr>
					<td colspan='2'>&nbsp;</td>
				</tr>\n\r";
				
	$output .="	<tr>
					<td>Accepted Currency</td>
					<td>
						<select name='eway_currency_code'>
							<option ".$select_currency['AUD']." value='AUD'>AUD - Australian Dollar</option>
							<option ".$select_currency['USD']." value='USD'>USD - U.S. Dollar</option>
						</select> 
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><small>The currency code that eWay will process the payment in. All products must be set up in this currency.</small></td>
				</tr>";	

	$output .="	<tr>\n\r
					<td><label for='eway_posturl'>Processing URL</label></td>\n\r
					<td><input name='eway_posturl' type='text' value='".$eway_url."' /></td>\n\r
				</tr>\n\r
				<tr>\n\r
					<td>&nbsp;</td>\n\r
					<td><small>URL of the secure payment page customers are sent to for payment processing. If unsure leave field blank for default settings.</small></td>\n\r
				</tr>\n\r";	
				
	$output .="	<tr>\n\r
					<td><label for='eway_resultsurl'>Response URL</label></td>\n\r
					<td><input name='eway_resultsurl' type='text' value='".$eway_resultsurl."' /></td>\n\r
				</tr>\n\r
				<tr>\n\r
					<td>&nbsp;</td>\n\r
					<td><small>URL of the secure payment page customers are sent to for payment processing. If unsure leave field blank for default settings.</small></td>\n\r
				</tr>\n\r";	
				
		$output .="	<tr>\n\r
						<td>Return URL</td>\n\r
						<td><input type='text' size='40' value='".get_option('eway_transact_url')."' name='eway_return_url' /></td>\n\r
				</tr>\n\r
				<tr>\n\r
					<td>&nbsp;</td>\n\r
					<td><small>URL that returns the payment response from eWay. If unsure leave field blank for default settings.</small></td>\n\r
				</tr>\n\r";	

	$output .="	<tr>\n\r
					<td>Debug Mode</td>\n\r
					<td>\n\r
						<input type='radio' value='1' name='eway_debug' id='eway_debug1' ".$eway_debug1." /> <label for='eway_debug1'>".__('Yes', 'wpsc')."</label> &nbsp;
						<input type='radio' value='0' name='eway_debug' id='eway_debug2' ".$eway_debug2." /> <label for='eway_debug2'>".__('No', 'wpsc')."</label>
					</td>\n\r
				</tr>\n\r";

	$output .="	<tr>\n\r
					<td style='border-bottom: medium none;' colspan='2'><strong class='form_group'>Forms Sent to Gateway</strong></td>\n\r
				</tr>\n\r
				
				<tr>\n\r
					<td>First Name Field</td>\n\r
					<td>\n\r<select name='eway_form[first_name]'>\n\r".nzshpcrt_form_field_list(get_option('eway_form_first_name'))."\n\r</select></td>\n\r
				</tr>\n\r
				<tr>\n\r
					<td>Last Name Field</td>\n\r
					<td>\n\r<select name='eway_form[last_name]'>\n\r".nzshpcrt_form_field_list(get_option('eway_form_last_name'))."\n\r</select></td>\n\r
				</tr>\n\r
				<tr>\n\r
					<td>Email Field</td>\n\r
					<td>\n\r<select name='eway_form[email]'>\n\r".nzshpcrt_form_field_list(get_option('eway_form_email'))."\n\r</select></td>\n\r
				</tr>\n\r
				<tr>\n\r
					<td>Address Field</td>\n\r
					<td>\n\r<select name='eway_form[address]'>\n\r".nzshpcrt_form_field_list(get_option('eway_form_address'))."\n\r</select></td>\n\r
				</tr>\n\r
				<tr>\n\r
					<td>City Field</td>\n\r
					<td>\n\r<select name='eway_form[city]'>\n\r".nzshpcrt_form_field_list(get_option('eway_form_city'))."\n\r</select></td>\n\r
				</tr>\n\r
				<tr>\n\r
					<td>State Field</td>\n\r
					<td>\n\r<select name='eway_form[state]'>\n\r".nzshpcrt_form_field_list(get_option('eway_form_state'))."\n\r</select></td>\n\r
				</tr>\n\r
				<tr>\n\r
					<td>Post Code Field</td>\n\r
					<td>\n\r<select name='eway_form[post_code]'>\n\r".nzshpcrt_form_field_list(get_option('eway_form_post_code'))."\n\r</select></td>\n\r
				</tr>\n\r
				<tr>\n\r
					<td>Country Field</td>\n\r
					<td>\n\r<select name='eway_form[country]'>\n\r".nzshpcrt_form_field_list(get_option('eway_form_country'))."\n\r</select></td>\n\r
				</tr>\n\r";

	return $output;
}

function submit_eway()
{
	if(isset($_POST['eway_customerid']))
	{ 
		update_option('eway_customerid', $_POST['eway_customerid']);		 
	}
	 
	if(isset($_POST['eway_username']))
	{ 
		update_option('eway_username', $_POST['eway_username']);		 
	}
	 
	if(isset($_POST['eway_currency_code']))
	{ 
		update_option('eway_currency_code', $_POST['eway_currency_code']);		 
	}
	 
	if(isset($_POST['eway_posturl']))
	{ 
		update_option('eway_posturl', $_POST['eway_posturl']);		 
	}

	if(isset($_POST['eway_resultsurl']))
	{ 
		update_option('eway_resultsurl', $_POST['eway_resultsurl']);		 
	}

	if(isset($_POST['eway_return_url']))
	{ 
		update_option('eway_transact_url', $_POST['eway_return_url']);		 
	}

  	if(isset($_POST['eway_debug']))
    {
    	update_option('eway_debug', $_POST['eway_debug']);
    }

    if (!isset($_POST['eway_form'])) $_POST['eway_form'] = array();
	foreach((array)$_POST['eway_form'] as $form => $value)
    {
    	update_option(('eway_form_'.$form), $value);
    }
	return true;
 
}

function gateway_eway($seperator, $sessionid)
{
	global $wpdb, $wpsc_cart;

	//This grabs the purchase log id from the database
	//that refers to the $sessionid
	$purchase_log = $wpdb->get_row("SELECT * FROM `".WPSC_TABLE_PURCHASE_LOGS."` WHERE `sessionid` = ".$sessionid." LIMIT 1", ARRAY_A) ;
	 
	//This grabs the users info using the $purchase_log
	// from the previous SQL query
	 
	$usersql = "SELECT * FROM `".WPSC_TABLE_CART_CONTENTS."` WHERE `purchaseid` = '".$purchase_log['id']."'";
	$userinfo = $wpdb->get_results($usersql, ARRAY_A);

	//Now we will store all the information into an associative array
	//called $data to prepare it for sending via cURL
	 
	//please note that the key in the array may need to be changed
	//to work with your gateway (refer to your gateways documentation).
	$data = array();
	 
	 //Account setup
	$data['CustomerID']	= get_option('eway_customerid');
	$data['UserName'] 	= get_option('eway_username');
	$data['Amount']	= number_format($wpsc_cart->total_price,2,'.','');
	$data['Currency'] = get_option('eway_currency_code');
	$data['ReturnURL'] = get_option('eway_transact_url');
	$data['CancelURL'] = get_option('eway_transact_url');
	$data['Language'] = "EN";
	//Company details
	$data['CompanyName'] = get_bloginfo( 'name', 'raw' );
	$data['CompanyLogo'] = get_bloginfo( 'name', 'raw' );
	$data['PageBanner'] = '';
	$data['PageTitle'] = get_bloginfo( 'name', 'raw' );
	$data['PageDescription'] = 'Qual';
	$data['PageFooter'] = get_bloginfo( 'name', 'raw' );
	//Extras
	$data['MerchantReference'] = $purchase_log['id'];
	$data['MerchantOption1'] = $sessionid;
	
	//print_r($wpsc_cart->cart_items);
	foreach($wpsc_cart->cart_items as $i => $Item)
	{
		
	}
	//Count the number of items in the shopping cart
	$cart_quantity = wpsc_cart_item_count();
	$product = "Product";
	if ($cart_quantity > 1)
		$product = "Products";
		
	$data['InvoiceDescription'] = $cart_quantity. " {$product}";


	//Customers billing info
	if ( !empty($_POST['collected_data'][get_option('eway_form_first_name')]) )
		$data['CustomerFirstName'] = $_POST['collected_data'][get_option('eway_form_first_name')];
		
	if ( !empty($_POST['collected_data'][get_option('eway_form_last_name')]) )
		$data['CustomerLastName'] = $_POST['collected_data'][get_option('eway_form_last_name')];
		
	if ( !empty($_POST['collected_data'][get_option('eway_form_email')]) )
		$data['CustomerEmail'] = $_POST['collected_data'][get_option('eway_form_email')];
	
	if ( !empty($_POST['collected_data'][get_option('eway_form_address')]) )
		$data['CustomerAddress'] = $_POST['collected_data'][get_option('eway_form_address')];
		
	if ( !empty($_POST['collected_data'][get_option('eway_form_city')]) )
		$data['CustomerCity'] = $_POST['collected_data'][get_option('eway_form_city')];
		
	if ( !empty($_POST['collected_data'][get_option('eway_form_state')]) )
		$data['CustomerState'] = $_POST['collected_data'][get_option('eway_form_state')];
		
	if ( !empty($_POST['collected_data'][get_option('eway_form_post_code')]) )
		$data['CustomerPostCode'] = $_POST['collected_data'][get_option('eway_form_post_code')];
		
	if ( !empty($_POST['collected_data'][get_option('eway_form_country')]) )
		$data['CustomerCountry'] = $_POST['collected_data'][get_option('eway_form_country')][0];

	//Debug mode ON
	if( get_option('eway_debug') == 1)
	{
		echo ("DEBUG MODE ON!!<br/>");
		echo("These are the product summary details purchased by the buyer:<br/>");
		echo("<pre>");
		print_r($data);
		echo ("</pre>");
	}

	//now we add all the information in the array into a long string
	 
	$transaction = "";
	 
	foreach($data as $key => $value)
	{
		//Convert the seperator if there already is a ?
		if(get_option('permalink_structure') != '')
			$seperator ="&";		
		else
			$seperator ="?";
		//////////			
		if (is_array($value))
		{
			foreach($value as $item)
			{
				if (strlen($transaction) > 0) 
					$transaction .= $seperator;
				
				$transaction .= "$key=".urlencode($item);
			}
		} else {
			if (strlen($transaction) > 0)
				$transaction .= $seperator;
			 
			$transaction .= "$key=".urlencode($value);
		}
	}
	$transaction = "?".	$transaction ;


	//Debug mode ON
	if( get_option('eway_debug') == 1)
	{
		echo ("DEBUG MODE ON!!<br/>");
		echo("This is the transaction string for eWay:<br/>");
		echo("<pre>");
		print_r(get_option('eway_posturl').$transaction);
		echo ("</pre>");
	}

	//Debug mode OFF
	if( get_option('eway_debug') == 0)
	{
		//Now we have the information we want to send to the gateway in a nicely formatted string we can setup the cURL
		$connection = curl_init();

		curl_setopt($connection, CURLOPT_URL, get_option('eway_posturl').$transaction);
		curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($connection, CURLOPT_HEADER, 1);
		curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
		 
		if (CURL_PROXY_REQUIRED == 'True') 
		{
			$proxy_tunnel_flag = (defined('CURL_PROXY_TUNNEL_FLAG') && strtoupper(CURL_PROXY_TUNNEL_FLAG) == 'FALSE') ? false : true;
			curl_setopt ($connection, CURLOPT_HTTPPROXYTUNNEL, $proxy_tunnel_flag);
			curl_setopt ($connection, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
			curl_setopt ($connection, CURLOPT_PROXY, CURL_PROXY_SERVER_DETAILS);
		}

		$useragent = 'WP e-Commerce plugin';
		 
		curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($connection, CURLOPT_NOPROGRESS, 1);
		curl_setopt($connection, CURLOPT_VERBOSE, 1);
		curl_setopt($connection, CURLOPT_FOLLOWLOCATION,0);
		curl_setopt($connection, CURLOPT_POST, 1);
		curl_setopt($connection, CURLOPT_POSTFIELDS, $transaction);
		curl_setopt($connection, CURLOPT_TIMEOUT, 30);
		curl_setopt($connection, CURLOPT_USERAGENT, $useragent);
	
		$response = curl_exec($connection);
		curl_close($connection);
		
		$responsemode = fetch_data($response, '<result>', '</result>');
	    $responseurl = fetch_data($response, '<uri>', '</uri>');

	}
	
	if( get_option('eway_debug') == 1)
	{
		echo ("DEBUG MODE ON!!<br/>");
		echo("This is the response from eWay:<br/>");
		echo("<pre>");
		print_r("MODE: ".$responsemode."<br>");
		print_r("URL: ".$responseurl);
		echo ("</pre>");
	}

	if( get_option('eway_debug') == 0)
	{
		if( $responsemode == "True" )
		{ 			  	  	
			//$sql = "UPDATE `".WPSC_TABLE_PURCHASE_LOGS."` SET `processed`= '2' WHERE `sessionid`=".$sessionid;
			 
			//$wpdb->query($sql);
			
			unset($_SESSION['WpscGatewayErrorMessage']);
			wp_redirect( $responseurl );
			exit;
		} else {
			//redirect back to checkout page with errors
			$sql = "UPDATE `".WPSC_TABLE_PURCHASE_LOGS."` SET `processed`= '5' WHERE `sessionid`=".$sessionid;
			$wpdb->query($sql);
			 
			$transact_url = get_option('checkout_url');
	
			$_SESSION['WpscGatewayErrorMessage'] = __('Sorry your transaction did not go through successfully, please try again.');
			 
			wp_redirect( $transact_url );
			exit;
		}
	}

}

function gatewayresults_eway()
{
	if (isset($_REQUEST["AccessPaymentCode"]))
	{
		global $wpdb;

		//Now we have the information we want to send to the gateway in a nicely formatted string we can setup the cURL
		//echo $_REQUEST["AccessPaymentCode"];
		 
		 //Account setup
		$data = array();
		$data['CustomerID']	= get_option('eway_customerid');
		$data['UserName'] 	= get_option('eway_username');
		
		$eway_responseurl = get_option('eway_resultsurl').'?CustomerID='.$data['CustomerID'].'&UserName='.$data['UserName'].'&AccessPaymentCode='.$_REQUEST["AccessPaymentCode"];
	//echo $eway_responseurl;
	//echo $_REQUEST["AccessPaymentCode"];
	//die();
		$connection = curl_init();
		curl_setopt($connection, CURLOPT_URL, $eway_responseurl);
		curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($connection, CURLOPT_HEADER, 1);
		curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
		
		if (CURL_PROXY_REQUIRED == 'True') 
		{
			$proxy_tunnel_flag = (defined('CURL_PROXY_TUNNEL_FLAG') && strtoupper(CURL_PROXY_TUNNEL_FLAG) == 'FALSE') ? false : true;
			curl_setopt ($connection, CURLOPT_HTTPPROXYTUNNEL, $proxy_tunnel_flag);
			curl_setopt ($connection, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
			curl_setopt ($connection, CURLOPT_PROXY, CURL_PROXY_SERVER_DETAILS);
		}
		
		//An array for the merchant return values
		$return = array();
		
		$response = curl_exec($connection);
		$return['authecode'] = fetch_data($response, '<authCode>', '</authCode>');
		$return['responsecode'] = fetch_data($response, '<responsecode>', '</responsecode>');
		$return['retrunamount'] = fetch_data($response, '<returnamount>', '</returnamount>');
		$return['trxnnumber'] = fetch_data($response, '<trxnnumber>', '</trxnnumber>');
		$return['trxnstatus'] = fetch_data($response, '<trxnstatus>', '</trxnstatus>');
		$return['trxnresponsemessage'] = fetch_data($response, '<trxnresponsemessage>', '</trxnresponsemessage>');
		
		$return['merchantoption1'] = fetch_data($response, '<merchantoption1>', '</merchantoption1>');
		$return['merchantoption2'] = fetch_data($response, '<merchantoption2>', '</merchantoption2>');
		$return['merchantoption3'] = fetch_data($response, '<merchantoption3>', '</merchantoption3>');
		$return['merchantreference'] = fetch_data($response, '<merchantreference>', '</merchantreference>');
		$return['merchantinvoice'] = fetch_data($response, '<merchantinvoice>', '</merchantinvoice>');
		
		$response = curl_exec($connection);
		curl_close($connection);
		
		
		if ($return['trxnstatus'] == 'True')
		{
			//Update the transaction as pending
			//Collect all the eWay codes
			$eway_codes = eway_response_codes();

			//Check to see if the transaction from the merchant was valid
			if ( $eway_codes[$return['responsecode']]['status'] == true )
			{
                            
                                wpsc_update_purchase_log_status($return['merchantreference'],3);
                                
			}
			//Transaction worked so remove the errors
			unset($_SESSION['wpsc_checkout_misc_error_messages']);

			$location = add_query_arg('sessionid', $return['merchantoption1'], get_option('transact_url'));
			
			//Redirect to the response page
			wp_redirect($location);
			exit;
		}
	
	}

}

/**
  * Get Response Codes
  * This will return all the responses that eWay send
  */
function eway_response_codes()
{
	$response_code = array();
	
	$response_code['CX'] = array('msg'=>'Customer Cancelled Transaction', 'status'=>false);
	$response_code['00'] = array('msg'=>'Transaction Approved', 'status'=>true);
	$response_code['01'] = array('msg'=>'Refer to Issuer', 'status'=>false);
	$response_code['02'] = array('msg'=>'Refer to Issuer, special', 'status'=>false);
	$response_code['03'] = array('msg'=>'No Merchant', 'status'=>false);
	$response_code['04'] = array('msg'=>'Pick Up Card', 'status'=>false);
	$response_code['05'] = array('msg'=>'Do Not Honour', 'status'=>false);
	$response_code['06'] = array('msg'=>'Error', 'status'=>false);
	$response_code['07'] = array('msg'=>'Pick Up Card, Special', 'status'=>false);
	$response_code['08'] = array('msg'=>'Honour With Identification', 'status'=>true);
	$response_code['09'] = array('msg'=>'Request In Progress', 'status'=>false);
	$response_code['10'] = array('msg'=>'Approved For Partial Amount', 'status'=>true);
	$response_code['11'] = array('msg'=>'Approved, VIP', 'status'=>true);
	$response_code['12'] = array('msg'=>'Invalid Transaction', 'status'=>false);
	$response_code['13'] = array('msg'=>'Invalid Amount', 'status'=>false);
	$response_code['14'] = array('msg'=>'Invalid Card Number', 'status'=>false);
	$response_code['15'] = array('msg'=>'No Issuer', 'status'=>false);
	$response_code['16'] = array('msg'=>'Approved, Update Track 3', 'status'=>true);
	$response_code['19'] = array('msg'=>'Re-enter Last Transaction', 'status'=>false);
	$response_code['21'] = array('msg'=>'No Action Taken', 'status'=>false);
	$response_code['22'] = array('msg'=>'Suspected Malfunction', 'status'=>false);
	$response_code['23'] = array('msg'=>'Unacceptable Transaction Fee', 'status'=>false);
	$response_code['25'] = array('msg'=>'Unable to Locate Record On File', 'status'=>false);
	$response_code['30'] = array('msg'=>'Format Error', 'status'=>false);
	$response_code['31'] = array('msg'=>'Bank Not Supported By Switch', 'status'=>false);
	$response_code['33'] = array('msg'=>'Expired Card, Capture', 'status'=>false);
	$response_code['34'] = array('msg'=>'Suspected Fraud, Retain Card', 'status'=>false);
	$response_code['35'] = array('msg'=>'Card Acceptor, Contact Acquirer, Retain Card', 'status'=>false);
	$response_code['36'] = array('msg'=>'Restricted Card, Retain Card', 'status'=>false);
	$response_code['37'] = array('msg'=>'Contact Acquirer Security Department, Retain Card', 'status'=>false);
	$response_code['38'] = array('msg'=>'PIN Tries Exceeded, Capture', 'status'=>false);
	$response_code['39'] = array('msg'=>'No Credit Account', 'status'=>false);
	$response_code['40'] = array('msg'=>'Function Not Supported', 'status'=>false);
	$response_code['41'] = array('msg'=>'Lost Card', 'status'=>false);
	$response_code['42'] = array('msg'=>'No Universal Account', 'status'=>false);
	$response_code['43'] = array('msg'=>'Stolen Card', 'status'=>false);
	$response_code['44'] = array('msg'=>'No Investment Account', 'status'=>false);
	$response_code['51'] = array('msg'=>'Insufficient Funds', 'status'=>false);
	$response_code['52'] = array('msg'=>'No Cheque Account', 'status'=>false);
	$response_code['53'] = array('msg'=>'No Savings Account', 'status'=>false);
	$response_code['54'] = array('msg'=>'Expired Card', 'status'=>false);
	$response_code['55'] = array('msg'=>'Incorrect PIN', 'status'=>false);
	$response_code['56'] = array('msg'=>'No Card Record', 'status'=>false);
	$response_code['57'] = array('msg'=>'Function Not Permitted to Cardholder', 'status'=>false);
	$response_code['58'] = array('msg'=>'Function Not Permitted to Terminal', 'status'=>false);
	$response_code['59'] = array('msg'=>'Suspected Fraud', 'status'=>false);
	$response_code['60'] = array('msg'=>'Acceptor Contact Acquirer', 'status'=>false);
	$response_code['61'] = array('msg'=>'Exceeds Withdrawal Limit', 'status'=>false);
	$response_code['62'] = array('msg'=>'Restricted Card', 'status'=>false);
	$response_code['63'] = array('msg'=>'Security Violation', 'status'=>false);
	$response_code['64'] = array('msg'=>'Original Amount Incorrect', 'status'=>false);
	$response_code['66'] = array('msg'=>'Acceptor Contact Acquirer, Security', 'status'=>false);
	$response_code['67'] = array('msg'=>'Capture Card', 'status'=>false);
	$response_code['75'] = array('msg'=>'PIN Tries Exceeded', 'status'=>false);
	$response_code['82'] = array('msg'=>'CVV Validation Error', 'status'=>false);
	$response_code['90'] = array('msg'=>'Cutoff In Progress', 'status'=>false);
	$response_code['91'] = array('msg'=>'Card Issuer Unavailable', 'status'=>false);
	$response_code['92'] = array('msg'=>'Unable To Route Transaction', 'status'=>false);
	$response_code['93'] = array('msg'=>'Cannot Complete, Violation Of The Law', 'status'=>false);
	$response_code['94'] = array('msg'=>'Duplicate Transaction', 'status'=>false);
	$response_code['96'] = array('msg'=>'System Error', 'status'=>false);
	
	return $response_code;
}

function fetch_data($string, $start_tag, $end_tag)
{
	$position = stripos($string, $start_tag);  
	$str = substr($string, $position);  		
	$str_second = substr($str, strlen($start_tag));  		
	$second_positon = stripos($str_second, $end_tag);  		
	$str_third = substr($str_second, 0, $second_positon);  		
	$fetch_data = trim($str_third);		
	return $fetch_data; 
}


add_action('init', 'gatewayresults_eway');
