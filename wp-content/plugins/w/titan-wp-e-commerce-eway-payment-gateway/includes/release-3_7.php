<?php
if( is_admin() ) {

	function titan_eway_add_modules_admin_pages( $page_hooks,$base_page ) {

		$page_hooks[] = add_submenu_page( $base_page,__( 'Titan WP E-Commerce eWay Gateway','vl_wpsccc' ),__( 'Titan WP E-Commerce eWay Gateway','vl_wpsccc' ),7,'vl_wpsccc','vl_wpsccc_html_page' );
		return $page_hooks;

	}
	add_filter( 'wpsc_additional_pages','vl_wpsccc_add_modules_admin_pages',10,2 );

}
?>
