<?php
function titan_eway_check_merchant_exists() {

	switch( wpsc_get_major_version() ) {

		case '3.7':
			$directory = 'merchants';
			break;

		case '3.8':
			$directory = 'wpsc-merchants';
			break;

	}
	if( !file_exists( WP_PLUGIN_DIR . '/wp-e-commerce/' . $directory . '/eway.php' ) ) {
		return true;
	} else {
		return false;
	}

}

if( !function_exists( 'wpsc_get_major_version' ) ) {

	function wpsc_get_major_version() {

		$version = get_option( 'wpsc_version' );
		return substr( $version,0,3 );

	}

}
?>