<?php
/*
Plugin Name: Titan Business Details
Plugin URI: http://plugins.staging.titanweb.com.au
Description: <strong>Requires WordPress 3.3 or later.</strong> Enables you to add business details and settings to your site.
Version: 2.2.1
License: GPL
Author: Ricky Fang, Earl Evan Amante (Titan Interactive)
Author URI: http://www.titanweb.com.au/
*/

class Titan_Business_Details
{
	static $instance;
	static $titan_version = '2.2.1';
    static $titan_author = 'Ricky Fang, Earl Evan Amante';
	static $plugin_remote_path = 'http://plugins.staging.titanweb.com.au/titan-business-details/update.php';
	var $titan_warning;
	var $titan_plugin_slug = 'titan-business-details';
	var $titan_plugin_name = "Business Details";
	var $themeURL;
	var $shortcodeBusiness;
	var $formID;
        var $has_access = false;

	var $arrayBusiness = array();
	public function __construct()
	{
		self::$instance = $this;
		$this->init();
	}

	public function init()
	{
		require_once( 'includes/wp_auto_update.php' );
		$this->themeURL = get_template_directory().'/business-details-themes/';
		$this->arrayBusiness = maybe_unserialize(get_option($this->titan_plugin_slug."-business-list"));
		//Check to see if the admin options form has been submitted with changes
		isset($_REQUEST['_wp_titan_nonce']) && isset($_REQUEST[$this->titan_plugin_slug]) ? add_action('admin_init',array($this,'titan_options_save')) : null;
		add_action('admin_head',array($this,'titan_admin_css'));
        add_action('admin_head',array($this,'titan_admin_js'));
		add_action('admin_menu',array($this,'titan_options_menu'));
		add_action('init', array(&$this, 'tbd_activate_auto_update'));
		//add_filter('the_content', array($this, 'add_contact_form'));
		add_shortcode($this->titan_plugin_slug, array($this, 'titan_shortcodes'));
                add_action('wp_ajax_remove_extra_detail', array( &$this, 'remove_extra_detail' ) );
                add_action('wp_ajax_add_extra_detail', array( &$this, 'add_extra_detail' ) ) ;
                add_action( 'wp_ajax_titan_update_google_tracking_id', array( &$this, 'update_tracking_id' ) );
                add_action( 'wp_head', array( &$this, 'add_tracking_code' ) );
                add_action( 'admin_notices', array( &$this, 'add_admin_notice' ) );
                add_action( 'init', array( &$this, 'check_asscess' ) );


	}

	function check_asscess() {
		global $current_user;
		$selected_roles = get_option( 'titan-business-details-roles', array('administrator') );
		if ( empty( $selected_roles ) ) {
			$selected_roles = array( 'administrator' );
		}
		$user_roles = $current_user->roles;
		foreach ( $user_roles as $user_role ) {
			if (in_array($user_role, $selected_roles) ) {
				$this->has_access = true;
				break;
			}
		}

	}

	/**
	*Activate Auto Update so Admin can update plugins from WordPress
	**/
	function tbd_activate_auto_update()
	{
		if( class_exists( 'wp_auto_update' ) ) {
			$slug = plugin_basename(__FILE__);
			new wp_auto_update ( self::$titan_version, self::$plugin_remote_path, $slug );
		}
	}

	/*
	 * Add Titan Business Details Stylesheet to admin head
	 */
	public function titan_admin_css()
	{
		$this->titan_enqueue_style($this->titan_plugin_slug, plugins_url('', __FILE__ ) . '/css/'.$this->titan_plugin_slug.'.css', $deps = array(), $ver = self::$titan_version, $media = 'all' );
		return;
	}

   /*
   * Add Javascript for dynamic input field update
   */
	public function titan_admin_js()
	{
	   $this->titan_enqueue_js($this->titan_plugin_slug, plugins_url('', __FILE__ ) . '/js/'.$this->titan_plugin_slug.'.js', $deps = array(), $ver = self::$titan_version, $media = 'all' );
	   return;
	}


	/*
	 * Add Titan Business Details Options Page to Settings menu
	 */
	public function titan_options_menu()
	{
                if(function_exists('add_submenu_page'))
                {

                        if( $this->has_access ) {
                            //add_options_page($this->titan_plugin_name, $this->titan_plugin_name, 'manage_options', $this->titan_plugin_slug, array($this,'titan_options_page'));
                            $icon = plugins_url('', __FILE__ ) . '/img/'.$this->titan_plugin_slug.'.png';
                            add_menu_page($this->titan_plugin_name, $this->titan_plugin_name, 'read', $this->titan_plugin_slug, array($this,'titan_list_business_page'), $icon, 3);
                            add_submenu_page( $this->titan_plugin_slug, $this->titan_plugin_name.' List', 'Edit Business', 'read', 'edit-business-details', array($this,'titan_options_page')); 
                            add_submenu_page( $this->titan_plugin_slug, $this->titan_plugin_name.' Option', 'Settings', 'add_users', 'business-detail-settings', array($this,'titan_settings_page')); 
                        }
                }
	}

    public function titan_settings_page(){
        global $wp_roles;
        $all_roles = $wp_roles->roles;
        if( $_POST['titan-business-details-settings_nonce'] && wp_verify_nonce( $_POST['titan-business-details-settings_nonce'], 'titan-business-details-settings' ) ) {
            update_option( 'titan-business-details-roles', $_POST['titan-business-details-roles'] );
        }
        $selected_roles = get_option( 'titan-business-details-roles', array('administrator') );

        ?>
        <div id="icon-options-general" class="icon32"><br></div>
        <h2>Plugin Settings</h2>
        <form method="post">
            <table class="form-table">
                <tr>
                    <th>Roles to use this plugin</th>
                    <td>
                        <?php foreach( $all_roles as $key=>$role ):?>
                        <?php if( $key == 'administrator' ) : //admin is always checked?>
                        <input type="checkbox" checked="checked" disabled="disabled" name="titan-business-details-roles[]" value="<?php echo $key; ?>"> <?php echo $role['name']?>
                        <input type="hidden" name="titan-business-details-roles[]" value="<?php echo $key; ?>" />
                        <?php else:?>
                            <input type="checkbox" <?php checked( true, in_array( $key, $selected_roles ) );?> name="titan-business-details-roles[]" value="<?php echo $key; ?>"> <?php echo $role['name']?>
                        <?php endif;?>
                        <br />
                        <?php endforeach;?>
                    </td>
                </tr>
            </table>
            <?php wp_nonce_field( 'titan-business-details-settings', 'titan-business-details-settings_nonce' ); ?>
            <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"></p>
        </form>


        <?php
    }

	/*
	 * Titan Business Details Options Save
	 */
	public function titan_options_save()
	{
		// Save the posted value in the database
		if(isset($_POST['titan-business-details-business-name']) && (!$this->checkBusiness($_POST['titan-business-details-business-name']))){
		    $businessName = $this->removeBackSlash(trim($_POST['titan-business-details-business-name']));
			$businessName = $this->clean_business_name($businessName, FALSE);
			
		    if($businessName != ''){
			 $this->arrayBusiness[] = array('name' => sanitize_text_field($businessName));
			 $this->updateBusinessList($this->arrayBusiness);
			}
		} else {
			foreach( $_POST as $formDatakey => $formDataValue){
				update_option(	$formDatakey , $this->removeBackSlash( htmlspecialchars( $formDataValue ) ) );
			}
		}
	}

	/*
	* Add the contact form and tempales
	*/
	public function add_contact_form($content){
		return($this->check_for_titan_shortcode($content));

		//return $content;
	}



	/*
	 * Return requested options via a shortcode
	 */
	public function titan_shortcodes($atts)
	{
		$this->shortcodeBusiness = explode(',', $atts['name']);
		$name = $atts['name'];
        $name = str_replace(' ', '-', $name);
        if (isset($atts['template']) && $atts['template'] != ""){
			$this->formID = $atts['form'];
			$this->includeCSS($atts['template']);
			return $this->includeTemplate($atts['template']);
		}
        elseif ( get_option( 'titan-'. $name.'-'.$atts['value'] ) )
		{
			if ($atts['value'] == 'gmaps')
				return html_entity_decode(stripcslashes(get_option('titan-'.$name.'-gmaps')));
			elseif(!empty($atts['value']))
				return get_option('titan-'. $name .'-'.$atts['value']);
		}

	}




	public function titan_list_business_page()
	{

			include( dirname( __FILE__ ) . '/includes/business-list.php');
	}
	/*
	 * Titan Admin Options Page
	 */
	public function titan_options_page()
	{
		if ( !isset($_GET['businessName']) || $_GET['businessName'] == '' ){
			include( dirname( __FILE__ ) . '/includes/business-list.php');
		} elseif ( !isset($_GET['action']) ) {
			$singleBusinessName = $this->removeBackSlash('titan-'.$_GET['businessName']);
			// Fetch added fields through query
			// These variables will be used as extra details on Business category details
			$extra_business_details = $this->get_extra_fields($singleBusinessName.'-cat_business');
			$extra_physical_address = $this->get_extra_fields($singleBusinessName.'-cat_physical');
			$extra_postal_address = $this->get_extra_fields($singleBusinessName.'-cat_postal');
			$extra_opening_hours = $this->get_extra_fields($singleBusinessName.'-cat_opening');
			$extra_social_links = $this->get_extra_fields($singleBusinessName.'-cat_social');

			include( dirname( __FILE__ ) . '/includes/business-details-form.php');
		} elseif (isset($_GET['action']) && $_GET['action'] == 'delete') {
			if($this->checkBusiness($this->removeBackSlash(str_replace('-' ,' ',$_GET['businessName'])))){
				if ( $this->deleteBusiness($this->removeBackSlash(str_replace('-' ,' ',$_GET['businessName'])))) {
					$this->arrayBusiness = maybe_unserialize(get_option($this->titan_plugin_slug."-business-list"));
					include( dirname( __FILE__ ) . '/includes/business-list.php');
				} else {
					include( dirname( __FILE__ ) . '/includes/business-list.php');
				}
			} else {
				include( dirname( __FILE__ ) . '/includes/business-list.php');
			}
		}

	}

	/*
	 * Function that mimics the core wp_enqueue_style function which doesn't appear to work on the login page
	 */
	public function titan_enqueue_style($handle='',$file='', $deps = array(), $ver = self::titan_version, $media = 'all')
	{
		echo '<link rel="stylesheet" id="' . $handle . '-css" href="' . $file . '?version=' . $ver .'" type="text/css" media="' . $media . '" />'."\n";
		return;
	}

	public function titan_enqueue_js($handle='',$file='', $deps = array(), $ver = self::titan_version, $media = 'all')
	{
		echo '<script src="' . $file . '?version=' . $ver .'" type="text/javascript"></script>'."\n";
		return;
	}

	/*
	* check to see if business exists
	*/
	private function checkBusiness($businessName){
		$found = false;
		if (count($this->arrayBusiness) > 0){
                    foreach ($this->arrayBusiness as $business){
                            //echo $business['name'].' - '. $businessName;
                            if ($business['name'] == $businessName){
                                    $found = true;
                                    break;
                            }
                    }
		}
		return $found;
	}
	private function deleteBusiness($businessName){
		$newArray = array();
		foreach ($this->arrayBusiness as $business){
			if ($business['name'] != $businessName){
				//echo
				$newArray[] = array('name' => $business['name']);
			}
		}
		if ($this->updateBusinessList($newArray)){
			return true;
		} else {
			return false;
		}
	}
	private function updateBusinesslist($arr){
		if (update_option ($this->titan_plugin_slug."-business-list", serialize($arr))){
		    $this->arrayBusiness = maybe_unserialize( get_option($this->titan_plugin_slug."-business-list") );
			return true;
		} else {
			return false;
		}
	}
	public function includeTemplate($filename){
		include($this->themeURL.$filename.'.php');
	}
	public function includeCSS($filename){
		wp_enqueue_style('titan-business-form',get_bloginfo('template_url').'/business-details-themes/css/'.$filename.'.css','','',false );

	}
	public function check_for_titan_shortcode($content){
		$stringArray = explode('[titan-business-details-contact ', $content);
		if (count($stringArray) > 1){
			$stringArray[1] = str_replace(']','',$stringArray[1]);
			$attributes = explode(' ', $stringArray[1]);
			if (count($attributes) > 1){
				$attArray = array();
				foreach ($attributes as $attribute){
					$tempArr = explode('=', $attribute);
					$attArray [$tempArr[0]] =  $tempArr[1];
				}
				$this->shortcodeBusiness = explode(',', $attArray['name']);
				$this->formID = $attArray['form'];
				$incTemp = $this->includeTemplate($attArray['template']);

				return($stringArray[0]);
			}
		}
	}
	public function removeBackSlash($data){
		return str_replace("\'", "'", $data);
	}
	public function clean_business_name($business_name, $spaces=TRUE) {
		if(!$spaces)
			return preg_replace('/[^A-Za-z0-9\- ]/', '', $business_name); // Removes special chars.
		$business_name = str_replace(' ', '-', $business_name); // Replaces all spaces with hyphens.
		return preg_replace('/[^A-Za-z0-9\-]/', '', $business_name); // Removes special chars.
	}

	/* Apply details on rows fetch from DB */
	public function get_extra_fields($option_name = '',$show_HTML = TRUE){

	  global $wpdb;
      $result = array();

	  $query = 'SELECT * FROM wp_options wo WHERE option_name LIKE "%s" ORDER BY option_id DESC';
	  $rows = $wpdb->get_results($wpdb->prepare($query,'%'.$option_name.'%'));

	  if($show_HTML){
	   if(sizeof($rows) > 0){

	    $business_name = $this->removeBackSlash('titan-'.$_GET['businessName']);
	    $shortcodeName =  $this->removeBackSlash($_GET['businessName']);

		$pattern = '/cat_(.*)_(.*)/';

	    // Populate the extra fields through HTML
	    foreach($rows as $row){

		 preg_match($pattern,$row->option_name,$match);

		 //$nice = str_ireplace('_',' ',ucwords(str_ireplace('-','',strrchr($row->option_name,"-"))));

		 $nice = ucwords(str_replace('-',' ',$match[2]));

		 $nice_shortcode = str_ireplace($business_name.'-','',$row->option_name);

		 $populate_html = ' <p>
										  <label for="'.$row->option_name.'">'.$nice.':</label>
                                          <input type="text" name="'.$row->option_name.'" id="'.$row->option_name.'" size="45" value="'.$row->option_value.'" /> [titan-business-details value='.$nice_shortcode.' name='.$shortcodeName.']
										  <input type="button" id="'. $row->option_id.'" rel="'.$row->option_name.'" class="button remove_extra_field" value="Remove"/>
										 </p>';

	      $result[] = array(
		   'option_id' => $row->option_id,
		   'populate_html' => $populate_html,
		   'nice_shortcode' => $nice_shortcode
		  );
	     }
	   }
	  }
	  else{
	   $result = $rows;
	  }

	  return $result;

	}

	/* Remove extra field option  */
	public function remove_extra_field($option_id = NULL,$option_name = NULL){

	 global $wpdb;

	 $query = 'DELETE FROM wp_options WHERE option_id = %d AND option_name = %s LIMIT 1';
	 $result = $wpdb->query($wpdb->prepare($query,array($option_id,$option_name)));

	 // Data exist and was successfully removed
	 if($result)
	  echo json_encode(array('error'=>'0'));
	 else
	   echo json_encode(array('error'=>'1'));

	}

	/* Add/Save extra field option through AJAX */
	public function add_extra_field($option_name = NULL,$option_value = NULL){

	  /*
	   *Fast and Lazy method
	   *Get business name since $_GET is not accessible through AJAX
	  */

	  $pattern = '/titan-(.*)-cat/';
	  preg_match_all($pattern,$option_name,$matches);
	  $business_name = 'titan-'.$matches[1][0];
	  $shortcodeName = $matches[1][0];

	  $option_name = sanitize_title_with_dashes($this->removeBackSlash($option_name));
	  $option_name = str_ireplace($shortcodeName,ucwords($shortcodeName),$option_name);
	  $option_value  = trim($this->removeBackSlash($option_value));

	  update_option($option_name,$option_value);

	  // Check if data was inserted successfully.
	  $result = $this->get_extra_fields($option_name,FALSE);

	  if(is_array($result) && !empty($result)){

	   $option_id = $result[0]->option_id;

	   //$nice = str_ireplace('_',' ',ucwords(str_ireplace('-','',strrchr($option_name,"-"))));

	   $pattern = '/cat_(.*)_(.*)/';
	   preg_match($pattern,$option_name,$match);

	   $nice = ucwords(str_replace('-',' ',$match[2]));
	   $nice_shortcode = str_ireplace($business_name.'-','',$option_name);

	   /*
	    Return HTML as result
	   */
	   $populate_html = ' <p>
										  <label for="'.$option_name.'">'.$nice.':</label>
                                          <input type="text" name="'.$option_name.'" id="'.$option_name.'" size="45" value="'.$option_value.'" />&nbsp;<span class="select_sc">[titan-business-details value='.$nice_shortcode.' name='.$shortcodeName.']</span>
										  <input type="button" id="'. $option_id.'" rel="'.$option_name.'" class="button remove_extra_field" value="Remove"/>
									</p>';
	   $response = array('error'=>0,'content'=>$populate_html,'oid'=>$option_id,'option_name'=>$option_name);
	   }
	   else{
	    $response = array('error'=>1);
	   }
	  echo json_encode($response);
	}

        function remove_extra_detail(){
            $option_id = (int) $_POST['id'];
            $option_name = $_POST['name'];
            $this->remove_extra_field($option_id,$option_name);
            die;
        }

        function add_extra_detail(){
            $name = $_POST['name'];
            $option_name = trim($_POST['label']);
            $option_value = $_POST['value'];

            // Easiest but lazy solution :D
            $option_name = str_replace('-value[]','_'.$option_name,$name);
            $this->add_extra_field($option_name,$option_value);
            die;
        }

        function update_tracking_id() {
            update_option( 'titan-google-analytics-id', $_POST['tracking_id'] );
			update_option( 'titan-google-analytics-id-2', $_POST['tracking_id_2'] );
			update_option( 'titan-google-analytics-id-3', $_POST['tracking_id_3'] );
            update_option( 'titan-insert-tracking-code-uni', $_POST['insert_tracking_code_uni'] );
            update_option( 'titan-insert-tracking-code', $_POST['insert_tracking_code'] );
            die();
        }

        function add_tracking_code() {
            $tracking_id = get_option( 'titan-google-analytics-id' );
			$tracking_id_2 = get_option( 'titan-google-analytics-id-2' );
			$tracking_id_3 = get_option( 'titan-google-analytics-id-3' );
            $insert_tracking_code_uni = get_option( 'titan-insert-tracking-code-uni' );
			$insert_tracking_code = get_option( 'titan-insert-tracking-code' );

            $host = $_SERVER['SERVER_NAME'];
            $host = str_replace( 'www.', '', $host );

            if( !empty( $tracking_id ) && $insert_tracking_code == 'yes' ) {

			    if( $insert_tracking_code_uni == 'yes' ){
				// Universal Analytics Tracking Code
                ?>
				<script>
				  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
					ga('create', '<?php echo $tracking_id;?>', '<?php echo $host; ?>');
					ga('send', 'pageview');
					<?php if(!empty( $tracking_id_2 ) && isset( $tracking_id_2 )){ ?>
ga('create', '<?php echo $tracking_id_2;?>', 'auto', {'name': 'Titan'});
					ga('Titan.send', 'pageview');
					<?php }
if(!empty( $tracking_id_3 ) && isset($tracking_id_3)){ ?>
					ga('create', '<?php echo $tracking_id_3;?>', 'auto', {'name': 'Titan2'});
					ga('Titan2.send', 'pageview');
					<?php } ?>
</script>

                <?php
				} else {
				// Standard Analytics Tracking Code
				?>
				<script type="text/javascript">
					var analyticsFileTypes = [''];
					var analyticsEventTracking = 'enabled';
				</script>
				<script type="text/javascript">
					var _gaq = _gaq || [];
					_gaq.push(['_setAccount', '<?php echo $tracking_id;?>']);
					_gaq.push(['_trackPageview']);
					<?php if(!empty( $tracking_id_2 ) && isset( $tracking_id_2 )){ ?>
_gaq.push(['titan._setAccount', '<?php echo $tracking_id_2;?>']);
					_gaq.push(['titan._trackPageview']);
					<?php } ?>
					<?php if(!empty( $tracking_id_3 ) && isset($tracking_id_3)){ ?>
_gaq.push(['titan2._setAccount', '<?php echo $tracking_id_3;?>']);
					_gaq.push(['titan2._trackPageview']);
					<?php } ?>
(function() {
					var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
					})();
				</script>
                <?php
				}

            }

        }

        function add_admin_notice() {
            if( !$this->has_access ) {
                return;
            }
            $tracking_id = get_option( 'titan-google-analytics-id' );
            if( empty( $tracking_id ) ):
            ?>
                <div class="error">
                    <p>You have not set your 'Google Analytics Tracking ID', please add <a href='<?php menu_page_url( $this->titan_plugin_slug ); ?>'>here</a>. </p>
                </div>

            <?php
            endif;
        }

}

$titan_tbd = new Titan_Business_Details();