<?php $theName = $this->shortcodeBusiness[0]; ?>
		<div class="contact-wrapper section-wrapper">
			<div class="section-pad-wrapper">
				<div class="contact-left section-block-wrapper left">
					<div class="section-pad-wrappper">
					<?php 
					gravity_form($this->formID,false,false); ?>
					</div>
				</div>
                <div class="contact-right contact-info section-block-wrapper right">
					<div class="section-pad-wrapper">
						<ul class="section-pad-wrapper">
							<li class="left">
								<h4>Phone</h4>
								<?php echo do_shortcode("[titan-business-details value=phone name=$theName]") ?>
								<h4>Fax</h4>
								<?php echo do_shortcode("[titan-business-details value=fax name=$theName]") ?>
								<h4>Email</h4>
								<?php echo do_shortcode("[titan-business-details value=enquiries-email name=$theName]") ?>
							</li>
							<li class="right">
								<h4>Address</h4>
								<?php echo do_shortcode("[titan-business-details value=address-1 name=$theName]") ?> <br />
								<?php echo do_shortcode("[titan-business-details value=suburb name=$theName]") ?> <?php echo do_shortcode("[titan-business-details value=state name=$theName]") ?> <?php echo do_shortcode("[titan-business-details value=postcode name=$theName]") ?>
								<h4>Showroom & Office Hours</h4>
								<table width="100%">
									<tr>
										<td>Mon - Thurs</td>
										<td><?php echo do_shortcode("[titan-business-details value=opening-mon-fri name=$theName]") ?> </td>	
									</tr>
									<tr>
										<td>Fri</td>
										<td><?php echo do_shortcode("[titan-business-details value=opening-fri name=$theName]") ?> </td>	
									</tr>
									<tr>
										<td>Sat</td>
										<td><?php echo do_shortcode("[titan-business-details value=opening-sat name=$theName]") ?> </td>	
									</tr>
								</table>
							</li>
						</ul>
					</div>
					<div class="google-maps section-block-wrapper">
						<?php 
						$contactAddress = do_shortcode("[titan-business-details value=address-1 name=$theName]") .  ' ' . do_shortcode("[titan-business-details value=suburb name=$theName]") . ' ' . do_shortcode("[titan-business-details value=state name=$theName]") . ' ' . do_shortcode("[titan-business-details value=postcode name=$theName]");
						echo do_shortcode('[google-map-v3 width="458" height="192" zoom="12" maptype="roadmap" mapalign="center" directionhint="false" language="default" poweredby="false" maptypecontrol="true" pancontrol="true" zoomcontrol="true" scalecontrol="true" streetviewcontrol="true" scrollwheelcontrol="false" draggable="true" tiltfourtyfive="false" addmarkermashupbubble="false" addmarkermashupbubble="false" addmarkerlist="'.$contactAddress.'" bubbleautopan="true" showbike="false" showtraffic="false" showpanoramio="false"]'); ?>
					</div>
				</div>
				
			</div>
		</div>