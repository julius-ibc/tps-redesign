<div class="wrap webby">
<?php $shortcodeName = $this->removeBackSlash($_GET['businessName']); ?>
		<div id="icon-options-general" class="icon32"><br /></div>
		<h2><?php echo $this->titan_plugin_name.' Edit '. $this->removeBackSlash(str_replace('-',' ',$_GET['businessName'])); ?> </h2>
        <div class="metabox-holder">
            <div id="poststuff" class="column-1">
    
                    <form method="post" action="" id="<?php echo $this->titan_plugin_slug; ?>">
                        
                        <div class="post-box-container normal">
                            
                                <div id="business-details" class="postbox">
                                    <div class="handlediv" title="Click to toggle"><br /></div>
                                    <h3 class='hndle'><span>Business Details</span></h3>
                                    <div class="inside">
                                         <p><label for="<?php echo $singleBusinessName; ?>-name">Name for business: </label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-name" id="titan-<?php echo $singleBusinessName; ?>-name" size="45" value="<?php echo get_option($singleBusinessName.'-name'); ?>" /> [titan-business-details value=name name=<?php echo $shortcodeName; ?>]</p>
                                        <p><label for="<?php echo $singleBusinessName; ?>-admin-email">Admin Email: </label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-admin-email" id="titan-<?php echo $singleBusinessName; ?>-admin-email" size="45" value="<?php echo get_option($singleBusinessName.'-admin-email'); ?>" /> [titan-business-details value=admin-email name=<?php echo $shortcodeName; ?>]</p>
                        
                                        <p><label for="<?php echo $singleBusinessName; ?>-enquiries-email">Enquires Email:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-enquiries-email" id="<?php echo $singleBusinessName; ?>-enquiries-email" size="45" value="<?php echo get_option($singleBusinessName.'-enquiries-email'); ?>" /> [titan-business-details value=enquiries-email name=<?php echo $shortcodeName; ?>]</p>
                        
                                        <p><label for="<?php echo $singleBusinessName; ?>-mobile-1">Mobile Number:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-mobile-1" id="<?php echo $singleBusinessName; ?>-mobile-1" size="45" value="<?php echo get_option($singleBusinessName.'-mobile-1'); ?>" /> [titan-business-details value=mobile-1 name=<?php echo $shortcodeName; ?>]</p>
                        
                                        <p><label for="<?php echo $singleBusinessName; ?>-phone">Phone Number:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-phone" id="<?php echo $singleBusinessName; ?>-phone" size="45" value="<?php echo get_option($singleBusinessName.'-phone'); ?>" /> [titan-business-details value=phone name=<?php echo $shortcodeName; ?>]</p>
                        
                                        <p><label for="<?php echo $singleBusinessName; ?>-fax">Fax Number:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-fax" id="<?php echo $singleBusinessName; ?>-fax" size="45" value="<?php echo get_option($singleBusinessName.'-fax'); ?>" /> [titan-business-details value=fax name=<?php echo $shortcodeName; ?>]</p>
										
										<?php
										
									     foreach($extra_business_details as $key => $value)
										  echo $value['populate_html'];
										?>
										
										<div class="populate_here"></div>
										
                                    </div>
                                </div>
                    
                                <div id="physical-address" class="postbox">
                                    <div class="handlediv" title="Click to toggle"><br /></div>
                                    <h3 class='hndle'><span>Physical Address</span></h3>
                                    <div class="inside">
                                    
                                        <p><label for="<?php echo $singleBusinessName; ?>-address-1">Address 1:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-address-1" id="<?php echo $singleBusinessName; ?>-address-1" size="45" value="<?php echo get_option($singleBusinessName.'-address-1'); ?>" /> [titan-business-details value=address-1 name=<?php echo $shortcodeName; ?>]</p>
                        
                                        <p><label for="<?php echo $singleBusinessName; ?>-address-2">Address 2:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-address-2" id="<?php echo $singleBusinessName; ?>-address-2" size="45" value="<?php echo get_option($singleBusinessName.'-address-2'); ?>" /> [titan-business-details value=address-2 name=<?php echo $shortcodeName; ?>]</p>
                        
                                        <p><label for="<?php echo $singleBusinessName; ?>-suburb">Suburb:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-suburb" id="<?php echo $singleBusinessName; ?>-suburb" size="45" value="<?php echo get_option($singleBusinessName.'-suburb'); ?>" /> [titan-business-details value=suburb name=<?php echo $shortcodeName; ?>]</p>
                        
                                        <p><label for="<?php echo $singleBusinessName; ?>-state">State:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-state" id="<?php echo $singleBusinessName; ?>-state" size="45" value="<?php echo get_option($singleBusinessName.'-state'); ?>" /> [titan-business-details value=state name=<?php echo $shortcodeName; ?>]</p>
                                        
                                        <p><label for="<?php echo $singleBusinessName; ?>-postcode">Postcode:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-postcode" id="<?php echo $singleBusinessName; ?>-postcode" size="45" value="<?php echo get_option($singleBusinessName.'-postcode'); ?>" /> [titan-business-details value=postcode name=<?php echo $shortcodeName; ?>]</p>
                                    
                                        <p><label for="<?php echo $singleBusinessName; ?>-country">Country:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-country" id="<?php echo $singleBusinessName; ?>-country" size="45" value="<?php echo get_option($singleBusinessName.'-country'); ?>" /> [titan-business-details value=country name=<?php echo $shortcodeName; ?>]</p>
                                        
                                        <p><label for="<?php echo $singleBusinessName; ?>-gmaps">Google Maps iFrame:</label>  [titan-business-details value=gmaps name=<?php echo $shortcodeName; ?>]
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-gmaps" id="<?php echo $singleBusinessName; ?>-gmaps" size="120" value='<?php echo (stripcslashes(get_option($singleBusinessName.'-gmaps'))); ?>' /></p>
                                        
										<?php
									     foreach($extra_physical_address as $key => $value)
										  echo $value['populate_html'];
										 ?>
										
										<div class="populate_here"></div>
										
                                    </div>
                                </div>
                                
                                <div id="postal-address" class="postbox">
                                    <div class="handlediv" title="Click to toggle"><br /></div>
                                    <h3 class='hndle'><span>Postal Address</span></h3>
                                    <div class="inside">
                                    
                                        <p><label for="<?php echo $singleBusinessName; ?>-postal-address-1">Address 1:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-postal-address-1" id="<?php echo $singleBusinessName; ?>-postal-address-1" size="45" value="<?php echo get_option($singleBusinessName.'-postal-address-1'); ?>" /> [titan-business-details value=postal-address-1 name=<?php echo $shortcodeName; ?>]</p>
                        
                                        <p><label for="<?php echo $singleBusinessName; ?>-postal-address-2">Address 2:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-postal-address-2" id="<?php echo $singleBusinessName; ?>-postal-address-2" size="45" value="<?php echo get_option($singleBusinessName.'-postal-address-2'); ?>" /> [titan-business-details value=postal-address-2 name=<?php echo $shortcodeName; ?>]</p>
                        
                                        <p><label for="<?php echo $singleBusinessName; ?>-postal-suburb">Suburb:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-postal-suburb" id="<?php echo $singleBusinessName; ?>-psuburb" size="45" value="<?php echo get_option($singleBusinessName.'-postal-suburb'); ?>" /> [titan-business-details value=postal-suburb name=<?php echo $shortcodeName; ?>]</p>
                        
                                        <p><label for="<?php echo $singleBusinessName; ?>-postal-state">State:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-postal-state" id="<?php echo $singleBusinessName; ?>-postal-state" size="45" value="<?php echo get_option($singleBusinessName.'-postal-state'); ?>" /> [titan-business-details value=postal-state name=<?php echo $shortcodeName; ?>]</p>
                                        
                                        <p><label for="<?php echo $singleBusinessName; ?>-postal-postcode">Postcode:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-postal-postcode" id="<?php echo $singleBusinessName; ?>-postal-postcode" size="45" value="<?php echo get_option($singleBusinessName.'-postal-postcode'); ?>" /> [titan-business-details value=postal-postcode name=<?php echo $shortcodeName; ?>]</p>
					
                                          <p><label for="<?php echo $singleBusinessName; ?>-postal-country">Country:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-postal-country" id="<?php echo $singleBusinessName; ?>-postal-country" size="45" value="<?php echo get_option($singleBusinessName.'-postal-country'); ?>" /> [titan-business-details value=postal-country name=<?php echo $shortcodeName; ?>]</p>
										<?php
									     foreach($extra_postal_address as $key => $value)
										  echo $value['populate_html'];
										 ?>

										<div class="populate_here"></div>
										
                                    </div>
                                    
                                </div>
                                <div id="opening-hours" class="postbox">
                                    <div class="handlediv" title="Click to toggle"><br /></div>
                                    <h3 class='hndle'><span>Opening Hours</span></h3>
                                    <div class="inside">
                                    
                                        <p><label for="<?php echo $singleBusinessName; ?>-opening-mon-fri">Monday to Friday:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-opening-mon-fri" id="<?php echo $singleBusinessName; ?>-opening-mon-fri" size="45" value="<?php echo get_option($singleBusinessName.'-opening-mon-fri'); ?>" /> [titan-business-details value=opening-mon-fri name=<?php echo $shortcodeName; ?>]</p>

                                        <p><label for="<?php echo $singleBusinessName; ?>-opening-mon">Monday:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-opening-mon" id="<?php echo $singleBusinessName; ?>-opening-mon" size="45" value="<?php echo get_option($singleBusinessName.'-opening-mon'); ?>" /> [titan-business-details value=opening-mon name=<?php echo $shortcodeName; ?>]</p>

                                        <p><label for="<?php echo $singleBusinessName; ?>-opening-tue">Tuesday:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-opening-tue" id="<?php echo $singleBusinessName; ?>-opening-tue" size="45" value="<?php echo get_option($singleBusinessName.'-opening-tue'); ?>" /> [titan-business-details value=opening-tue name=<?php echo $shortcodeName; ?>]</p>

                                        <p><label for="<?php echo $singleBusinessName; ?>-opening-wed">Wednesday:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-opening-wed" id="<?php echo $singleBusinessName; ?>-opening-wed" size="45" value="<?php echo get_option($singleBusinessName.'-opening-wed'); ?>" /> [titan-business-details value=opening-wed name=<?php echo $shortcodeName; ?>]</p>

                                        <p><label for="<?php echo $singleBusinessName; ?>-opening-thurs">Thursday:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-opening-thurs" id="<?php echo $singleBusinessName; ?>-opening-thurs" size="45" value="<?php echo get_option($singleBusinessName.'-opening-thurs'); ?>" /> [titan-business-details value=opening-thurs name=<?php echo $shortcodeName; ?>]</p>

                                        <p><label for="<?php echo $singleBusinessName; ?>-opening-fri">Friday:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-opening-fri" id="<?php echo $singleBusinessName; ?>-opening-fri" size="45" value="<?php echo get_option($singleBusinessName.'-opening-fri'); ?>" /> [titan-business-details value=opening-fri name=<?php echo $shortcodeName; ?>]</p>
                        
                                        <p><label for="<?php echo $singleBusinessName; ?>-opening-sat">Saturday:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-opening-sat" id="<?php echo $singleBusinessName; ?>-opening-sat" size="45" value="<?php echo get_option($singleBusinessName.'-opening-sat'); ?>" /> [titan-business-details value=opening-sat name=<?php echo $shortcodeName; ?>]</p>
                        
                                        <p><label for="<?php echo $singleBusinessName; ?>-opening-sun">Sunday & Public Hols:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-opening-sun" id="<?php echo $singleBusinessName; ?>-opening-sun" size="45" value="<?php echo get_option($singleBusinessName.'-opening-sun'); ?>" /> [titan-business-details value=opening-sun name=<?php echo $shortcodeName; ?>]</p>
										
										<?php
									     foreach($extra_opening_hours as $key => $value)
										  echo $value['populate_html'];
										?>
										
										<div class="populate_here"></div>
										
                                    </div>                                    
                                </div>
                                <div id="social-links" class="postbox">
                                    <div class="handlediv" title="Click to toggle"><br /></div>
                                    <h3 class='hndle'><span>Social Links</span></h3>
                                    <div class="inside">
                                    
                                        <p><label for="<?php echo $singleBusinessName; ?>-social-facebook">Facebook:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-social-facebook" id="<?php echo $singleBusinessName; ?>-social-facebook" size="45" value="<?php echo get_option($singleBusinessName.'-social-facebook'); ?>" /> [titan-business-details value=social-facebook name=<?php echo $shortcodeName; ?>]</p>
                        
                                        <p><label for="<?php echo $singleBusinessName; ?>-social-twitter">Twitter:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-social-twitter" id="<?php echo $singleBusinessName; ?>-social-twitter" size="45" value="<?php echo get_option($singleBusinessName.'-social-twitter'); ?>" /> [titan-business-details value=social-twitter name=<?php echo $shortcodeName; ?>]</p>
                        
                                        <p><label for="<?php echo $singleBusinessName; ?>-social-linkedin">LinkedIn:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-social-linkedin" id="<?php echo $singleBusinessName; ?>-social-linkedin" size="45" value="<?php echo get_option($singleBusinessName.'-social-linkedin'); ?>" /> [titan-business-details value=social-linkedin name=<?php echo $shortcodeName; ?>]</p>

                                        <p><label for="<?php echo $singleBusinessName; ?>-social-youtube">YouTube:</label>
                                        <input type="text" name="<?php echo $singleBusinessName; ?>-social-youtube" id="<?php echo $singleBusinessName; ?>-social-youtube" size="45" value="<?php echo get_option($singleBusinessName.'-social-youtube'); ?>" /> [titan-business-details value=social-youtube name=<?php echo $shortcodeName; ?>]</p>
									
										<?php
									     foreach($extra_social_links as $key => $value)
										  echo $value['populate_html'];
										?>
										
										<div class="populate_here"></div>
									
                                    </div>                                    
                                </div>
                                
                        </div>
    
                        <p class="submit">
                            <?php wp_nonce_field($this->titan_plugin_slug,'_wp_titan_nonce'); ?>
                            <?php submit_button( __('Save Changes', $this->titan_plugin_slug), 'button-primary', 'submit', false ); ?>
                        </p>
        
                        <input type="hidden" name="<?php echo $this->titan_plugin_slug; ?>" value="1" />
                    </form>
                </div>
                
                
                <div id="poststuff" class="column-2">
                
                    <div id="members-about" class="postbox" >
                        <div class="handlediv" title="Click to toggle"><br /></div>
                        <h3 class='hndle'><span>About</span></h3>
                        <div class="inside">
                        
                            <p><strong>Name:</strong> <?php echo $this->titan_plugin_name; ?></p>
                            <p><strong>Version:</strong> <?php echo self::$titan_version; ?></p>
                            <p><strong>Author:</strong> <a href="http://www.titanweb.com.au" target="_blank" title="Visit author homepage"><?php echo self::$titan_author;?> (Titan Interactive)</a></p>
                            
                            <p><strong>Description:</strong></p>
                            <p>This plugin will enable your site to have all your business details in one locations. Just fill in your business details here to enable the use of them around your website.</p>
                            <p>All these fields are stored with in Wordpress. So in order to utilise them I have made available 2 options:</p>
                            <ul class="webby-list-long">
                            	<li><strong>Shortcodes</strong></li>
                            	<li><strong>Get Option</strong></li>
                            </ul>
                            <p>To use either of these options see below</p>
                       </div>
                    </div>
                    
                    <div id="members-shortcodes" class="postbox" >
                        <div class="handlediv" title="Click to toggle"><br /></div>
                        <h3 class='hndle'><span>Shortcodes</span></h3>
                        <div class="inside">
                        
                            <p><strong>How to use shortcodes:</strong></p>
                            <p>To use these field variables around the site theme by using Shortcodes, simply use the do_shortcode() Wordpress method along with the required name and parameters,<br />OR<br />simply insert the shortcode without the do_shortcode() Wordpress method into a post or page.</p>
                            <ul class="webby-list-long">
                            	<li><strong>Shortcode Name:</strong> <?php echo $this->titan_plugin_slug; ?></li>
                            	<li><strong>Value:</strong> gmaps (this is an example value only)</li>
                            	<li><strong>Name:</strong> Name of the business (no spaces)</li>
                            </ul>
							<p>See below for examples and shortcode values that are available. Just replace the "value" variable in the shortcode with one of the below available variables</p>
                            <p><strong>Theme Examples:</strong></p>
                            <p>&lt;?php echo do_shortcode("[<strong>insert short code here</strong>]") ?&gt; </p>
                            
                            <p><strong>Post/Page Examples:</strong></p>
                            <p>Copy and paste short code next to the filed you want</p>
                            
                           
                       </div>
                    </div>
                    
                    <div id="members-getoptions" class="postbox" >
                        <div class="handlediv" title="Click to toggle"><br /></div>
                        <h3 class='hndle'><span>Get Options</span></h3>
                        <div class="inside">
                        
                            <p><strong>How to use Get Options:</strong></p>
                            <p>Using this option you are only able to use them with in your theme and not in Posts/Pages.</p>
                            <p>It is very similar to using the do_shortcode() method however this time you need to utilise the get_option() method and you will need to specify the full name of the available variable.<br />See below for examples and a list of available variables/fields.</p>
                            
                            <p><strong>For Example:</strong></p>
                            <p>&lt;?php echo get_option('<?php echo $singleBusinessName; ?>-admin-email'); ?&gt; <br />&lt;?php echo get_option('<?php echo $singleBusinessName; ?>-mobile-1'); ?&gt;</p>
                            
                            <p><strong>Google Maps Example:</strong></p>
                            <p>&lt;?php echo html_entity_decode(stripcslashes(get_option('<?php echo $singleBusinessName; ?>-admin-email'))); ?&gt;</p>
                            
                            <p><strong>Available Variables:</strong></p>
                            <ul class="webby-list-long" id="webby-list-long">
                                <li><?php echo $singleBusinessName; ?>-admin-email</li>
                                <li><?php echo $singleBusinessName; ?>-enquiries-email</li>
                                <li><?php echo $singleBusinessName; ?>-mobile-1</li>
                                <li><?php echo $singleBusinessName; ?>-phone</li>
                                <li class='li_business'><?php echo $singleBusinessName; ?>-fax</li>
								
								<?php
								 foreach($extra_business_details as $key => $value)
								  echo "<li class='li_business' id='".$value['option_id']."'>".$singleBusinessName.'-'.$value['nice_shortcode']."</li>";
								?>
								
                                <li class="pad-top"><?php echo $singleBusinessName; ?>-address-1</li>
                                <li><?php echo $singleBusinessName; ?>-address-2</li>
                                <li><?php echo $singleBusinessName; ?>-suburb</li>
                                <li><?php echo $singleBusinessName; ?>-state</li>
                                <li><?php echo $singleBusinessName; ?>-postcode</li>
                                <li class='li_physical'><?php echo $singleBusinessName; ?>-gmaps</li>
								
								<?php
								 foreach($extra_physical_address as $key => $value)
								  echo "<li class='li_physical' id='".$value['option_id']."'>".$singleBusinessName.'-'.$value['nice_shortcode']."</li>";
								?>
								
                                <li class="pad-top"><?php echo $singleBusinessName; ?>-postal-address-1</li>
                                <li><?php echo $singleBusinessName; ?>-postal-address-2</li>
                                <li><?php echo $singleBusinessName; ?>-postal-suburb</li>
                                <li><?php echo $singleBusinessName; ?>-postal-state</li>
                                <li class='li_postal'><?php echo $singleBusinessName; ?>-postal-postcode</li>
								
								<?php
								 foreach($extra_postal_address as $key => $value)
								  echo "<li class='li_postal' id='".$value['option_id']."'>".$singleBusinessName.'-'.$value['nice_shortcode']."</li>";
								?>
								
                                <li class="pad-top"><?php echo $singleBusinessName; ?>-opening-mon-fri</li>
                                <li><?php echo $singleBusinessName; ?>-opening-mon</li>
                                <li><?php echo $singleBusinessName; ?>-opening-tue</li>
								<li><?php echo $singleBusinessName; ?>-opening-wed</li>
                                <li><?php echo $singleBusinessName; ?>-opening-thurs</li>
								<li><?php echo $singleBusinessName; ?>-opening-fri</li>
                                <li><?php echo $singleBusinessName; ?>-opening-sat</li>
                                <li class='li_opening'><?php echo $singleBusinessName; ?>-opening-sun</li>
								
								<?php
								 foreach($extra_opening_hours as $key => $value)
								  echo "<li class='li_opening' id='".$value['option_id']."'>".$singleBusinessName.'-'.$value['nice_shortcode']."</li>";
								?>
								
                                <li class="pad-top"><?php echo $singleBusinessName; ?>-social-facebook</li>
                                <li><?php echo $singleBusinessName; ?>-social-twitter</li>
                                <li><?php echo $singleBusinessName; ?>-social-linkedin</li>
                                <li class='li_social'><?php echo $singleBusinessName; ?>-social-youtube</li>
								
								<?php
								 foreach($extra_social_links as $key => $value)
								  echo "<li class='li_social' id='".$value['option_id']."'>".$singleBusinessName.'-'.$value['nice_shortcode']."</li>";
								?>

                            </ul>
                       </div>
                    </div>
                    
                                            
                </div>
                
                
            </div>
        </div>