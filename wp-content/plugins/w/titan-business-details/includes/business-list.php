<div class="wrap webby">

		<div id="icon-options-general" class="icon32"><br /></div>
		<h2><?php echo $this->titan_plugin_name; ?></h2>
        <div class="metabox-holder">
            <div id="poststuff" class="column-1">
    			<div class="post-box-container normal">
                            <div id="titain-analytics-id" class="postbox">
                                  <h3 class='hndle'><span>Google Analytics</span></h3>
                                    <div class="inside">
                                     <form method="post" action="" id="titan-google-analytics-form">
                                         <p>
                                            <label>Google Analytics Tracking ID 1</label> 
                                            <input type="text" id="titan-google-analytics-id" size="20" name="titan-google-analytics-id" id="titan-google-analytics-id" value="<?php echo get_option( 'titan-google-analytics-id' ); ?>" />
                                            <span class="description">eg. UA-47403208-1</span>
                                         </p>
										 
										 <p>
                                            <label>Google Analytics Tracking ID 2</label> 
                                            <input type="text" id="titan-google-analytics-id-2" size="20" name="titan-google-analytics-id-2" id="titan-google-analytics-id-2" value="<?php echo get_option( 'titan-google-analytics-id-2' ); ?>" />
                                            <span class="description">eg. UA-48680904-2 </span>
                                         </p>
										 
										 <p>
                                            <label>Google Analytics Tracking ID 3</label> 
                                            <input type="text" id="titan-google-analytics-id-3" size="20" name="titan-google-analytics-id-3" id="titan-google-analytics-id-3" value="<?php echo get_option( 'titan-google-analytics-id-3' ); ?>" />
                                            <span class="description"> eg. UA-56842465-3 </span>
                                         </p>
										 
										 <p>
                                            <label>Universial Tracking Code</label> 
                                            <input type="checkbox" value="yes" <?php checked( 'yes', get_option( 'titan-insert-tracking-code-uni', 'yes' ) ); ?> name="titan-insert-tracking-code-uni" id="titan-insert-tracking-code-uni" />
                                             <span class="description"> If checked, Universal Analytics code is used. </span>
                                         </p>
										 
                                         <p>
                                            <label>Insert Google Tracking Code</label> 
                                            <input type="checkbox" value="yes" <?php checked( 'yes', get_option( 'titan-insert-tracking-code', 'yes' ) ); ?> name="titan-insert-tracking-code" id="titan-insert-tracking-code" />
                                            <span class="spinner"></span>
                                            <input type="submit" value="Update" name="titan-update-google-analytics" id="titan-update-google-analytics" class="button-primary alignright" />
                                             <span class="description">If checked, google tracking code will be inserted into header.</span>
                                         </p>
                                         
                                     
                                     </form>
                                    </div>
                            </div>
                                <div id="add-business" class="postbox">
                                    <div class="handlediv" title="Click to toggle"><br /></div>
                                    <h3 class='hndle'><span>Add a new business</span></h3>
                                    <div class="inside">
                                    <form method="post" action="" id="<?php echo $this->titan_plugin_slug; ?>">
									 <p><label for="<?php echo $this->titan_plugin_slug; ?>-business-name">Business Name:</label>
                                        <input type="text" name="<?php echo $this->titan_plugin_slug; ?>-business-name" id="<?php echo $this->titan_plugin_slug; ?>-business-name" size="45" value="" maxlength="40" /></p>
										<p>Note: Business Name should be Alpha Numeric only.</p>
                                    <p class="submit">
										<?php wp_nonce_field($this->titan_plugin_slug,'_wp_titan_nonce'); ?>
                                        <?php submit_button( __('Add Business', $this->titan_plugin_slug), 'button-primary', 'submit', false ); ?>
                                    </p>
        							
                        			<input type="hidden" name="<?php echo $this->titan_plugin_slug; ?>" value="1" />
                                    </form>
                                    </div>
                                 </div>
                                 <div id="business-list" class="postbox">
                                 	<div class="handlediv" title="Click to toggle"><br /></div>
                                    <h3 class='hndle'><span>Business list</span></h3>
                                    <div class="inside">
                                    <table>
                                            <?php	
											global $titan_tbd;
                                            if ( $this->arrayBusiness && is_array( $this->arrayBusiness ) ){
                                                    foreach ($this->arrayBusiness as $business){ 				
                                                    ?>
                                                    <tr height="30">
                                                    <td width="200"><?php echo $business['name']; ?></td><td width="100"><a href="admin.php?page=edit-business-details&businessName=<?php echo $titan_tbd->clean_business_name($business['name']); ?>" class="button">Edit Details</a></td><td width="100"><a href="admin.php?page=edit-business-details&businessName=<?php echo $titan_tbd->clean_business_name($business['name']); ?>&action=delete" class="button">Delete Business</a></td>
                                                    </tr>
                                                    <?php }	
                                            } ?>
                                    </table>
                                       
                                    
                                    </div>
                                </div>
                </div>
    
                </div>
                
                
                <div id="poststuff" class="column-2">
                
                    <div id="members-about" class="postbox" >
                        <div class="handlediv" title="Click to toggle"><br /></div>
                        <h3 class='hndle'><span>About</span></h3>
                        <div class="inside">
                        
                            <p><strong>Name:</strong> <?php echo $this->titan_plugin_name; ?></p>
                            <p><strong>Version:</strong> <?php echo self::$titan_version; ?></p>
                            <p><strong>Author:</strong> <a href="http://www.titanweb.com.au" target="_blank" title="Visit author homepage">Shayne (Titan Interactive)</a></p>
                            
                            <p><strong>Description:</strong></p>
                            <p>This plugin will enable your site to have all your business details in one locations. Just fill in your business details here to enable the use of them around your website.</p>
                           
                       </div>
                    </div>
                    
                               
                </div>
                
                
            </div>
        </div>


