<?php

function titan_options_delete()
{
	// Save the posted value in the database
	delete_option( 'titan-business-details-admin-email' );							
	delete_option( 'titan-business-details-enquiries-email' );							
	delete_option( 'titan-business-details-mobile-1' );							
	delete_option( 'titan-business-details-phone' );							
	delete_option( 'titan-business-details-fax' );	
							
	delete_option( 'titan-business-details-address-1' );							
	delete_option( 'titan-business-details-address-2' );							
	delete_option( 'titan-business-details-suburb' );							
	delete_option( 'titan-business-details-state' );							
	delete_option( 'titan-business-details-postcode' );							
	delete_option( 'titan-business-details-gmaps' );							
							
	delete_option( 'titan-business-details-postal-address-1' );							
	delete_option( 'titan-business-details-postal-address-2' );							
	delete_option( 'titan-business-details-postal-suburb' );							
	delete_option( 'titan-business-details-postal-state' );							
	delete_option( 'titan-business-details-postal-postcode' );	
	
	delete_option( 'titan-business-details-opening-mon-fri' );
	delete_option( 'titan-business-details-opening-thurs' );		
	delete_option( 'titan-business-details-opening-sat' );		
	delete_option( 'titan-business-details-opening-sun' );							

	delete_option( 'titan-business-details-social-facebook' );							
	delete_option( 'titan-business-details-social-twitter' );
	delete_option( 'titan-business-details-social-linkedin' );							
	delete_option( 'titan-business-details-social-youtube' );							
								
}

titan_options_delete();
?>