var j = jQuery.noConflict();

j(function(){
 
 var category_dom = ['business-details','physical-address','postal-address','opening-hours','social-links'];
 var html = '<div class="dynamic_ed"><div class="new_field_dom" style="display:none">New Label: <input type="text" size="45" class="label" name="titan-'+j.url('businessName')+'-cat_[xxx]-label[]" value="" />&nbsp;Value: <input type="text" size="45" class="value" name="titan-'+j.url('businessName')+'-cat_[xxx]-value[]" value="" />&nbsp;<input type="button" value="Save" class="button button-primary save_extra_field" rel="[xxx]"/>&nbsp;<input type="button" value="Cancel" class="button button-primary cancel_extra_field" rel="[xxx]"/><br/></div><input type="button" class="button button-primary new_extra_field" value="Add New Field" style="margin-top:5px;"/></div>'; 
 var $cloned = '',x = '',t='';
 
 for (var i=0, tot=category_dom.length; i < tot; i++) {
   x = category_dom[i].replace(/-(.*)/g,'');
   t = html.replace(/\[xxx\]/g,x);
   j('#'+category_dom[i]+' .inside .populate_here').html(t);
 }
 
 j('.new_extra_field').click(function(){
  
   var $obj = j(this),$parent = $obj.parent(),errors = true;
   
   if($parent.find('.new_field_dom').css('display') == 'none')$parent.find('.new_field_dom').css('display','block');
   
   $parent .find('input:text').each(function(i,o){
    if(j.trim(j(o).val()) == ''){ errors = true;return false;}
	else errors	= false;
   });

   if(!errors){
    $cloned =  $parent.find('.new_field_dom:eq(0)').clone();
    $cloned.find('.label').attr({'value':''});
	$cloned.find('.value').attr({'value':''});
    j($cloned).insertBefore($obj);
   }
   
 });
 
 j('#titan-business-details').delegate('.remove_extra_field','click',function(){
 var $obj = j(this);
 var id = this.id, n = $obj.attr('rel');
 
  j.ajax({
   async:false, // Prevent double clicking
   url: "/wp-admin/admin-ajax.php",
   data:{action: 'remove_extra_detail',id: id,name: n},
   dataType:'json',
   type:'POST',
   success:function(data){
    if(data.error == 0){
	 $obj.parent().remove();
	  // Also remove from sidebar "Available Variables"
	  j('#webby-list-long').find('#'+id).remove();
	}
   },
   error: function(){
	alert('Internal error occured.');
   }
  });
 });
 
 j('#titan-business-details').delegate('.save_extra_field','click',function(e){

 var $obj = j(this);
 var $parent = $obj.parent();
 
 var $label = $parent.find('.label');
 var $value =$parent.find('.value');
 var cat = 'li_'+$obj.attr('rel');
 
 var name =$parent.find('.value').attr('name'); 
 
 var html = '';
 
  if(j.trim($label.val()) != '' && j.trim($value.val()) != ''){
	 j.ajax({
	   url: "/wp-admin/admin-ajax.php",
	   data:{action: 'add_extra_detail',label: $label.val(),value: $value.val(),name:name},
	   dataType:'json',
	   type:'POST',
	   success:function(data){
	    if(data.error == 0){
		  $obj.closest('.inside').find('p:last').after(data.content);
		  
		  // Also append to sidebar "Available Variables"
		  j('#webby-list-long').find('.'+cat).last().after("<li class='"+cat+"' id='"+data.oid+"'>"+data.option_name+"</li>");
		  
		  if($obj.closest('.dynamic_ed').find('.new_field_dom').length == 1){ $label.val('');$value.val('');}
		  else $obj.parent().remove();
		}
	   },
	   error: function(){
	    alert('Internal error occured.');
	   }
	});
  }
 });
 
 j('#titan-business-details').delegate('.cancel_extra_field','click',function(){
  var $obj = j(this);
  if($obj.closest('.dynamic_ed').find('.new_field_dom').length != 1) $obj.parent().remove();
 }); 
 
j('#submit').click(function(){j('.populate_here').remove()}) 
    
    //Tracking ID
    j('#titan-update-google-analytics').click(function(e){
        e.preventDefault();
        var tracking_id = j('#titan-google-analytics-id').val();
		
			var tracking_id_2 = j('#titan-google-analytics-id-2').val();
			var tracking_id_3 = j('#titan-google-analytics-id-3').val();
        
			var insert_tracking_code_uni = 'no';
			if(  j('#titan-insert-tracking-code-uni').is(':checked') ) {
				insert_tracking_code_uni = 'yes';
			}
		
		var insert_tracking_code = 'no';
        if(  j('#titan-insert-tracking-code').is(':checked') ) {
            insert_tracking_code = 'yes';
        }
        
        
        var data = {
		action: 'titan_update_google_tracking_id',
		tracking_id: j('#titan-google-analytics-id').val(),
			tracking_id_2: j('#titan-google-analytics-id-2').val(),
			tracking_id_3: j('#titan-google-analytics-id-3').val(),
			insert_tracking_code_uni: insert_tracking_code_uni,
        insert_tracking_code: insert_tracking_code
	    };
		
 
		
		
		
        j('#titan-google-analytics-form .spinner').show();
	// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
	j.post(ajaxurl, data, function(response) {
            j('#titan-google-analytics-form .spinner').hide();
	});
    });



});
// Get url parameters
j.url = function(name){ var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(top.window.location.href); return (results !== null) ? results[1] : 0;}